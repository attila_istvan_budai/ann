package com.bme.matirx.operations;

import static org.bridj.Pointer.allocateFloats;
import static org.bridj.Pointer.allocateInts;

import java.io.IOException;
import java.nio.ByteOrder;

import org.bridj.Pointer;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.bme.data.clmatrix.CLMatrixOperations;
import com.bme.data.densematrix.DenseMatrix;
import com.bme.data.densematrix.DenseMatrixOperations;
import com.bme.datamining.ann.rbm.RestrictedBoltzmannMachineOpenCL;
import com.bme.opencl.CLMemoryManager;
import com.bme.opencl.OpenCLResources;
import com.nativelibs4java.opencl.CLBuffer;
import com.nativelibs4java.opencl.CLContext;
import com.nativelibs4java.opencl.CLMem;
import com.nativelibs4java.opencl.CLMem.Usage;
import com.nativelibs4java.opencl.CLProgram;
import com.nativelibs4java.opencl.CLQueue;
import com.nativelibs4java.opencl.JavaCL;
import com.nativelibs4java.opencl.util.ParallelRandom;
import com.nativelibs4java.util.IOUtils;

public class CLMatrixOperationsTest {
	CLContext context;
	CLQueue queue;
	ByteOrder byteOrder;
	CLProgram program;

	@BeforeClass
	public void initObjects() throws IOException {
		context = JavaCL.createBestContext();
		queue = context.createDefaultQueue();
		byteOrder = context.getByteOrder();
		String src = IOUtils.readText(RestrictedBoltzmannMachineOpenCL.class.getResource("rbm.cl"));
		program = context.createProgram(src);
		OpenCLResources resources = new OpenCLResources(context, queue, byteOrder, program);
		CLMemoryManager memoryManager = new CLMemoryManager(resources);

	}

	@Test
	public void testMatrixMultiplication() {
		System.out.println("testMatrixMultiplicationCL");

		float[] first = new float[] { -2, 3, -4, 1, 5, -3 };
		float[] second = new float[] { -2, -1, 6, 7, 8, 9, 10, 0 };

		DenseMatrix firstMatrix = new DenseMatrix(first, 2, 3);
		DenseMatrix secondMatrix = new DenseMatrix(second, 4, 2);

		Pointer<Float> matrix1 = allocateFloats(first.length).order(byteOrder), matrix2 = allocateFloats(second.length).order(byteOrder);
		matrix1.setFloats(first);
		matrix2.setFloats(second);
		CLBuffer<Float> m1 = context.createBuffer(Usage.Input, matrix1), m2 = context.createBuffer(Usage.Input, matrix2);
		CLBuffer<Float> out = context.createFloatBuffer(CLMem.Usage.InputOutput, 4 * 3);

		CLMatrixOperations.multiplyByMatrix(m1, m2, out, 2, 4, 3, new OpenCLResources(context, queue, byteOrder, program));
		Pointer<Float> outPtr = out.read(queue);

		float[] result = DenseMatrixOperations.multiplyByMatrix(first, second, 2, 4);
		DenseMatrix resultMatrix = new DenseMatrix(result, 4, 3);

		System.out.println(firstMatrix);
		System.out.println("+");
		System.out.println(secondMatrix);
		System.out.println("=");
		System.out.println(resultMatrix);

		for (int i = 0; i < result.length; i++) {
			System.out.println(outPtr.get(i) + " " + result[i]);
		}
		for (int i = 0; i < result.length; i++) {
			Assert.assertEquals(outPtr.get(i), result[i]);
		}

	}

	@Test
	public void testMatrixMultiplication2() {
		System.out.println("testMatrixMultiplicationCL");

		float[] first = new float[] { -2, 3, -4, 1, 5, -3 };
		float[] second = new float[] { -2, -1, 6, 7, 8, 9, 10, 0 };

		DenseMatrix firstMatrix = new DenseMatrix(first, 2, 3);
		DenseMatrix secondMatrix = new DenseMatrix(second, 4, 2);

		Pointer<Float> matrix1 = allocateFloats(first.length).order(byteOrder), matrix2 = allocateFloats(second.length).order(byteOrder);
		matrix1.setFloats(first);
		matrix2.setFloats(second);
		CLBuffer<Float> m1 = context.createBuffer(Usage.Input, matrix1), m2 = context.createBuffer(Usage.Input, matrix2);
		CLBuffer<Float> out = context.createFloatBuffer(CLMem.Usage.InputOutput, 4 * 3);

		CLMatrixOperations.multiplyByMatrixV2(m1, m2, out, 2, 4, 3, new OpenCLResources(context, queue, byteOrder, program));
		Pointer<Float> outPtr = out.read(queue);

		float[] result = DenseMatrixOperations.multiplyByMatrix(first, second, 2, 4);
		DenseMatrix resultMatrix = new DenseMatrix(result, 4, 3);

		System.out.println(firstMatrix);
		System.out.println("+");
		System.out.println(secondMatrix);
		System.out.println("=");
		System.out.println(resultMatrix);

		for (int i = 0; i < result.length; i++) {
			System.out.println(outPtr.get(i) + " " + result[i]);
		}
		for (int i = 0; i < result.length; i++) {
			Assert.assertEquals(outPtr.get(i), result[i]);
		}

	}

	// @Test
	public void testMatrixMultiplication3() {
		System.out.println("testMatrixMultiplicationCL v3");

		float[] first = new float[] { -2, 3, -4, 1, 5, -3 };
		float[] second = new float[] { -2, -1, 6, 7, 8, 9, 10, 0 };

		DenseMatrix firstMatrix = new DenseMatrix(first, 2, 3);
		DenseMatrix secondMatrix = new DenseMatrix(second, 4, 2);

		Pointer<Float> matrix1 = allocateFloats(first.length).order(byteOrder), matrix2 = allocateFloats(second.length).order(byteOrder);
		matrix1.setFloats(first);
		matrix2.setFloats(second);
		CLBuffer<Float> m1 = context.createBuffer(Usage.Input, matrix1), m2 = context.createBuffer(Usage.Input, matrix2);
		CLBuffer<Float> out = context.createFloatBuffer(CLMem.Usage.InputOutput, 4 * 3);

		CLMatrixOperations.multiplyByMatrixV3(m1, m2, out, 2, 4, 3, new OpenCLResources(context, queue, byteOrder, program));
		Pointer<Float> outPtr = out.read(queue);

		float[] result = DenseMatrixOperations.multiplyByMatrix(first, second, 2, 4);
		DenseMatrix resultMatrix = new DenseMatrix(result, 4, 3);

		System.out.println(firstMatrix);
		System.out.println("+");
		System.out.println(secondMatrix);
		System.out.println("=");
		System.out.println(resultMatrix);

		for (int i = 0; i < result.length; i++) {
			System.out.println(outPtr.get(i) + " " + result[i]);
		}
		for (int i = 0; i < result.length; i++) {
			Assert.assertEquals(outPtr.get(i), result[i]);
		}

	}

	@Test
	public void testMatrixMultiplicationNotSquared() {
		System.out.println("testMatrixMultiplicationNotSquaredCL");

		float[] first = new float[] { -4, 5, 2, 3, 4, -3, 1, 6, 7 };
		float[] second = new float[] { 0, 5, -4, 8, 9, -3, 4, -2, 6 };

		Pointer<Float> matrix1 = allocateFloats(first.length).order(byteOrder), matrix2 = allocateFloats(second.length).order(byteOrder);
		matrix1.setFloats(first);
		matrix2.setFloats(second);
		CLBuffer<Float> m1 = context.createBuffer(Usage.Input, matrix1), m2 = context.createBuffer(Usage.Input, matrix2);
		CLBuffer<Float> out = context.createFloatBuffer(CLMem.Usage.InputOutput, 3 * 3);

		CLMatrixOperations.multiplyByMatrix(m1, m2, out, 3, 3, 3, new OpenCLResources(context, queue, byteOrder, program));
		Pointer<Float> outPtr = out.read(queue);

		float[] result = DenseMatrixOperations.multiplyByMatrix(first, second, 3, 3);

		for (int i = 0; i < result.length; i++) {
			System.out.println(outPtr.get(i) + " " + result[i]);
		}
		for (int i = 0; i < result.length; i++) {
			Assert.assertEquals(outPtr.get(i), result[i]);
		}

	}

	@Test
	public void testMatrixTranspose() {
		float[] first = new float[] { 1, 2, 3, 4, 5, 6, 7, 8 };

		Pointer<Float> matrix = allocateFloats(first.length).order(byteOrder);
		matrix.setFloats(first);
		CLBuffer<Float> m = context.createBuffer(Usage.Input, matrix);
		// CLBuffer<Float> out =
		// context.createFloatBuffer(CLMem.Usage.InputOutput, 8);

		CLBuffer<Float> out = CLMatrixOperations.transpose(m, 2, 4, new OpenCLResources(context, queue, byteOrder, program));
		Pointer<Float> outPtr = out.read(queue);

		float[] result = DenseMatrixOperations.transposeMatrix(first, 2);

		for (int i = 0; i < result.length; i++) {
			System.out.println(outPtr.get(i) + " " + result[i]);
		}
		for (int i = 0; i < result.length; i++) {
			Assert.assertEquals(outPtr.get(i), result[i]);
		}

	}

	@Test
	public void testMatrixTransposeForBigDataSquaerd() {
		System.out.println("CLMatrixOperationsTest.testMatrixTransposeForBigDataSquaerd()");
		float[] first = new float[80];
		for (int i = 0; i < first.length; i++) {
			first[i] = 1;
		}

		Pointer<Float> matrix = allocateFloats(first.length).order(byteOrder);
		matrix.setFloats(first);
		CLBuffer<Float> m = context.createBuffer(Usage.Input, matrix);
		// CLBuffer<Float> out =
		// context.createFloatBuffer(CLMem.Usage.InputOutput, 8);

		CLBuffer<Float> out = CLMatrixOperations.transpose(m, 8, 10, new OpenCLResources(context, queue, byteOrder, program));
		Pointer<Float> outPtr = out.read(queue);

		float[] result = DenseMatrixOperations.transposeMatrix(first, 8);

		for (int i = 0; i < result.length; i++) {
			System.out.println(outPtr.get(i) + " " + result[i]);
		}
		for (int i = 0; i < result.length; i++) {
			Assert.assertEquals(outPtr.get(i), result[i]);
		}

	}

	@Test
	public void testMatrixTransposeForBigData() {
		System.out.println("CLMatrixOperationsTest.testMatrixTransposeForBigData()");
		float[] first = new float[1400];
		for (int i = 0; i < first.length; i++) {
			first[i] = 1;
		}

		Pointer<Float> matrix = allocateFloats(first.length).order(byteOrder);
		matrix.setFloats(first);
		CLBuffer<Float> m = context.createBuffer(Usage.Input, matrix);
		// CLBuffer<Float> out =
		// context.createFloatBuffer(CLMem.Usage.InputOutput, 8);

		CLBuffer<Float> out = CLMatrixOperations.transpose(m, 20, 70, new OpenCLResources(context, queue, byteOrder, program));
		Pointer<Float> outPtr = out.read(queue);

		float[] result = DenseMatrixOperations.transposeMatrix(first, 20);

		for (int i = 0; i < result.length; i++) {
			System.out.println(outPtr.get(i) + " " + result[i]);
		}
		for (int i = 0; i < result.length; i++) {
			Assert.assertEquals(outPtr.get(i), result[i]);
		}

	}

	@Test
	public void testAddMatrices() {
		float[] first = new float[] { 1, 2, 3, 4, 5, 6, 7, 8 };
		float[] second = new float[] { 1, 2, 3, 4, 5, 6, 7, 8 };

		Pointer<Float> matrix1 = allocateFloats(first.length).order(byteOrder), matrix2 = allocateFloats(second.length).order(byteOrder);
		matrix1.setFloats(first);
		matrix2.setFloats(second);
		CLBuffer<Float> m1 = context.createBuffer(Usage.Input, matrix1), m2 = context.createBuffer(Usage.Input, matrix2);

		CLMatrixOperations.addMatrix(m1, m2, first.length, new OpenCLResources(context, queue, byteOrder, program));
		Pointer<Float> outPtr = m1.read(queue);

		float[] result = DenseMatrixOperations.addMatrix(first, second);

		for (int i = 0; i < result.length; i++) {
			Assert.assertEquals(outPtr.get(i), result[i]);
		}
	}

	@Test
	public void testSubstractMatrices() {
		float[] first = new float[] { 1, 2, 3, 4, 5, 6, 7, 8 };
		float[] second = new float[] { 1, 2, 3, 4, 5, 6, 7, 8 };

		Pointer<Float> matrix1 = allocateFloats(first.length).order(byteOrder), matrix2 = allocateFloats(second.length).order(byteOrder);
		matrix1.setFloats(first);
		matrix2.setFloats(second);
		CLBuffer<Float> m1 = context.createBuffer(Usage.Input, matrix1), m2 = context.createBuffer(Usage.Input, matrix2);

		CLMatrixOperations.substractMatrix(m1, m2, first.length, new OpenCLResources(context, queue, byteOrder, program));
		Pointer<Float> outPtr = m1.read(queue);

		float[] result = DenseMatrixOperations.substractMatrix(first, second);

		for (int i = 0; i < result.length; i++) {
			Assert.assertEquals(outPtr.get(i), result[i]);
		}
	}

	@Test
	public void testMultiplyMatrix() {
		float[] first = new float[] { 1, 2, 3, 4, 5, 6, 7, 8 };
		float alpha = 0.1f;

		Pointer<Float> matrix1 = allocateFloats(first.length).order(byteOrder);
		matrix1.setFloats(first);
		CLBuffer<Float> m1 = context.createBuffer(Usage.InputOutput, matrix1);

		CLMatrixOperations.multipleMatrix(m1, first.length, alpha, new OpenCLResources(context, queue, byteOrder, program));
		Pointer<Float> outPtr = m1.read(queue);

		float[] result = DenseMatrixOperations.multiplyMatrix(first, alpha);

		for (int i = 0; i < result.length; i++) {
			Assert.assertEquals(outPtr.get(i), result[i]);
		}
	}

	@Test
	public void testMaskMatrix() {
		float[] first = new float[] { 1, 2, 3, 4, 5, 6, 7, 8 };
		float[] mask = new float[] { 0, 0, 0, 0, 1, 1, 0, 0 };

		Pointer<Float> matrix1 = allocateFloats(first.length).order(byteOrder), matrix2 = allocateFloats(mask.length).order(byteOrder);
		matrix1.setFloats(first);
		matrix2.setFloats(mask);
		CLBuffer<Float> m1 = context.createBuffer(Usage.Input, matrix1), m2 = context.createBuffer(Usage.Input, matrix2);

		CLMatrixOperations.maskMatrix(m1, m2, first.length, new OpenCLResources(context, queue, byteOrder, program));
		Pointer<Float> outPtr = m1.read(queue);

		for (int i = 0; i < first.length; i++) {
			if (mask[i] == 0) {
				Assert.assertEquals(0f, outPtr.get(i));
			} else if (mask[i] == 1) {
				Assert.assertEquals(first[i], outPtr.get(i));
			}
		}
	}

	@Test
	public void testApplySigmoid() {
		float[] first = new float[] { -5, -4, -3, -2, -1, 0, 1, 2, 3, 4, 5 };

		Pointer<Float> matrix1 = allocateFloats(first.length).order(byteOrder);
		matrix1.setFloats(first);
		CLBuffer<Float> m1 = context.createBuffer(Usage.InputOutput, matrix1);

		CLMatrixOperations.applySigmoid(m1, 11, 1, new OpenCLResources(context, queue, byteOrder, program));
		Pointer<Float> outPtr = m1.read(queue);

		for (int i = 0; i < first.length; i++) {
			System.out.println(outPtr.get(i));
		}
	}

	@Test
	public void testReduce() {
		System.out.println("test reduction");
		float[] first = new float[] { 1, 2, 3, 4, -1, -2, -3 };

		Pointer<Float> matrix1 = allocateFloats(first.length).order(byteOrder);
		matrix1.setFloats(first);
		CLBuffer<Float> m1 = context.createBuffer(Usage.InputOutput, matrix1);

		CLBuffer out = CLMatrixOperations.reduce(m1, first.length, new OpenCLResources(context, queue, byteOrder, program));
		Pointer<Float> outPtr = out.read(queue);

		System.out.println(outPtr.get(0));
		Assert.assertEquals(outPtr.get(0), 4f);

	}

	@Test
	public void testToBinary() throws IOException {
		System.out.println("testToBinary");
		float[] first = new float[] { 0, 0.1f, 0.5f, 0.5f, 0.5f, 0.5f, 0.5f, 0.5f, 0.8f, 0.9f };

		Pointer<Float> matrix1 = allocateFloats(first.length).order(byteOrder);
		matrix1.setFloats(first);
		CLBuffer<Float> m1 = context.createBuffer(Usage.InputOutput, matrix1);

		Pointer<Integer> random = allocateInts(2 * 5).order(byteOrder);
		ParallelRandom pRand = new ParallelRandom(queue, 2 * 5, System.currentTimeMillis());
		pRand.next(random);

		CLBuffer out = CLMatrixOperations.convertToBinary(m1, 2, 5, random, new OpenCLResources(context, queue, byteOrder, program));
		Pointer<Float> outPtr = out.read(queue);

		for (int i = 0; i < first.length; i++) {
			System.out.println(outPtr.get(i));
		}

	}

	@Test
	public void testMatrixMultiplicationBig() {
		System.out.println("testMatrixMultiplicationCL");

		float[] first = new float[10000];
		float[] second = new float[10000];

		for (int i = 0; i < 10000; i++) {
			first[i] = 1;
			second[i] = 1;
		}

		Pointer<Float> matrix1 = allocateFloats(first.length).order(byteOrder), matrix2 = allocateFloats(second.length).order(byteOrder);
		matrix1.setFloats(first);
		matrix2.setFloats(second);
		CLBuffer<Float> m1 = context.createBuffer(Usage.Input, matrix1), m2 = context.createBuffer(Usage.Input, matrix2);
		CLBuffer<Float> out = context.createFloatBuffer(CLMem.Usage.InputOutput, 2 * 2);

		CLMatrixOperations.multiplyByMatrix(m1, m2, out, 5000, 2, 5000, new OpenCLResources(context, queue, byteOrder, program));
		Pointer<Float> outPtr = out.read(queue);

		for (int i = 0; i < 4; i++) {
			System.out.println(outPtr.get(i));
		}

	}

	@Test
	public void testMatrixMultiplicationSigmoidBig() {
		System.out.println("testMatrixMultiplicationCL");

		float[] first = new float[10000];
		float[] second = new float[10000];

		for (int i = 0; i < 10000; i++) {
			first[i] = 1;
			second[i] = 1;
		}

		Pointer<Float> matrix1 = allocateFloats(first.length).order(byteOrder), matrix2 = allocateFloats(second.length).order(byteOrder);
		matrix1.setFloats(first);
		matrix2.setFloats(second);
		CLBuffer<Float> m1 = context.createBuffer(Usage.Input, matrix1), m2 = context.createBuffer(Usage.Input, matrix2);
		CLBuffer<Float> out = context.createFloatBuffer(CLMem.Usage.InputOutput, 2 * 2);

		CLMatrixOperations.multiplyByMatrixWithSigmoid(m1, m2, out, 5000, 2, 5000, new OpenCLResources(context, queue, byteOrder, program));
		Pointer<Float> outPtr = out.read(queue);

		for (int i = 0; i < 4; i++) {
			System.out.println(outPtr.get(i));
		}

	}
}
