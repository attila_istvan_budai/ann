package com.bme.matirx.operations;

import static org.bridj.Pointer.allocateFloats;

import java.io.IOException;
import java.nio.ByteOrder;

import org.bridj.Pointer;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.bme.data.densematrix.DenseMatrix;
import com.bme.data.densematrix.DenseMatrixOperations;
import com.bme.datamining.ann.rbm.RestrictedBoltzmannMachineOpenCL;
import com.bme.opencl.OpenCLResources;
import com.nativelibs4java.opencl.CLBuffer;
import com.nativelibs4java.opencl.CLContext;
import com.nativelibs4java.opencl.CLEvent;
import com.nativelibs4java.opencl.CLKernel;
import com.nativelibs4java.opencl.CLMem;
import com.nativelibs4java.opencl.CLMem.Usage;
import com.nativelibs4java.opencl.CLProgram;
import com.nativelibs4java.opencl.CLQueue;
import com.nativelibs4java.opencl.JavaCL;
import com.nativelibs4java.opencl.LocalSize;
import com.nativelibs4java.util.IOUtils;

public class CLMatrixMultiplicationTest {
	CLContext context;
	CLQueue queue;
	ByteOrder byteOrder;
	CLProgram program;
	OpenCLResources resources;

	private CLBuffer<Float> m1_23;
	private CLBuffer<Float> m2_42;
	private CLBuffer<Float> out_43;

	private CLBuffer<Float> m3_33;
	private CLBuffer<Float> m4_33;
	private CLBuffer<Float> out_33;

	private DenseMatrix resultMatrix_43;
	private DenseMatrix resultMatrix_33;

	public static final int m1ColumnLength = 2;
	public static final int m1RowLength = 3;
	public static final int m2ColumnLength = 4;
	public static final int m2RowLength = 2;

	public static final int m3ColumnLength = 3;
	public static final int m3RowLength = 3;
	public static final int m4ColumnLength = 3;
	public static final int m4RowLength = 3;

	@BeforeClass
	public void initObjects() throws IOException {
		context = JavaCL.createBestContext();
		queue = context.createDefaultQueue();
		byteOrder = context.getByteOrder();
		String src = IOUtils.readText(RestrictedBoltzmannMachineOpenCL.class.getResource("rbm.cl"));
		program = context.createProgram(src);
		resources = new OpenCLResources(context, queue, byteOrder, program);

		setInputData();

	}

	public void setInputData() {
		float[] first = new float[] { -2, 3, -4, 1, 5, -3 };
		float[] second = new float[] { -2, -1, 6, 7, 8, 9, 10, 0 };

		DenseMatrix firstMatrix = new DenseMatrix(first, 2, 3);
		DenseMatrix secondMatrix = new DenseMatrix(second, 4, 2);

		Pointer<Float> matrix1 = allocateFloats(first.length).order(byteOrder), matrix2 = allocateFloats(second.length).order(byteOrder);
		matrix1.setFloats(first);
		matrix2.setFloats(second);
		m1_23 = context.createBuffer(Usage.Input, matrix1);
		m2_42 = context.createBuffer(Usage.Input, matrix2);
		out_43 = context.createFloatBuffer(CLMem.Usage.InputOutput, 4 * 3);

		float[] result = DenseMatrixOperations.multiplyByMatrix(first, second, 2, 4);
		resultMatrix_43 = new DenseMatrix(result, 4, 3);

		float[] first_33 = new float[] { -4, 5, 2, 3, 4, -3, 1, 6, 7 };
		float[] second_33 = new float[] { 0, 5, -4, 8, 9, -3, 4, -2, 6 };

		Pointer<Float> matrix1_33 = allocateFloats(first_33.length).order(byteOrder), matrix2_33 = allocateFloats(second_33.length).order(byteOrder);
		matrix1_33.setFloats(first_33);
		matrix2_33.setFloats(second_33);
		m3_33 = context.createBuffer(Usage.Input, matrix1_33);
		m4_33 = context.createBuffer(Usage.Input, matrix2_33);
		out_33 = context.createFloatBuffer(CLMem.Usage.InputOutput, 3 * 3);

		float[] result_33 = DenseMatrixOperations.multiplyByMatrix(first_33, second_33, 3, 3);
		resultMatrix_33 = new DenseMatrix(result_33, 3, 3);

	}

	public static void multiplyByMatrix(OpenCLResources res, int[] global_size, int[] local_size, CLKernel kernel) {
		CLQueue queue = res.getQueue();
		queue.finish();
		if (local_size != null) {
			CLEvent addEvt = kernel.enqueueNDRange(queue, global_size, local_size);
		} else {
			CLEvent addEvt = kernel.enqueueNDRange(queue, global_size);
		}

		// matrixMultiplicationKernel.release();
	}

	public void checkResult43() {
		Pointer<Float> outPtr = out_43.read(queue);
		for (int i = 0; i < resultMatrix_43.getData().length; i++) {
			// Assert.assertEquals(outPtr.get(i), resultMatrix_43.getData()[i]);
			System.out.println(outPtr.get(i) + " " + resultMatrix_43.getData()[i]);
		}
	}

	public void checkResult33() {
		Pointer<Float> outPtr = out_33.read(queue);
		for (int i = 0; i < resultMatrix_33.getData().length; i++) {
			// Assert.assertEquals(outPtr.get(i), resultMatrix_43.getData()[i]);
			System.out.println(outPtr.get(i) + " " + resultMatrix_33.getData()[i]);
		}
	}

	@BeforeMethod
	public void refreshMemory() {
		out_43 = context.createFloatBuffer(CLMem.Usage.InputOutput, 4 * 3);
		out_33 = context.createFloatBuffer(CLMem.Usage.InputOutput, 3 * 3);
	}

	@Test
	public void test_mmul() {
		System.out.println("test_mmul");
		CLKernel matrixMultiplicationKernel = resources.createKernel("mmul");
		matrixMultiplicationKernel.setArgs(m2ColumnLength, m1RowLength, m1ColumnLength, m1_23, m2_42, out_43);

		int[] global_size = new int[1];
		global_size[0] = m2ColumnLength;

		int[] localWorkSize = new int[1];
		localWorkSize[0] = m2ColumnLength / 20;

		multiplyByMatrix(resources, global_size, null, matrixMultiplicationKernel);

		checkResult43();

	}

	@Test
	public void test_mmul_squared() {
		System.out.println("test_mmul_squared");

		CLKernel matrixMultiplicationKernel = resources.createKernel("mmul");
		matrixMultiplicationKernel.setArgs(m4ColumnLength, m3RowLength, m3ColumnLength, m3_33, m4_33, out_33);

		int[] global_size = new int[1];
		global_size[0] = m4ColumnLength;

		int[] localWorkSize = new int[1];
		localWorkSize[0] = m4ColumnLength / 20;

		multiplyByMatrix(resources, global_size, null, matrixMultiplicationKernel);

		checkResult33();

	}

	@Test
	public void test_matrixMultiplication() {
		System.out.println("matrixMultiplication");
		CLKernel matrixMultiplicationKernel = resources.createKernel("matrixMultiplication");
		matrixMultiplicationKernel.setArgs(out_43, m1_23, m2_42, m1ColumnLength);

		int[] global_size = new int[2];
		global_size[0] = m2ColumnLength;
		global_size[1] = m1RowLength;

		multiplyByMatrix(resources, global_size, null, matrixMultiplicationKernel);

		checkResult43();

	}

	@Test
	public void test_matrixMultiplication_squared() {
		System.out.println("matrixMultiplication squared");

		CLKernel matrixMultiplicationKernel = resources.createKernel("matrixMultiplication");
		matrixMultiplicationKernel.setArgs(out_33, m3_33, m4_33, m3ColumnLength);

		int[] global_size = new int[2];
		global_size[0] = m4ColumnLength;
		global_size[1] = m3RowLength;

		multiplyByMatrix(resources, global_size, null, matrixMultiplicationKernel);

		checkResult33();

	}

	@Test
	public void test_mmul_local() {
		System.out.println("test_mmul_local");
		CLKernel matrixMultiplicationKernel = resources.createKernel("mmul_local");

		// CLBuffer<Float> localbuffer = context.createFloatBuffer(Usage.Input,
		// m2ColumnLength);
		matrixMultiplicationKernel.setArgs(m2ColumnLength, m1ColumnLength, m1RowLength, m1_23, m2_42, out_43, new LocalSize(m2ColumnLength));

		int[] global_size = new int[1];
		global_size[0] = m2ColumnLength;
		// global_size[1] = m1RowLength;

		int[] localWorkSize = new int[1];
		localWorkSize[0] = m2ColumnLength / 20;

		multiplyByMatrix(resources, global_size, null, matrixMultiplicationKernel);

		checkResult43();

	}

	@Test
	public void test_mmul_local_squared() {
		System.out.println("test_mmul_local_squared");

		CLKernel matrixMultiplicationKernel = resources.createKernel("mmul_local");

		CLBuffer<Float> localbuffer = context.createFloatBuffer(Usage.Input, m2ColumnLength);
		matrixMultiplicationKernel.setArgs(m4ColumnLength, m3RowLength, m3ColumnLength, m3_33, m4_33, out_33, new LocalSize(m2ColumnLength));

		int[] global_size = new int[1];
		global_size[0] = m4ColumnLength;

		int[] localWorkSize = new int[1];
		localWorkSize[0] = m4ColumnLength / 20;

		multiplyByMatrix(resources, global_size, null, matrixMultiplicationKernel);

		checkResult33();

	}

	@Test
	public void test_mmul_private() {
		System.out.println("test_mmul_private");
		CLKernel matrixMultiplicationKernel = resources.createKernel("mmul_private");

		CLBuffer<Float> localbuffer = context.createFloatBuffer(Usage.InputOutput, m2ColumnLength);

		// final int m1ColumnLength = 2;
		// final int m1RowLength = 3;
		// final int m2ColumnLength = 4;
		// final int m2RowLength = 2;
		matrixMultiplicationKernel.setArgs(m1ColumnLength, m2ColumnLength, m1RowLength, m1_23, m2_42, out_43, new LocalSize(m2ColumnLength));

		int[] global_size = new int[1];
		global_size[0] = m2ColumnLength;
		// global_size[1] = 6400;

		int[] localWorkSize = new int[1];
		localWorkSize[0] = 2;

		multiplyByMatrix(resources, global_size, localWorkSize, matrixMultiplicationKernel);

		checkResult43();

	}

	@Test
	public void test_mmul_private_squared() {
		System.out.println("test_mmul_private_squared");

		CLKernel matrixMultiplicationKernel = resources.createKernel("mmul_private");

		CLBuffer<Float> localbuffer = context.createFloatBuffer(Usage.InputOutput, m2ColumnLength);
		matrixMultiplicationKernel.setArgs(m3ColumnLength, m4ColumnLength, m3RowLength, m3_33, m4_33, out_33, new LocalSize(m2ColumnLength));

		int[] global_size = new int[1];
		global_size[0] = m2ColumnLength;

		int[] localWorkSize = new int[1];
		localWorkSize[0] = 2;

		multiplyByMatrix(resources, global_size, localWorkSize, matrixMultiplicationKernel);

		checkResult33();

	}

	@Test
	public void testdotThenSigmoid() {
		System.out.println("matrixMultiplicationWithSigmoid");
		CLKernel matrixMultiplicationKernel = resources.createKernel("matrixMultiplicationWithSigmoid");

		float[] m = new float[] { 1, 1, 1, 1 };
		float[] w = new float[] { 1, 1, 1, 1, 1, 1, 1, 1, 1 };

		Pointer<Float> m1 = allocateFloats(m.length).order(byteOrder);
		Pointer<Float> weight = allocateFloats(w.length).order(byteOrder);

		m1.setFloats(m);
		weight.setFloats(w);
		CLBuffer<Float> mBuffer = context.createBuffer(Usage.Input, m1);
		CLBuffer<Float> weightBuffer = context.createBuffer(Usage.Input, weight);
		CLBuffer<Float> out = context.createFloatBuffer(CLMem.Usage.InputOutput, 2 * 2);

		matrixMultiplicationKernel.setArgs(out, mBuffer, weightBuffer, 2);

		int[] globalSize = new int[] { 3, 2 };

		multiplyByMatrix(resources, globalSize, null, matrixMultiplicationKernel);

		Pointer<Float> outPtr = out.read(resources.getQueue());
		float[] result = outPtr.getFloats();
		for (float num : result)
			System.out.println(num);
	}

	@Test
	public void testAddColumnVectorToMatrix() {
		System.out.println("matrixMultiplicationWithSigmoid");
		CLKernel matrixMultiplicationKernel = resources.createKernel("addToRow");

		float[] m = new float[] { 1, 2, 1, 2 };
		float[] w = new float[] { 2, 4 };

		Pointer<Float> m1 = allocateFloats(m.length).order(byteOrder);
		Pointer<Float> weight = allocateFloats(w.length).order(byteOrder);

		m1.setFloats(m);
		weight.setFloats(w);
		CLBuffer<Float> mBuffer = context.createBuffer(Usage.Input, m1);
		CLBuffer<Float> weightBuffer = context.createBuffer(Usage.Input, weight);

		matrixMultiplicationKernel.setArgs(mBuffer, weightBuffer);

		int[] globalSize = new int[] { 2, 2 };

		multiplyByMatrix(resources, globalSize, null, matrixMultiplicationKernel);

		Pointer<Float> outPtr = mBuffer.read(resources.getQueue());
		float[] result = outPtr.getFloats();
		for (float num : result)
			System.out.println(num);
	}

	@Test
	public void testAddRowVectorToMatrix() {
		System.out.println("matrixMultiplicationWithSigmoid");
		CLKernel matrixMultiplicationKernel = resources.createKernel("addToColumn");

		float[] m = new float[] { 1, 2, 1, 2 };
		float[] w = new float[] { 2, 4 };

		Pointer<Float> m1 = allocateFloats(m.length).order(byteOrder);
		Pointer<Float> weight = allocateFloats(w.length).order(byteOrder);

		m1.setFloats(m);
		weight.setFloats(w);
		CLBuffer<Float> mBuffer = context.createBuffer(Usage.Input, m1);
		CLBuffer<Float> weightBuffer = context.createBuffer(Usage.Input, weight);

		matrixMultiplicationKernel.setArgs(mBuffer, weightBuffer);

		int[] globalSize = new int[] { 2, 2 };

		multiplyByMatrix(resources, globalSize, null, matrixMultiplicationKernel);

		Pointer<Float> outPtr = mBuffer.read(resources.getQueue());
		float[] result = outPtr.getFloats();
		for (float num : result)
			System.out.println(num);
	}

}
