package com.bme.matirx.operations;

import static org.bridj.Pointer.allocateFloats;

import java.io.IOException;
import java.nio.ByteOrder;

import org.bridj.Pointer;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.bme.data.clmatrix.CLMatrix;
import com.bme.data.densematrix.DenseMatrix;
import com.bme.data.densematrix.DenseMatrixOperations;
import com.bme.datamining.ann.rbm.RestrictedBoltzmannMachineOpenCL;
import com.bme.opencl.CLMemoryManager;
import com.bme.opencl.OpenCLResources;
import com.nativelibs4java.opencl.CLBuffer;
import com.nativelibs4java.opencl.CLContext;
import com.nativelibs4java.opencl.CLMem;
import com.nativelibs4java.opencl.CLMem.Usage;
import com.nativelibs4java.opencl.CLProgram;
import com.nativelibs4java.opencl.CLQueue;
import com.nativelibs4java.opencl.JavaCL;
import com.nativelibs4java.util.IOUtils;

public class CLMatrixTest {
	CLContext context;
	CLQueue queue;
	ByteOrder byteOrder;
	CLProgram program;

	@BeforeClass
	public void initObjects() throws IOException {
		context = JavaCL.createBestContext();
		queue = context.createDefaultQueue();
		byteOrder = context.getByteOrder();
		String src = IOUtils.readText(RestrictedBoltzmannMachineOpenCL.class.getResource("rbm.cl"));
		program = context.createProgram(src);
		OpenCLResources resources = new OpenCLResources(context, queue, byteOrder, program);
		CLMemoryManager memoryManager = new CLMemoryManager(resources);

	}

	@Test
	public void testCopyMatrix() {
		float[] testNumbers = new float[] { -2, -1, 6, 7, 8, 9, 10, 0 };
		OpenCLResources resource = new OpenCLResources(context, queue, byteOrder, program);
		Pointer<Float> data = allocateFloats(testNumbers.length).order(byteOrder);
		data.setFloats(testNumbers);
		CLBuffer<Float> buff = context.createBuffer(Usage.Input, data);
		CLMatrix matrixToCopy = new CLMatrix(buff, 2, 3, resource);

		CLMatrix copied = matrixToCopy.copy();
		float[] original = matrixToCopy.getData();
		float[] copydata = copied.getData();

		for (int i = 0; i < original.length; i++) {
			Assert.assertEquals(original[i], copydata[i]);
		}

	}

	@Test
	public void testMatrixMultiplication() {

		float[] first = new float[] { -2, 3, -4, 1, 5, -3 };
		float[] second = new float[] { -2, -1, 6, 7, 8, 9, 10, 0 };

		OpenCLResources resource = new OpenCLResources(context, queue, byteOrder, program);
		Pointer<Float> matrix1 = allocateFloats(first.length).order(byteOrder), matrix2 = allocateFloats(second.length).order(byteOrder);
		matrix1.setFloats(first);
		matrix2.setFloats(second);
		CLBuffer<Float> m1 = context.createBuffer(Usage.Input, matrix1), m2 = context.createBuffer(Usage.Input, matrix2);
		CLMatrix matrixFirst = new CLMatrix(m1, 2, 3, resource);
		CLMatrix matrixSecond = new CLMatrix(m2, 4, 2, resource);

		CLMatrix res = matrixFirst.dot(matrixSecond);

		float[] clresult = res.getData();

		float[] result = DenseMatrixOperations.multiplyByMatrix(first, second, 2, 4);

		for (int i = 0; i < result.length; i++) {
			Assert.assertEquals(clresult[i], result[i]);
		}

	}

	@Test
	public void testMatrixTranspose() {

		float[] first = new float[] { 1, 2, 3, 4, 5, 6, 7, 8 };
		OpenCLResources resource = new OpenCLResources(context, queue, byteOrder, program);
		Pointer<Float> matrix1 = allocateFloats(first.length).order(byteOrder);
		matrix1.setFloats(first);
		CLBuffer<Float> m1 = context.createBuffer(Usage.Input, matrix1);
		CLMatrix matrixFirst = new CLMatrix(m1, 4, 2, resource);
		CLBuffer<Float> out = context.createFloatBuffer(CLMem.Usage.InputOutput, 8);

		CLMatrix res = matrixFirst.transpose();

		float[] clresult = res.getData();

		float[] result = DenseMatrixOperations.transposeMatrix(first, 4);

		for (int i = 0; i < result.length; i++) {
			Assert.assertEquals(clresult[i], result[i]);
		}

	}

	@Test
	public void testAdd() {
		float[] first = new float[] { 5, 4, 3, 2 };
		float[] second = new float[] { 1, 1, 1, 1 };
		OpenCLResources resource = new OpenCLResources(context, queue, byteOrder, program);

		Pointer<Float> matrix1 = allocateFloats(first.length).order(byteOrder), matrix2 = allocateFloats(second.length).order(byteOrder);
		matrix1.setFloats(first);
		matrix2.setFloats(second);
		CLBuffer<Float> m1 = context.createBuffer(Usage.Input, matrix1), m2 = context.createBuffer(Usage.Input, matrix2);
		CLMatrix matrixFirst = new CLMatrix(m1, 2, 2, resource);
		CLMatrix matrixSecond = new CLMatrix(m2, 2, 2, resource);

		CLMatrix res = matrixFirst.add(matrixSecond);

		float[] clresult = res.getData();

		float[] result = DenseMatrixOperations.addMatrix(first, second);

		for (int i = 0; i < result.length; i++) {
			Assert.assertEquals(clresult[i], result[i]);
		}

	}

	@Test
	public void testSubstract() {
		float[] first = new float[] { 5, 4, 3, 2 };
		float[] second = new float[] { 1, 1, 1, 1 };

		OpenCLResources resource = new OpenCLResources(context, queue, byteOrder, program);
		Pointer<Float> matrix1 = allocateFloats(first.length).order(byteOrder), matrix2 = allocateFloats(second.length).order(byteOrder);
		matrix1.setFloats(first);
		matrix2.setFloats(second);
		CLBuffer<Float> m1 = context.createBuffer(Usage.Input, matrix1), m2 = context.createBuffer(Usage.Input, matrix2);
		CLMatrix matrixFirst = new CLMatrix(m1, 2, 2, resource);
		CLMatrix matrixSecond = new CLMatrix(m2, 2, 2, resource);

		CLMatrix res = matrixFirst.substract(matrixSecond);

		float[] clresult = res.getData();

		float[] result = DenseMatrixOperations.substractMatrix(first, second);

		for (int i = 0; i < result.length; i++) {
			Assert.assertEquals(clresult[i], result[i]);
		}

	}

	@Test
	public void testMultiplyByConstant() {
		float[] first = new float[] { 5, 4, 3, 2 };
		float alpha = 3;

		OpenCLResources resource = new OpenCLResources(context, queue, byteOrder, program);
		Pointer<Float> matrix1 = allocateFloats(first.length).order(byteOrder);
		matrix1.setFloats(first);
		CLBuffer<Float> m1 = context.createBuffer(Usage.Input, matrix1);
		CLMatrix matrixFirst = new CLMatrix(m1, 2, 2, resource);

		CLMatrix res = matrixFirst.multiplyWithConstant(alpha);

		float[] clresult = res.getData();

		float[] result = DenseMatrixOperations.multiplyMatrix(first, alpha);

		for (int i = 0; i < result.length; i++) {
			Assert.assertEquals(clresult[i], result[i]);
		}

	}

	@Test
	public void testdotThenSigmoid() {
		System.out.println("Matrix.testdotThenSigmoid");
		int size = 2048;
		float[] first = new float[size * size];
		float[] second = new float[size * size];

		OpenCLResources resource = new OpenCLResources(context, queue, byteOrder, program);
		Pointer<Float> matrix1 = allocateFloats(first.length).order(byteOrder), matrix2 = allocateFloats(second.length).order(byteOrder);
		;
		matrix1.setFloats(first);
		CLBuffer<Float> m1 = context.createBuffer(Usage.Input, matrix1);
		matrix1.setFloats(second);
		CLBuffer<Float> m2 = context.createBuffer(Usage.Input, matrix2);

		CLMatrix matrixFirst = new CLMatrix(m1, size, size, resource);
		CLMatrix matrixSecond = new CLMatrix(m1, size, size, resource);

		long start = System.currentTimeMillis();
		CLMatrix res = matrixFirst.dotThenSigmoid(matrixSecond);

		float[] clresult = res.getData();
		long time = System.currentTimeMillis() - start;
		System.out.println("Execution time is: " + time + " ms");

		for (int i = 0; i < 10; i++) {
			System.out.println(clresult[i]);
		}

	}

	@Test
	public void testdotAndApplySigmoid() {
		int size = 6;
		System.out.println("Matrix.dot + Marix.applysigmoid");
		float[] first = new float[size * size];
		float[] second = new float[size * size];

		OpenCLResources resource = new OpenCLResources(context, queue, byteOrder, program);
		Pointer<Float> matrix1 = allocateFloats(first.length).order(byteOrder), matrix2 = allocateFloats(second.length).order(byteOrder);
		;
		matrix1.setFloats(first);
		CLBuffer<Float> m1 = context.createBuffer(Usage.Input, matrix1);
		matrix1.setFloats(second);
		CLBuffer<Float> m2 = context.createBuffer(Usage.Input, matrix2);

		CLMatrix matrixFirst = new CLMatrix(m1, size, size, resource);
		CLMatrix matrixSecond = new CLMatrix(m1, size, size, resource);
		CLBuffer<Float> out = context.createFloatBuffer(CLMem.Usage.InputOutput, size * size);
		CLBuffer<Float> out2 = context.createFloatBuffer(CLMem.Usage.InputOutput, size * size);

		long start = System.currentTimeMillis();
		CLMatrix res = null;
		// for(int i=0; i<10; i++){
		res = matrixFirst.dot(matrixSecond);
		res = res.applySigmoid();
		// }

		float[] clresult = res.getData();
		long time = System.currentTimeMillis() - start;
		System.out.println("Execution time is: " + time + " ms");

		for (int i = 0; i < 10; i++) {
			System.out.println(clresult[i]);
		}
	}

	@Test
	public void testApplySigmoid() {
		int size = 5;
		System.out.println("testApplySigmoid..");
		float[] first = new float[size * size];
		for (int i = 0; i < first.length; i++) {
			first[i] = 5;
		}

		OpenCLResources resource = new OpenCLResources(context, queue, byteOrder, program);
		Pointer<Float> matrix1 = allocateFloats(first.length).order(byteOrder);
		matrix1.setFloats(first);
		CLBuffer<Float> m1 = context.createBuffer(Usage.InputOutput, matrix1);
		CLMatrix matrixFirst = new CLMatrix(m1, size, size, resource);

		long start = System.currentTimeMillis();
		// for(int i=0; i<10; i++){
		CLMatrix res = matrixFirst.applySigmoid();
		// }

		float[] clresult = res.getData();
		long time = System.currentTimeMillis() - start;
		System.out.println("Execution time is: " + time + " ms");

		for (int i = 0; i < size * size; i++) {
			System.out.println(clresult[i]);
		}
	}

	@Test
	public void testFreeMemory() {
		CLBuffer<Float> m1 = context.createFloatBuffer(Usage.InputOutput, 1);
		OpenCLResources resource = new OpenCLResources(context, queue, byteOrder, program);
		CLMatrix matrixFirst = new CLMatrix(m1, 1, 1, resource);
		matrixFirst.freeMemory();
	}

	@Test
	public void testFreeMemoryError() {
		CLBuffer<Float> m1 = context.createFloatBuffer(Usage.InputOutput, 1);
		OpenCLResources resource = new OpenCLResources(context, queue, byteOrder, program);
		CLMatrix matrixFirst = new CLMatrix(m1, 1, 1, resource);
		m1.release();
		matrixFirst.freeMemory();
	}

	@Test
	public void testConstrcutorWithDenseMatrix() {
		float[] data = new float[] { 5, 4, 3, 2 };
		DenseMatrix matrix = new DenseMatrix(data, 2, 2);
		OpenCLResources resource = new OpenCLResources(context, queue, byteOrder, program);
		CLMatrix matrixFirst = new CLMatrix(matrix, resource);
		float[] mdata = matrixFirst.getData();
		for (int i = 0; i < data.length; i++) {
			Assert.assertEquals(data[i], mdata[i]);
		}
	}

}
