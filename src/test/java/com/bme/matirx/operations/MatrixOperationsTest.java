package com.bme.matirx.operations;

import java.util.Arrays;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.bme.data.densematrix.DenseMatrixOperations;

public class MatrixOperationsTest {
	@Test
	public void getSelectedColumnTest()
	
	{
		float[] array = new float[]{1, 2, 3, 4};
		float[] result = DenseMatrixOperations.getSelectedColumn(array, 0, 2, 2);
		Assert.assertEquals( new float[]{1,3}, result);
		
	}
	
	@Test
	public void outerproductTest()
	{
		float[] row = new float[]{1,2};
		float[] col = new float[]{3,4};
		
		float[] result = DenseMatrixOperations.calculateOuterProduct(row, col);
		float[] expectedResult = new float[]{3, 6, 4, 8};
		
		for(int i=0; i< result.length; i++){
		    Assert.assertEquals(result[i], expectedResult[i]);
		}
		
		
		
	}
	
	@Test
	public void substractTest()
	{
		float[] first = new float[]{5,4,3,2};
		float[] second = new float[]{1,1,1,1};
		
		float[] result = DenseMatrixOperations.substractMatrix(first, second);
		
		
		Assert.assertEquals( new float[]{4,3,2,1}, result);
		
		
	}
	
	@Test
	public void multiplyTest()
	{
		float[] first = new float[]{5,4,3,2};
		float alpha = 3;
		
		float[] result = DenseMatrixOperations.multiplyMatrix(first, alpha);
		
		
		Assert.assertEquals( new float[]{15,12,9,6}, result);
		
		
	}
	
	@Test
	public void multiplyByMatrixTest(){
		
		float[] first = new float[]{-2, 3, -4, 1, 5, -3};
		float[] second = new float[]{-2, -1, 6, 7, 8, 9, 10, 0};
		
		float[] result = DenseMatrixOperations.multiplyByMatrix(first, second, 2, 4);
		
		for(int i=0; i<result.length; i++){
			if(i%4==0) System.out.println();
			System.out.print(result[i] + " ");
		}
	}
	
	@Test
	public void inverseMatrixTest(){
		
		float[] first = new float[]{1, 2, 3, 4, 5, 6, 7, 8};
		
		float[] result = DenseMatrixOperations.transposeMatrix(first, 4);
		
		for(int i=0; i<result.length; i++){
			if(i%2==0) System.out.println();
			System.out.print(result[i] + " ");
		}
	}
}
