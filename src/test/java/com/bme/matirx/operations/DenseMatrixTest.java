package com.bme.matirx.operations;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.testng.annotations.Test;

import com.bme.data.densematrix.DenseMatrix;
import com.bme.data.matrix.MatrixElement;
import com.bme.datamining.filereader.ReadFiles;

public class DenseMatrixTest {
	
	@Test
	public void initializeFromMatrixElementTest(){
		List<MatrixElement> elements = new ArrayList<MatrixElement>();
		elements.add(new MatrixElement(1, 0, 0));
		elements.add(new MatrixElement(2, 1, 0));
		elements.add(new MatrixElement(3, 2, 0));
		elements.add(new MatrixElement(4, 3, 0));
		elements.add(new MatrixElement(5, 0, 2));
		elements.add(new MatrixElement(6, 3, 3));
		
		DenseMatrix matrix = new DenseMatrix(elements, 4, 4);
		System.out.println(matrix);
	}
	
	@Test
	public void initializeFromSparseFileTest() throws IOException{
		long start = System.currentTimeMillis();
		List<MatrixElement> elements = ReadFiles.load("./src/main/resources/ratings.train", " ");
		
		DenseMatrix matrix = new DenseMatrix(elements, 3952,6040);
		
		long time = System.currentTimeMillis() - start;
		System.out.println("Execution time is: " + time + " ms");
		
	}

}
