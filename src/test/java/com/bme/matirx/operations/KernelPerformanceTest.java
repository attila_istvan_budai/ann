package com.bme.matirx.operations;

import static org.bridj.Pointer.allocateFloats;

import java.io.IOException;
import java.nio.ByteOrder;
import java.util.List;

import org.bridj.Pointer;

import com.bme.data.clmatrix.CLMatrix;
import com.bme.data.densematrix.DenseMatrix;
import com.bme.data.matrix.MatrixElement;
import com.bme.datamining.ann.rbm.RestrictedBoltzmannMachineOpenCL;
import com.bme.datamining.ann.weights.Weight;
import com.bme.datamining.filereader.ReadFiles;
import com.bme.opencl.CLMemoryManager;
import com.bme.opencl.OpenCLResources;
import com.nativelibs4java.opencl.CLBuffer;
import com.nativelibs4java.opencl.CLContext;
import com.nativelibs4java.opencl.CLMem.Usage;
import com.nativelibs4java.opencl.CLProgram;
import com.nativelibs4java.opencl.CLQueue;
import com.nativelibs4java.opencl.JavaCL;
import com.nativelibs4java.util.IOUtils;

public class KernelPerformanceTest {

	CLContext context;
	CLQueue queue;
	ByteOrder byteOrder;
	CLProgram program;
	OpenCLResources resources;
	CLMatrix testDataSet;

	public static void main(String[] args) throws IOException {
		System.out.println("Performance test started");
		KernelPerformanceTest test = new KernelPerformanceTest();
		test.initObjects();

		test.testMatrixTranspose();
		test.testMatrixCopy();
		test.testDot();
		test.testDotV2();

		test.testDotThenSigmoid();
		test.testDotThenSigmoidV2();
	}

	public void initObjects() throws IOException {
		context = JavaCL.createBestContext();
		queue = context.createDefaultQueue();
		byteOrder = context.getByteOrder();
		String src = IOUtils.readText(RestrictedBoltzmannMachineOpenCL.class.getResource("rbm.cl"));
		program = context.createProgram(src);
		resources = new OpenCLResources(context, queue, byteOrder, program);
		CLMemoryManager memoryManager = new CLMemoryManager(resources);

	}

	public CLMatrix convertArrayIntoClMatrix(float[] array, int col, int row, OpenCLResources resources) {
		Pointer<Float> oPointer = allocateFloats(array.length).order(resources.getByteOrder());
		oPointer.setFloats(array);
		CLBuffer<Float> outBuffer = resources.getContext().createBuffer(Usage.InputOutput, oPointer);
		CLMatrix out = new CLMatrix(outBuffer, col, row, resources);
		return out;
	}

	public void loadTestData() throws IOException {
		RestrictedBoltzmannMachineOpenCL rbm = new RestrictedBoltzmannMachineOpenCL();
		List<MatrixElement> elements = ReadFiles.load("./src/main/resources/ratings.train", " ");
		DenseMatrix matrix = new DenseMatrix(elements, 3952, 6040);
		testDataSet = convertArrayIntoClMatrix(matrix.getData(), 3952, 6040, resources);

	}

	public CLMatrix loadWeight() {
		float[] w = new float[3952 * 100];
		Weight weightVec = new Weight(w);
		weightVec.initializeNormalDistributedWeight();
		w = weightVec.getWeight();

		return convertArrayIntoClMatrix(w, 100, 3952, resources);
	}

	public void testMatrixTranspose() throws IOException {
		loadTestData();
		long start = System.currentTimeMillis();
		CLMatrix res = testDataSet.transpose();

		float[] clresult = res.getData();
		long time = System.currentTimeMillis() - start;
		System.out.print("testMatrixTranspose ");
		System.out.println("Execution time is: " + time + " ms");

	}

	public void testMatrixCopy() throws IOException {
		loadTestData();
		long start = System.currentTimeMillis();
		CLMatrix res = testDataSet.copy();

		float[] clresult = res.getData();
		long time = System.currentTimeMillis() - start;
		System.out.print("testMatrixCopy ");
		System.out.println("Execution time is: " + time + " ms");

	}

	public void testDot() throws IOException {
		loadTestData();
		CLMatrix w = loadWeight();
		long start = System.currentTimeMillis();
		CLMatrix res = testDataSet.dot(w).applySigmoid();

		float[] clresult = res.getData();
		long time = System.currentTimeMillis() - start;
		System.out.print("testDot ");
		System.out.println("Execution time is: " + time + " ms");

	}

	public void testDotV2() throws IOException {
		loadTestData();
		CLMatrix w = loadWeight();
		long start = System.currentTimeMillis();
		CLMatrix res = testDataSet.dotV2(w).applySigmoid();

		float[] clresult = res.getData();
		long time = System.currentTimeMillis() - start;
		System.out.print("testDotV2 ");
		System.out.println("Execution time is: " + time + " ms");

	}

	public void testDotThenSigmoidV2() throws IOException {
		loadTestData();
		CLMatrix w = loadWeight();
		long start = System.currentTimeMillis();
		CLMatrix res = testDataSet.dotThenSigmoidV2(w);

		float[] clresult = res.getData();
		long time = System.currentTimeMillis() - start;
		System.out.print("testDotThenSigmoidV2 ");
		System.out.println("Execution time is: " + time + " ms");

	}

	public void testDotThenSigmoid() throws IOException {
		loadTestData();
		CLMatrix w = loadWeight();
		long start = System.currentTimeMillis();
		CLMatrix res = testDataSet.dotThenSigmoid(w);

		float[] clresult = res.getData();
		long time = System.currentTimeMillis() - start;
		System.out.print("testDotThenSigmoid ");
		System.out.println("Execution time is: " + time + " ms");

	}

}
