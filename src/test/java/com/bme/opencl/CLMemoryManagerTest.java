package com.bme.opencl;

import static org.bridj.Pointer.allocateFloats;

import java.io.IOException;
import java.nio.ByteOrder;

import junit.framework.Assert;

import org.bridj.Pointer;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.bme.datamining.ann.rbm.RestrictedBoltzmannMachineOpenCL;
import com.nativelibs4java.opencl.CLBuffer;
import com.nativelibs4java.opencl.CLContext;
import com.nativelibs4java.opencl.CLEvent;
import com.nativelibs4java.opencl.CLKernel;
import com.nativelibs4java.opencl.CLMem.Usage;
import com.nativelibs4java.opencl.CLProgram;
import com.nativelibs4java.opencl.CLQueue;
import com.nativelibs4java.opencl.JavaCL;
import com.nativelibs4java.util.IOUtils;

public class CLMemoryManagerTest {
	CLContext context;
	CLQueue queue;
	ByteOrder byteOrder;
	CLProgram program;
	OpenCLResources resources;

	@BeforeClass
	public void initObjects() throws IOException {
		context = JavaCL.createBestContext();
		queue = context.createDefaultQueue();
		byteOrder = context.getByteOrder();
		String src = IOUtils.readText(RestrictedBoltzmannMachineOpenCL.class.getResource("rbm.cl"));
		program = context.createProgram(src);
		resources = new OpenCLResources(context, queue, byteOrder, program);
	}

	// @Test
	public void testMemoryUsage() {
		float[] data = new float[10000];
		Pointer<Float> dataPointer = allocateFloats(data.length).order(byteOrder);
		dataPointer.setFloats(data);
		for (int i = 0; i < 100; i++) {
			CLBuffer<Float> m1 = context.createBuffer(Usage.Input, dataPointer);
			int[] global_size = new int[1];
			global_size[0] = data.length;
			CLKernel applySigmoidKernel = program.createKernel("applySigmoidFunction");
			applySigmoidKernel.setArgs(m1);
			CLEvent addEvt = applySigmoidKernel.enqueueNDRange(queue, global_size);
			System.out.println(i);
		}

	}

	@Test
	public void searchForBufferTest() {
		CLMemoryManager memoryManager = new CLMemoryManager(resources);
		CLBuffer<Float> buffer = resources.getContext().createFloatBuffer(Usage.Input, 100);
		memoryManager.addBuffer(buffer);

		Assert.assertNotNull(memoryManager.searchForBuffer(buffer));

	}

	@Test
	public void searchForBufferBySizeTest() {
		CLMemoryManager memoryManager = new CLMemoryManager(resources);
		CLBuffer<Float> buffer = resources.getContext().createFloatBuffer(Usage.Input, 100);
		memoryManager.addBuffer(buffer);

		Assert.assertNotNull(memoryManager.searchForBufferBySize(100));

	}

	@Test
	public void inactivateTest() {
		CLMemoryManager memoryManager = new CLMemoryManager(resources);
		CLBuffer<Float> buffer = resources.getContext().createFloatBuffer(Usage.Input, 100);
		memoryManager.addBuffer(buffer);

		Assert.assertNotNull(memoryManager.searchForBufferBySize(100));

		memoryManager.inactivateBuffer(buffer);

		Assert.assertNotNull(memoryManager.searchForBufferBySize(100));

	}

	@Test
	public void createBufferWithPointerTest() {
		CLMemoryManager memoryManager = new CLMemoryManager(resources);
		float[] data = new float[100];
		Pointer<Float> dataPointer = allocateFloats(data.length).order(byteOrder);
		CLBuffer<Float> buffer1 = memoryManager.createCLBuffer(dataPointer);
		memoryManager.inactivateBuffer(buffer1);
		CLBuffer<Float> buffer2 = memoryManager.createCLBuffer(dataPointer);

		Assert.assertTrue(buffer1 == buffer2);

	}
}
