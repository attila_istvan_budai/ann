package com.bme.datamining.filewriter;

import java.io.IOException;

import org.testng.annotations.Test;

public class TestSaveFloatArray {

	@Test
	public void readAndWriteTest() throws IOException {
		float[] data = new float[] { 1f, 2f, 3f, 4f, 5f, 6f, 7f, 8f };
		SaveFloatArray.storeOO(data, "testWriteFile.out");
		float[] dataCopy = SaveFloatArray.readOO(data.length, "testWriteFile.out");

		for (int i = 0; i < data.length; i++) {
			System.out.println(data[i] + " " + dataCopy[i]);
		}
	}
}
