package com.bme.datamining.filewriter;

import org.testng.annotations.Test;

import com.bme.datamining.ann.dnn.DNNOpenCL;
import com.bme.datamining.ann.dnn.DNNOpenCL.DNNBuilder;
import com.bme.datamining.filereader.LoadModel;
import com.bme.datamining.svm.SVM;

public class SaveModelTest {

	@Test
	public void saveModelTest() throws Exception {

		DNNBuilder dnnBuilder = new DNNBuilder();
		DNNOpenCL dnn = dnnBuilder.addFirstLayer(3, 6).addLayer(3).create();

		float[] input = new float[] //
		{ 1, 1, 0, 0, 0, 0, //
				1, 1, 0, 0, 0, 0, //
				0, 0, 1, 1, 0, 0, //
				0, 0, 1, 1, 0, 0, //
				0, 0, 0, 0, 1, 1, //
				0, 0, 0, 0, 1, 1 };

		float[] w = dnn.train(input, 6, 5000, 0.1f, 0.5f, 50, 0.01f, 6, null);

		SaveModel.saveModelToName(dnn, "testFolder", null);

	}

	@Test
	public void saveModelWithSVMTest() throws Exception {

		DNNBuilder dnnBuilder = new DNNBuilder();
		DNNOpenCL dnn = dnnBuilder.addFirstLayer(3, 6).addLayer(3).addSVMToTopLayer().create();

		float[] input = new float[] //
		{ 1, 1, 0, 0, 0, 0, //
				1, 1, 0, 0, 0, 0, //
				0, 0, 1, 1, 0, 0, //
				0, 0, 1, 1, 0, 0, //
				0, 0, 0, 0, 1, 1, //
				0, 0, 0, 0, 1, 1 };

		float[] labels = { 0, 0, 1, 1, 2, 2 };
		float[] w = dnn.train(input, 6, 5000, 0.1f, 0.5f, 50, 0.01f, 6, labels);

		SaveModel.saveModelToName(dnn, "testFolderSVM", null);

	}

	@Test
	public void loadModelTest() throws Exception {

		DNNOpenCL dnn;

		for (int i = 0; i < 1; i++) {
			dnn = LoadModel.loadModel("testFolderSVM");

			float[] input = new float[] //
			{ 1, 1, 0, 0, 0, 0, //
					1, 1, 0, 0, 0, 0, //
					0, 0, 1, 1, 0, 0, //
					0, 0, 1, 1, 0, 0, //
					0, 0, 0, 0, 1, 1, //
					0, 0, 0, 0, 1, 1 };

			SVM svm = dnn.getSvm();
			float[] testset = dnn.calculateHiddenLayer(input, 6);
			float[] labels = { 0, 0, 1, 1, 2, 2 };
			svm.evaluateLibLinear(testset, labels, 6, 3, 3, true);
		}

	}
}
