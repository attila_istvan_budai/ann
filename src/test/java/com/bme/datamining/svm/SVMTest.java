package com.bme.datamining.svm;

import java.util.Random;

import libsvm.svm_model;

import org.testng.annotations.Test;

public class SVMTest {

	@Test
	public void testSVM() {
		SVM s = new SVM();
		float[] trainingLabels = { 1, 1, 1, 0, 0, 0 };
		float[] trainingSet = { 1, 1, 0, 1, 1, 0, -1, 0, -1, -1, 1, -1 };
		svm_model model = s.learn(trainingSet, trainingLabels, 6, 2, 0.01f, 100000, null);
		s.evaluate(trainingSet, trainingLabels, 6, 2, 2);
	}

	@Test
	public void testSVMLibLinear() {
		SVM s = new SVM();
		float[] trainingLabels = { 1, 1, 1, 0, 0, 0 };
		float[] trainingSet = { 1, 1, 0, 1, 1, 0, -1, 0, -1, -1, 1, -1 };
		s.learnLibLinear(trainingSet, trainingLabels, 6, 2, 0.01f, 100000, null);
		s.evaluateLibLinear(trainingSet, trainingLabels, 6, 2, 2, true);
	}

	@Test
	public void testSVM2() {
		SVM s = new SVM();
		Random rand = new Random();
		float[] trainingLabels = new float[100];
		float[] trainingSet = new float[200];
		for (int i = 0; i < 100; i++) {
			int label = i % 4;
			trainingLabels[i] = label;

			int c1 = 1, c2 = 1;

			if (label == 1 || label == 3) {
				c1 = -1;
			}
			if (label == 2 || label == 3) {
				c2 = -1;
			}

			float dim1 = rand.nextFloat() * c1;
			float dim2 = rand.nextFloat() * c2;

			trainingSet[i * 2] = dim1;
			trainingSet[i * 2 + 1] = dim2;
		}

		svm_model model = s.learn(trainingSet, trainingLabels, 100, 2, 0.01f, 100000, null);
		s.evaluate(trainingSet, trainingLabels, 100, 2, 4);
	}

	@Test
	public void testSVMLibLinear2() {
		SVM s = new SVM();
		Random rand = new Random();
		float[] trainingLabels = new float[60000];
		float[] trainingSet = new float[120000];
		for (int i = 0; i < 60000; i++) {
			int label = i % 4;
			trainingLabels[i] = label;

			int c1 = 1, c2 = 1;

			if (label == 1 || label == 3) {
				c1 = -1;
			}
			if (label == 2 || label == 3) {
				c2 = -1;
			}

			float dim1 = rand.nextFloat() * c1;
			float dim2 = rand.nextFloat() * c2;

			trainingSet[i * 2] = dim1;
			trainingSet[i * 2 + 1] = dim2;
		}

		s.learnLibLinear(trainingSet, trainingLabels, 60000, 2, 0.01f, 100000, null);
		s.evaluateLibLinear(trainingSet, trainingLabels, 60000, 2, 4, true);
	}
}
