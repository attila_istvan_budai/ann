package com.bme.datamining.ann.rbm;

import static org.bridj.Pointer.allocateFloats;
import static org.bridj.Pointer.allocateInts;

import java.io.IOException;
import java.nio.ByteOrder;
import java.nio.charset.Charset;

import org.bridj.Pointer;
import org.testng.annotations.Test;

import com.bme.data.clmatrix.CLMatrixOperations;
import com.bme.datamining.filereader.ReadFiles;
import com.bme.opencl.OpenCLResources;
import com.nativelibs4java.opencl.CLBuffer;
import com.nativelibs4java.opencl.CLContext;
import com.nativelibs4java.opencl.CLDevice;
import com.nativelibs4java.opencl.CLEvent;
import com.nativelibs4java.opencl.CLException;
import com.nativelibs4java.opencl.CLKernel;
import com.nativelibs4java.opencl.CLMem;
import com.nativelibs4java.opencl.CLMem.Usage;
import com.nativelibs4java.opencl.CLPlatform;
import com.nativelibs4java.opencl.CLProgram;
import com.nativelibs4java.opencl.CLQueue;
import com.nativelibs4java.opencl.JavaCL;
import com.nativelibs4java.opencl.util.ParallelRandom;

public class JavaCLFuntionTest {

	@Test
	public void ListHardweres() {
		CLContext context = JavaCL.createBestContext();
		CLPlatform platform = context.getPlatform();

		for (CLDevice device : platform.listAllDevices(true)) {
			System.out.println("Name: " + device.getName());
			System.out.println("----------------------------------------------------");
			System.out.println("Vendor: " + device.getVendor());
			System.out.println("Driver version: " + device.getDriverVersion());
			System.out.println("Max workitem size: ");
			for (long workitemSize : device.getMaxWorkItemSizes()) {
				System.out.println(workitemSize);
			}

		}

	}

	@Test
	public void TestAddumbersWithTwoKernels() {
		System.out.println("TestAddumbersWithTwoKernels");
		CLContext context = JavaCL.createBestContext();
		CLQueue queue = context.createDefaultQueue();
		ByteOrder byteOrder = context.getByteOrder();

		int sizeOfArrays = 10000000;
		Pointer<Float> firstArray = allocateFloats(sizeOfArrays).order(byteOrder), secondArray = allocateFloats(sizeOfArrays).order(byteOrder);

		for (int i = 0; i < sizeOfArrays; i++) {
			firstArray.set(i, 2.0f);
			secondArray.set(i, 2.0f);
		}

		int[] global_size = new int[1];
		global_size[0] = sizeOfArrays;

		CLBuffer<Float> a = context.createBuffer(Usage.Input, firstArray);
		CLBuffer<Float> b = context.createBuffer(Usage.Input, secondArray);

		// Create an OpenCL output buffer :
		CLBuffer<Float> out = context.createFloatBuffer(CLMem.Usage.InputOutput, sizeOfArrays);

		// Read the program sources and compile them :
		String src;
		try {
			src = ReadFiles.readFileToString("./src/main/resources/test.cl", Charset.defaultCharset());
			CLProgram program = context.createProgram(src);

			long start = System.currentTimeMillis();
			// Get and call the kernel :
			CLKernel addArraysKernel = program.createKernel("addTwoArrays");
			addArraysKernel.setArgs(a, b, out);
			CLKernel appendtoArraysKernel = program.createKernel("appendToArray");
			appendtoArraysKernel.setArgs(b, out);

			CLEvent addEvt = addArraysKernel.enqueueNDRange(queue, global_size);
			CLEvent appendEvt = appendtoArraysKernel.enqueueNDRange(queue, global_size);

			Pointer<Float> outPtr = out.read(queue, appendEvt); // blocks until
																// add_floats
																// finished
			long time = System.currentTimeMillis() - start;
			System.out.println("Execution time is: " + time + " ms");
			// Print the first 10 output values :
			for (int i = 0; i < 10 && i < sizeOfArrays; i++)
				System.out.println(outPtr.get(i));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	public void TestAddumbersWithOneKernel() {
		System.out.println("TestAddumbersWithOneKernel //5000*5000 size");
		CLContext context = JavaCL.createBestContext();
		context.setCacheBinaries(false);
		CLQueue queue = context.createDefaultQueue();
		ByteOrder byteOrder = context.getByteOrder();

		int sizeOfArrays = 5000;
		Pointer<Float> firstArray = allocateFloats(sizeOfArrays).order(byteOrder), secondArray = allocateFloats(sizeOfArrays).order(byteOrder);

		for (int i = 0; i < sizeOfArrays; i++) {
			firstArray.set(i, 1.0f);
			secondArray.set(i, 1.0f);
		}

		int[] global_size = new int[1];
		global_size[0] = sizeOfArrays;

		CLBuffer<Float> a = context.createBuffer(Usage.Input, firstArray);
		CLBuffer<Float> b = context.createBuffer(Usage.Input, secondArray);

		// Create an OpenCL output buffer :
		CLBuffer<Float> out = context.createFloatBuffer(CLMem.Usage.InputOutput, sizeOfArrays);

		// Read the program sources and compile them :
		String src;
		try {
			src = ReadFiles.readFileToString("./src/main/resources/test.cl", Charset.defaultCharset());
			CLProgram program = context.createProgram(src);

			// Get and call the kernel :
			CLKernel addFloatsKernel = program.createKernel("addTwoArrays");
			long start = System.currentTimeMillis();
			addFloatsKernel.setArgs(a, b, out);

			CLEvent addEvt = addFloatsKernel.enqueueNDRange(queue, global_size);

			Pointer<Float> outPtr = out.read(queue, addEvt); // blocks until
																// add_floats
																// finished
			long time = System.currentTimeMillis() - start;
			System.out.println("Execution time is: " + time + " ms");
			// Print the first 10 output values :
			for (int i = 0; i < 10 && i < sizeOfArrays; i++)
				System.out.println(outPtr.get(i));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	public void TestPowerArrayKernel() {
		System.out.println("TestPowerArrayKernel");
		CLContext context = JavaCL.createBestContext();
		CLQueue queue = context.createDefaultQueue();
		ByteOrder byteOrder = context.getByteOrder();

		int sizeOfArrays = 10000000;
		Pointer<Float> firstArray = allocateFloats(sizeOfArrays).order(byteOrder);

		for (int i = 0; i < sizeOfArrays; i++) {
			firstArray.set(i, 2.0f);
		}

		int[] global_size = new int[1];
		global_size[0] = sizeOfArrays;

		CLBuffer<Float> a = context.createBuffer(Usage.InputOutput, firstArray);

		// Read the program sources and compile them :
		String src;
		try {
			src = ReadFiles.readFileToString("./src/main/resources/test.cl", Charset.defaultCharset());
			CLProgram program = context.createProgram(src);

			long start = System.currentTimeMillis();
			// Get and call the kernel :
			CLKernel powArrayKernel = program.createKernel("powArray");
			powArrayKernel.setArgs(a, 10);

			CLEvent powEvt = powArrayKernel.enqueueNDRange(queue, global_size);

			Pointer<Float> outPtr = a.read(queue, powEvt); // blocks until
															// add_floats
															// finished
			long time = System.currentTimeMillis() - start;
			System.out.println("Execution time is: " + time + " ms");
			// Print the first 10 output values :
			for (int i = 0; i < 10 && i < sizeOfArrays; i++)
				System.out.println(outPtr.get(i));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	public void TestPowerArrayFromHost() {
		System.out.println("TestPowerArrayFromHost");
		CLContext context = JavaCL.createBestContext();
		CLQueue queue = context.createDefaultQueue();
		ByteOrder byteOrder = context.getByteOrder();

		int sizeOfArrays = 5000 * 5000;
		Pointer<Float> firstArray = allocateFloats(sizeOfArrays).order(byteOrder);

		for (int i = 0; i < sizeOfArrays; i++) {
			firstArray.set(i, 2.0f);
		}

		int[] global_size = new int[1];
		global_size[0] = sizeOfArrays;

		CLBuffer<Float> a = context.createBuffer(Usage.InputOutput, firstArray);

		// Read the program sources and compile them :
		String src;
		try {
			src = ReadFiles.readFileToString("./src/main/resources/test.cl", Charset.defaultCharset());
			CLProgram program = context.createProgram(src);

			// Get and call the kernel :
			CLKernel powArrayKernel = program.createKernel("multipleArray");
			powArrayKernel.setArgs(a, 2.0f);
			CLEvent powEvt = null;
			// for(int i=0; i<10; i++){
			long start = System.currentTimeMillis();
			powEvt = powArrayKernel.enqueueNDRange(queue, global_size);
			// }

			Pointer<Float> outPtr = a.read(queue, powEvt); // blocks until
															// add_floats
															// finished
			long time = System.currentTimeMillis() - start;
			System.out.println("Execution time is: " + time + " ms");
			// Print the first 10 output values :
			for (int i = 0; i < 10 && i < sizeOfArrays; i++)
				System.out.println(outPtr.get(i));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	public void testApplySigmoidFunction() {
		System.out.println("applySigmoidFunction test");
		CLContext context = JavaCL.createBestContext();
		CLQueue queue = context.createDefaultQueue();
		ByteOrder byteOrder = context.getByteOrder();

		int sizeOfArrays = 1024 * 1024;
		Pointer<Float> firstArray = allocateFloats(sizeOfArrays).order(byteOrder);

		for (int i = 0; i < sizeOfArrays; i++) {
			firstArray.set(i, 2.0f);
		}

		int[] global_size = new int[1];
		global_size[0] = sizeOfArrays / 4;

		CLBuffer<Float> a = context.createBuffer(Usage.InputOutput, firstArray);

		// Read the program sources and compile them :
		String src;
		try {
			src = ReadFiles.readFileToString("./src/main/resources/rbm.cl", Charset.defaultCharset());
			CLProgram program = context.createProgram(src);

			// Get and call the kernel :
			CLKernel powArrayKernel = program.createKernel("applySigmoidFunction");
			powArrayKernel.setArgs(a);
			CLEvent powEvt = null;
			// for(int i=0; i<10; i++){
			long start = System.currentTimeMillis();
			powEvt = powArrayKernel.enqueueNDRange(queue, global_size);
			// }

			Pointer<Float> outPtr = a.read(queue, powEvt); // blocks until
															// add_floats
															// finished
			long time = System.currentTimeMillis() - start;
			System.out.println("Execution time is: " + time + " ms");
			// Print the first 10 output values :
			for (int i = 0; i < 10 && i < sizeOfArrays; i++)
				System.out.println(outPtr.get(i));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	public void testMatrixMultiplication() throws IOException {
		System.out.println("testMatrixMultiplicationCL..   ");

		CLContext context;
		CLQueue queue;
		ByteOrder byteOrder;
		CLProgram program;

		context = JavaCL.createBestContext();
		queue = context.createDefaultQueue();
		byteOrder = context.getByteOrder();
		String src = ReadFiles.readFileToString("./src/main/resources/rbm.cl", Charset.defaultCharset());
		program = context.createProgram(src);

		int sizeOfArrays = 1000;
		Pointer<Float> firstArray = allocateFloats(sizeOfArrays * sizeOfArrays).order(byteOrder), secondArray = allocateFloats(sizeOfArrays * sizeOfArrays).order(byteOrder);

		for (int i = 0; i < sizeOfArrays * sizeOfArrays; i++) {
			firstArray.set(i, 1.0f);
			secondArray.set(i, 1.0f);
		}

		CLBuffer<Float> m1 = context.createBuffer(Usage.Input, firstArray), m2 = context.createBuffer(Usage.Input, secondArray);
		CLBuffer<Float> out = context.createFloatBuffer(CLMem.Usage.InputOutput, sizeOfArrays * sizeOfArrays);

		long start = System.currentTimeMillis();
		try {
			CLMatrixOperations.multiplyByMatrix(m1, m2, out, sizeOfArrays, sizeOfArrays, sizeOfArrays, new OpenCLResources(context, queue, byteOrder, program));
		} catch (CLException ex) {
			System.out.println(ex.getCode());
			System.out.println(ex.getMessage());
		}
		Pointer<Float> outPtr = out.read(queue);

		System.out.println("Execution time is: " + (System.currentTimeMillis() - start) + " ms");

		for (int i = 0; i < 10; i++) {
			System.out.println(outPtr.get(i) + " ");
		}

	}

	@Test
	public void testMatrixMultiplicationOptimized() {
		System.out.println("MAtrixMultiplication - > matrix_mult");
		CLContext context = JavaCL.createBestContext();
		CLQueue queue = context.createDefaultQueue();
		ByteOrder byteOrder = context.getByteOrder();

		int sizeOfArrays = 1000;

		Pointer<Float> firstArray = allocateFloats(sizeOfArrays * sizeOfArrays).order(byteOrder), secondArray = allocateFloats(sizeOfArrays * sizeOfArrays).order(byteOrder);

		for (int i = 0; i < sizeOfArrays * sizeOfArrays; i++) {
			firstArray.set(i, 1.0f);
			secondArray.set(i, 1.0f);
		}

		int[] global_size = new int[1];
		global_size[0] = sizeOfArrays;

		CLBuffer<Float> m1 = context.createBuffer(Usage.Input, firstArray), m2 = context.createBuffer(Usage.Input, secondArray);
		CLBuffer<Float> out = context.createFloatBuffer(CLMem.Usage.InputOutput, sizeOfArrays * sizeOfArrays);

		// Read the program sources and compile them :
		String src;
		try {
			src = ReadFiles.readFileToString("./src/main/resources/rbm.cl", Charset.defaultCharset());
			CLProgram program = context.createProgram(src);

			// Get and call the kernel :
			CLKernel powArrayKernel = program.createKernel("matrix_mult");
			powArrayKernel.setArgs(m1, m2, out);
			// for(int i=0; i<10; i++){
			long start = System.currentTimeMillis();
			CLEvent powEvt = powArrayKernel.enqueueNDRange(queue, global_size);
			powEvt.waitFor();
			// }

			long time = System.currentTimeMillis() - start;
			Pointer<Float> outPtr = out.read(queue, powEvt); // blocks until
																// add_floats
																// finished

			System.out.println("Execution time is: " + time + " ms");
			// Print the first 10 output values :
			for (int i = 0; i < 10 && i < sizeOfArrays; i++)
				System.out.println(outPtr.get(i));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// @Test
	public void testMatrixMTranspose() {
		System.out.println("Tranpose..");
		CLContext context = JavaCL.createBestContext();
		CLQueue queue = context.createDefaultQueue();
		ByteOrder byteOrder = context.getByteOrder();

		int sizeOfArrays = 1024;

		Pointer<Float> firstArray = allocateFloats(sizeOfArrays * sizeOfArrays).order(byteOrder), secondArray = allocateFloats(sizeOfArrays * sizeOfArrays).order(byteOrder);

		for (int i = 0; i < sizeOfArrays * sizeOfArrays; i++) {
			firstArray.set(i, 1.0f);
		}

		int[] global_size = new int[2];
		global_size[0] = sizeOfArrays;
		global_size[1] = sizeOfArrays;

		CLBuffer<Float> m1 = context.createBuffer(Usage.InputOutput, firstArray);
		CLBuffer<Float> out = context.createFloatBuffer(CLMem.Usage.InputOutput, sizeOfArrays * sizeOfArrays);

		// Read the program sources and compile them :
		String src;
		try {
			src = ReadFiles.readFileToString("./src/main/resources/rbm.cl", Charset.defaultCharset());
			CLProgram program = context.createProgram(src);

			// Get and call the kernel :
			CLKernel powArrayKernel = program.createKernel("Transpose");
			powArrayKernel.setArgs(out, m1, sizeOfArrays, sizeOfArrays);
			// for(int i=0; i<10; i++){
			long start = System.currentTimeMillis();
			CLEvent powEvt = powArrayKernel.enqueueNDRange(queue, global_size);
			powEvt.waitFor();
			// }

			long time = System.currentTimeMillis() - start;
			Pointer<Float> outPtr = out.read(queue, powEvt); // blocks until
																// add_floats
																// finished

			System.out.println("Execution time is: " + time + " ms");
			// Print the first 10 output values :
			for (int i = 0; i < 10 && i < sizeOfArrays; i++)
				System.out.println(outPtr.get(i));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Test
	public void randomTest() throws IOException {
		CLContext context = JavaCL.createBestContext();
		CLQueue queue = context.createDefaultQueue();
		ByteOrder byteOrder = context.getByteOrder();
		ParallelRandom random = new ParallelRandom(queue, 30, System.currentTimeMillis());
		Pointer<Integer> output = allocateInts(30).order(byteOrder);
		float[] array = new float[30];
		for (int i = 0; i < array.length; i++) {
			if (i < 10) {
				array[i] = 0.4f;
			} else if (i < 20) {
				array[i] = 0.6f;
			}
		}
		random.next(output);
		for (Integer i : output) {
			System.out.println((((double) i / Integer.MAX_VALUE) + 1) / 2);
		}

		for (int i = 0; i < array.length; i++) {
			if (array[i] < ((((double) output.get(i) / Integer.MAX_VALUE) + 1) / 2)) {
				System.out.print(0 + " ");
			} else {
				System.out.print(1 + " ");
			}
		}

	}
}
