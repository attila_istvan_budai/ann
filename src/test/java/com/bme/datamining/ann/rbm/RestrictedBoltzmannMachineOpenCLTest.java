package com.bme.datamining.ann.rbm;

import org.testng.annotations.Test;

import com.bme.datamining.ann.dnn.ConvulotionalNetwork;
import com.bme.datamining.ann.dnn.ConvulotionalNetwork.ConvulotionalNetworkBuilder;
import com.bme.datamining.ann.dnn.DNNOpenCL;
import com.bme.datamining.ann.dnn.DNNOpenCL.DNNBuilder;
import com.bme.memorytest.RBMObserver;

public class RestrictedBoltzmannMachineOpenCLTest {

	@Test
	public void simpleTest() throws Exception {
		RestrictedBoltzmannMachineOpenCL rbm = new RestrictedBoltzmannMachineOpenCL();

		float[] input = new float[] //
		{ 1, 1, 0, 0, 0, 0, //
				1, 1, 0, 0, 0, 0, //
				0, 0, 1, 1, 0, 0, //
				0, 0, 1, 1, 0, 0, //
				0, 0, 0, 0, 1, 1, //
				0, 0, 0, 0, 1, 1 };

		float[] w = rbm.train(input, 6, 3, 6, 25000, 0.1f, 0.5f, 1000, 0.01f, 6);
		float[] hiddendata = rbm.stepForward(input, w, 6, 3, 6);
		float[] w2 = rbm.train(hiddendata, 3, 6, 6, 25000, 0.1f, 0.5f, 1000, 0.01f, 6);

		arrayToString(rbm.reConstruct(new float[] { 1, 1, 0, 0, 0, 0, }, w, 6, 3, 1));
		arrayToString(rbm.reConstruct(new float[] { 0, 0, 1, 1, 0, 0, }, w, 6, 3, 1));
		arrayToString(rbm.reConstruct(new float[] { 0, 0, 0, 0, 1, 1, }, w, 6, 3, 1));
		System.out.println("------------");
		arrayToString(rbm.reConstruct(new float[] { 1, 1, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 1, 1 }, w, 6, 3, 3));
		System.out.println("------------");

		arrayToString(rbm.reConstruct(new float[] { 1, 1, 0, 0, 0, 0, }, w2, 6, 3, 1));
		arrayToString(rbm.reConstruct(new float[] { 0, 0, 1, 1, 0, 0, }, w2, 6, 3, 1));
		arrayToString(rbm.reConstruct(new float[] { 0, 0, 0, 0, 1, 1, }, w2, 6, 3, 1));
		System.out.println("------------");
		arrayToString(rbm.reConstruct(new float[] { 1, 1, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 1, 1 }, w2, 6, 3, 3));
		System.out.println("------------");

		// float[] d1 = rbm.stepForward(new float[] { 1, 1, 0, 0, 0, 0 }, w, 6,
		// 3, 1);
		// arrayToString(rbm.stepBackward(d1, w, 6, 3, 1));
		// float[] d2 = rbm.stepForward(new float[] { 0, 0, 1, 1, 0, 0, }, w, 6,
		// 3, 1);
		// arrayToString(rbm.stepBackward(d2, w, 6, 3, 1));
		// float[] d3 = rbm.stepForward(new float[] { 0, 0, 0, 0, 1, 1, }, w, 6,
		// 3, 1);
		// arrayToString(rbm.stepBackward(d3, w, 6, 3, 1));
	}

	public void arrayToString(float[] array, int columns) {
		for (int i = 0; i < array.length; i++) {
			if (i != 0 && i % columns == 0) {
				System.out.println();
			}
			System.out.print(array[i] + " ");
		}
		System.out.println();
	}

	public void arrayToString(float[] array) {
		arrayToString(array, array.length);
	}

	@Test
	public void simpleDNNTest() throws Exception {

		DNNBuilder dnnBuilder = new DNNBuilder();
		DNNOpenCL dnn = dnnBuilder.addFirstLayer(4, 8).create();

		float[] input = new float[] //
		{ 1, 1, 0, 0, 0, 0, 0, 0,//
				1, 1, 0, 0, 0, 0, 0, 0,//
				0, 0, 1, 1, 0, 0, 0, 0,// //
				0, 0, 0, 0, 1, 1, 0, 0, //
				0, 0, 0, 0, 1, 1, 0, 0, //
				0, 0, 0, 0, 0, 0, 1, 1, };

		float[] w = dnn.train(input, 6, 20000, 0.1f, 0.5f, 5000, 0.01f, 6, null);

		float[] res = dnn.reconstructData(new float[] { 1, 1, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 1, 1 }, 3);
		arrayToString(res, 8);

		System.out.println("-------------------------");
		res = dnn.reconstructData(new float[] { 1, 1, 0, 0, 0, 0, 0, 0 }, 1);
		arrayToString(res, 8);
		res = dnn.reconstructData(new float[] { 0, 0, 1, 1, 0, 0, 0, 0 }, 1);
		arrayToString(res, 8);
		res = dnn.reconstructData(new float[] { 0, 0, 0, 0, 1, 1, 0, 0 }, 1);
		arrayToString(res, 8);
		res = dnn.reconstructData(new float[] { 0, 0, 0, 0, 0, 0, 1, 1, }, 1);

	}

	@Test
	public void simpleCNNTest() throws Exception {

		ConvulotionalNetworkBuilder cnnBuilder = new ConvulotionalNetworkBuilder();
		ConvulotionalNetwork convNN = cnnBuilder.addFirstLayer(8, 8).addLayer(4).create();

		float[] input = new float[] //
		{ 1, 1, 0, 0, 0, 0, 0, 0,//
				1, 1, 0, 0, 0, 0, 0, 0,//
				0, 0, 1, 1, 0, 0, 0, 0,// //
				0, 0, 0, 0, 1, 1, 0, 0, //
				0, 0, 0, 0, 1, 1, 0, 0, //
				0, 0, 0, 0, 0, 0, 1, 1, };

		RBMObserver observer = new RBMObserver(null);
		convNN.registerObserver(observer);
		float[] w = convNN.train(input, 3, 20000, 0.1f, 0.5f, 5000, 0.01f, 12, 8, 2, 2, 2);

		System.out.println("-------------------------");
		float[] res;

		// float[] res = dnn.reconstructData(new float[] { 1, 1, 0, 0, 0, 0, 0,
		// 0, 1, 1, 0, 0, 0, 0, 0, 0, 1, 1 }, 3);
		// arrayToString(res, 8);
		//
		System.out.println("-------------------------");
		res = convNN.calculateHiddenLayer(new float[] { 1, 1, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0 }, 1);
		arrayToString(res, 8);
		res = convNN.calculateHiddenLayer(new float[] { 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0 }, 1);
		arrayToString(res, 8);
		res = convNN.calculateHiddenLayer(new float[] { 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0 }, 1);
		arrayToString(res, 8);
		res = convNN.calculateHiddenLayer(new float[] { 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 1, 1 }, 1);
		arrayToString(res, 8);
	}

	@Test
	public void simpleDNNTestBig() throws Exception {

		DNNBuilder dnnBuilder = new DNNBuilder();
		DNNOpenCL dnn = dnnBuilder.addFirstLayer(10, 100).create();

		float[] input = new float[500000];
		int i = 0;
		for (; i < 100000; i++) {
			if (i % 100 == 0) {
				input[i] = 255;
			} else if (i % 100 == 1) {
				input[i] = 255;
			}
		}

		for (; i < 200000; i++) {
			if (i % 100 == 2) {
				input[i] = 255;
			} else if (i % 100 == 3) {
				input[i] = 255;
			}
		}

		for (; i < 300000; i++) {
			if (i % 100 == 4) {
				input[i] = 255;
			} else if (i % 100 == 5) {
				input[i] = 255;
			}
		}

		for (; i < 400000; i++) {
			if (i % 100 == 6) {
				input[i] = 255;
			} else if (i % 100 == 7) {
				input[i] = 255;
			}
		}

		input = normalizeData(input);

		float[] w = dnn.train(input, 5000, 20000, 0.1f, 0.5f, 5000, 0.01f, 5000, null);

		float[] testData = new float[100];
		testData[0] = 1;
		testData[1] = 1;

		float[] res = dnn.reconstructData(testData, 1);
		arrayToString(res);
		testData = new float[100];
		testData[2] = 1;
		testData[3] = 1;
		res = dnn.reconstructData(testData, 1);
		arrayToString(res);
		testData = new float[100];
		testData[4] = 1;
		testData[5] = 1;
		res = dnn.reconstructData(testData, 1);
		arrayToString(res);
		testData = new float[100];
		testData[6] = 1;
		testData[7] = 1;
		res = dnn.reconstructData(testData, 1);
		arrayToString(res);
		testData = new float[100];

	}

	public float[] normalizeData(float[] array) {
		for (int i = 0; i < array.length; i++) {
			array[i] /= 256;
		}
		return array;

	}
}
