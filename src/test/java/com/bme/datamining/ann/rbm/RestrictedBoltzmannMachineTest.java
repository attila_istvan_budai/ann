package com.bme.datamining.ann.rbm;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.bme.datamining.ann.activatefunctions.SigmoidFunction;
import com.bme.datamining.ann.neurons.InnerNeuron;
import com.bme.datamining.ann.weights.MultiDimensionalWeight;
import com.bme.datamining.ann.weights.Weight;

public class RestrictedBoltzmannMachineTest {
	RestrictedBoltzmannMachine rbm;

	@BeforeClass
	public void init() {
		rbm = new RestrictedBoltzmannMachine(new InnerNeuron(new SigmoidFunction()));
	}

	@Test
	public void updateWeightTest() {
		Weight w = new Weight(4);
		float[] positiveGradient = { 2, 2, 2, 2 };
		float[] negativeGradient = { 1, 1, 1, 1 };
		float[] expectedResult = { 0.5f, 0.5f, 0.5f, 0.5f };
		w = rbm.updateWeight(w, positiveGradient, negativeGradient, 0.5f);
		for (int i = 0; i < 4; i++) {
			Assert.assertEquals(expectedResult[i], w.get(i));
		}

	}
	
	@Test
	public void calculateHiddenUnitsTest()
	{
		MultiDimensionalWeight weight = new MultiDimensionalWeight(5, 1);
		float[] visibleUnits = { 1, 1, 1, 1 };
		float[] result = rbm.calculateHiddenUnits(visibleUnits, weight, 4, 1);
		Assert.assertEquals(1, result.length);
		Assert.assertEquals(0.5f, result[0]);
	}
	
	@Test
	public void calculateVisibleUnitsTest()
	{
		MultiDimensionalWeight weight = new MultiDimensionalWeight(5, 1);
		float[] visibleUnits = { 1, 1, 1, 1 };
		float[] hiddenUnits = { 1};
		float[] result = rbm.calculateVisibleUnits(visibleUnits, hiddenUnits, weight, 4, 1);
		Assert.assertEquals(4, result.length);
		Assert.assertEquals(0.5f, result[0]);
	}

}
