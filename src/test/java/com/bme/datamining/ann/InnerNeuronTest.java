package com.bme.datamining.ann;


import org.mockito.Matchers;
import org.mockito.Mockito;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.bme.datamining.ann.activatefunctions.SigmoidFunction;
import com.bme.datamining.ann.neurons.InnerNeuron;
import com.bme.datamining.ann.weights.Weight;


public class InnerNeuronTest {

	@Test
	public void testCalculateOutputWithOneValue() {

		InnerNeuron neuron = new InnerNeuron(new SigmoidFunction());
		Weight w = new Weight(3);
		float result = neuron.calculateOutput(new float[] {1f}, w, 0, 1);
		Assert.assertEquals(result, 0.5, 0.01f);
	}
}
