package com.bme.datamining.ann;

import junit.framework.Assert;

import org.testng.annotations.Test;

import com.bme.datamining.ann.weights.MultiDimensionalWeight;
import com.bme.datamining.ann.weights.Weight;

public class MultiDimensionalWeightTest {
	
	@Test
	public void testGetColumn() {
		MultiDimensionalWeight w = new MultiDimensionalWeight(2, 2);
		Weight testWeight = w.getColumn(0);
		Assert.assertEquals(0f, testWeight.get(0));
		Assert.assertEquals(0f, testWeight.get(1));
	}

}
