package com.bme.datamining.ann.dnn;

import org.testng.annotations.Test;

public class ConvulotionalNetworkTest {

	public void arrayToString(float[] array, int columns) {
		for (int i = 0; i < array.length; i++) {
			if (i != 0 && i % columns == 0) {
				System.out.println();
			}
			System.out.print(array[i] + " ");
		}
		System.out.println();
		System.out.println("---------------");
	}

	@Test
	public void test() {
		ConvulotionalNetwork cnn = new ConvulotionalNetwork.ConvulotionalNetworkBuilder().addFirstLayer(10, 10).create();
		float[] data = { 1, 1, 2, 2, 3, 3, 4, 4,//
				5, 5, 6, 6, 7, 7, 8, 8,//
				9, 9, 10, 10, 11, 11, 12, 12,//
				13, 13, 14, 14, 15, 15, 16, 16, //
				21, 21, 22, 22, 23, 23, 24, 24,//
				25, 25, 26, 26, 27, 27, 28, 28,//
				29, 29, 30, 30, 31, 31, 32, 32,//
				33, 33, 34, 34, 35, 35, 36, 36 //
		};
		int columnSize = 8;
		int rowSize = 4;
		int sliceHeight = 4;
		int sliceWidth = 2;
		int slices = cnn.countSlices(columnSize, rowSize, sliceWidth, sliceHeight);
		for (int i = 0; i < slices * 1; i++) {
			float[] res = cnn.getSlice(data, columnSize, rowSize, sliceWidth, sliceHeight, i);
			arrayToString(res, sliceWidth);
		}

		for (int i = 0; i < slices * 1; i++) {
			float[] res = cnn.createDataFromSlices(data, 2, columnSize, rowSize, sliceWidth, sliceHeight, i);
			arrayToString(res, sliceWidth);
		}

	}

}
