package com.bme.datamining.ann.logisticregression;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.bme.datamining.ann.activatefunctions.SigmoidFunction;
import com.bme.datamining.ann.neurons.InnerNeuron;
import com.bme.datamining.ann.weights.Weight;

public class LogisticRegressionTest {

	LogisticRegression logreg;

	@BeforeClass
	public void init() {
		logreg = new LogisticRegression(new InnerNeuron(new SigmoidFunction()));
	}

	@Test
	public void updateWeightTestWithZeroLearningRate() {
		Weight w = new Weight(5);
		float[] error = { 1 };
		float[] exampleset = { 1, 1, 1, 1 };
		logreg.updateWeight(exampleset, w, error, 0, 4, 0);

		for (int i = 0; i < 4; i++) {
			Assert.assertEquals(0f, w.get(i), 0.001);
		}
	}

	@Test
	public void updateWeightTestWithZeroError() {
		Weight w = new Weight(5);
		float[] error = { 0 };
		float[] exampleset = { 1, 1, 1, 1 };
		logreg.updateWeight(exampleset, w, error, 0, 4, 1);

		for (int i = 0; i < 4; i++) {
			Assert.assertEquals(0f, w.get(i), 0.001);
		}
	}

	@Test
	public void logregCLtest() {
		Weight w = new Weight(5);
		float[] trainingLabels = { 1, 1, 1, 0, 0, 0 };
		float[] trainingSet = { 1, 1, 0, 1, 1, 0, -1, 0, -1, -1, 1, -1 };
		float[] result;
		try {
			result = logreg.learnCL(trainingSet, trainingLabels, 6, 2, 0.01f, 100000);
			for (int i = 0; i < result.length; i++) {
				System.out.println(result[i]);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
