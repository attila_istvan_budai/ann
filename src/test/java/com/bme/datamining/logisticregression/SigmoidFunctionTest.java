package com.bme.datamining.logisticregression;


import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.bme.datamining.ann.activatefunctions.SigmoidFunction;

public class SigmoidFunctionTest {
	
    @DataProvider(name = "test1")
    public static Object[][] inputNumbers() {
        return new Object[][] { { 0, 0.5f }, { -5, 0.0f }, { 5, 1.0f } };
    }
    
    @Test(dataProvider = "test1")
	public void testCalculateOutput(float input, float expectedResult) {
		SigmoidFunction func = new SigmoidFunction();
		float result = func.calculateOutput(input);
		Assert.assertEquals(expectedResult, result, 0.01);
	}
    
    @Test
    public void testCalculateOutputLinearity(){
        SigmoidFunction func = new SigmoidFunction();
        float number=-10f;
        float lastnumber=-1;
        float currentnumber;
        for(int i=0; i<100; i++){
            number+=0.1f;
            currentnumber = func.calculateOutput(number);
            Assert.assertTrue(currentnumber>=lastnumber);
            lastnumber = currentnumber;
            
        }
    }

}
