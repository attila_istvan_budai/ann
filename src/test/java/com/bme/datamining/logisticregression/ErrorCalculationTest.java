package com.bme.datamining.logisticregression;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.bme.datamining.ann.ErrorCalculation;

public class ErrorCalculationTest {

	@Test
	public void testCalculateRMSE() {
		float[] error = { 1f, 1f, 1f, 1f };
		double rmse = ErrorCalculation.calculateRMSE(error);
		Assert.assertEquals(1.0, rmse, 0.001);
	}

	@Test
	public void testCalculateAUC() {
		float[] prediction = { 0.1f, 0.2f, 0.3f, 0.4f, 0.5f, 0.6f, 0.7f, 0.8f, 0.9f };
		float[] label = { 0f, 0f, 0f, 0f, 0f, 1f, 1f, 1f, 1f };
		float auc = ErrorCalculation.calculateAUC(prediction, label);
		Assert.assertEquals(1f, auc, 0.001);
	}

	@Test
	public void testCalculateAUCReversed() {
		float[] prediction = { 0.1f, 0.2f, 0.3f, 0.4f, 0.5f, 0.6f, 0.7f, 0.8f, 0.9f };
		float[] label = { 1f, 1f, 1f, 1f, 1f, 0f, 0f, 0f, 0f };
		float auc = ErrorCalculation.calculateAUC(prediction, label);
		Assert.assertEquals(0f, auc, 0.001);
	}

	@Test
	public void testCalculateAUCtoHalf() {
		float[] prediction = { 0.1f, 0.2f, 0.3f, 0.4f, 0.5f, 0.6f, 0.7f, 0.8f, 0.9f };
		float[] label = { 0f, 1f, 0f, 1f, 0f, 1f, 0f, 1f, 0f };
		float auc = ErrorCalculation.calculateAUC(prediction, label);
		Assert.assertEquals(0.5f, auc, 0.001);
	}

	@Test
	public void testCalculateAUCtoHalfRev() {
		float[] prediction = { 0.1f, 0.2f, 0.3f, 0.4f, 0.5f, 0.6f, 0.7f, 0.8f, 0.9f };
		float[] label = { 1f, 0f, 1f, 0f, 1f, 0f, 1f, 0f, 1f };
		float auc = ErrorCalculation.calculateAUC(prediction, label);
		Assert.assertEquals(0.5f, auc, 0.001);
	}

	@Test
	public void testCalculateAUCWithPositiveSameLabels() {
		float[] prediction = { 0.1f, 0.2f, 0.3f, 0.4f, 0.5f, 0.6f, 0.7f, 0.8f, 0.9f };
		float[] label = { 1f, 1f, 1f, 1f, 1f, 1f, 1f, 1f, 1f };
		float auc = ErrorCalculation.calculateAUC(prediction, label);
		Assert.assertEquals(1f, auc, 0.001);
	}

	@Test
	public void testCalculateAUCWithNegativeSameLabels() {
		float[] prediction = { 0.1f, 0.2f, 0.3f, 0.4f, 0.5f, 0.6f, 0.7f, 0.8f, 0.9f };
		float[] label = { 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f };
		float auc = ErrorCalculation.calculateAUC(prediction, label);
		Assert.assertEquals(1f, auc, 0.001);
	}

	@Test
	public void testAccuracyWhenItIsZero() {
		float[] error = { 0.9f, 0.7f, 0.7f, 0.5f, 0.6f, 0.6f, 0.7f, 0.8f, 0.9f };
		float accuracy = ErrorCalculation.calculateAccuracy(error);
		Assert.assertEquals(0f, accuracy, 0.001);
	}

	@Test
	public void testAccuracyWhenItIsHalf() {
		float[] error = { 0.01f, 0.05f, 0.10f, 0.15f, 0.19f, 0.6f, 0.6f, 0.7f, 0.8f, 0.9f };
		float accuracy = ErrorCalculation.calculateAccuracy(error);
		Assert.assertEquals(0.5f, accuracy, 0.001);
	}
}
