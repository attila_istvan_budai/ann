package com.bme.datamining.filereader;

import junit.framework.Assert;

import org.testng.annotations.Test;

public class FloatListTest {

	@Test
	public void testIsEmpty() {
		FloatList list = new FloatList();
		boolean isEmpty = list.isEmpty();
		Assert.assertTrue(isEmpty);
	}

	@Test
	public void testSize() {
		FloatList list = new FloatList(5);
		list.add(2f);
		Assert.assertEquals(list.size(), 1);
		boolean isEmpty = list.isEmpty();
		Assert.assertFalse(isEmpty);
	}

	@Test
	public void testClear() {
		FloatList list = new FloatList(5);
		list.add(2f);
		int listSize = list.size();
		Assert.assertEquals(list.size(), 1);
		list.clear();
		listSize = list.size();
		Assert.assertEquals(listSize, 0);
	}
}
