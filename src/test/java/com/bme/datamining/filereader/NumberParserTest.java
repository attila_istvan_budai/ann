package com.bme.datamining.filereader;


import org.testng.Assert;
import org.testng.annotations.Test;

import com.bme.datamining.ann.ErrorCalculation;

public class NumberParserTest {
	
	@Test
	public void testParseFloat() {
		float result = NumberParser.parseFloat("1.01");
		Assert.assertEquals(1.01f, result, 0.01);
	}
	
	@Test
	public void testParseFloatWithNegative() {
		float result = NumberParser.parseFloat("-1.01");
		Assert.assertEquals(-1.01f, result, 0.01);
	}
	
	@Test
	public void testParseFloatWithMultipleDigits() {
		float result = NumberParser.parseFloat("10.01");
		Assert.assertEquals(10.01f, result, 0.01);
	}
	
	@Test
	public void testParseFloatWithDotCharacterFirst() {
		float result = NumberParser.parseFloat(".01");
		Assert.assertEquals(0.01f, result, 0.01);
	}
	
	@Test
	public void testParseFloatWithLetters() {
		float result = NumberParser.parseFloat("1.01abced");
		Assert.assertEquals(1.01f, result, 0.01);
	}
	
	@Test
	public void testParseFloatScientific() {
		float result = NumberParser.parseFloat("1.103e+02");
		Assert.assertEquals(110.3f, result, 0.01);
	}
	
	@Test
	public void testParseFloatScientificnegativeNumber() {
		float result = NumberParser.parseFloat("1.103e-02");
		Assert.assertEquals(0.01103f, result, 0.01);
	}
	
	@Test(expectedExceptions = NumberFormatException.class)
	public void testParseFloatException() {
		float result = NumberParser.parseFloat("abced");
	}
	
	@Test
	public void testParseInt() {
		int result = NumberParser.parseInt("1");
		Assert.assertEquals(1, result);
	}
	
	@Test
	public void testParseIntWithNegativeNumber() {
		int result = NumberParser.parseInt("-1");
		Assert.assertEquals(-1, result);
	}
	
	@Test
	public void testParseIntWithMultipleDigits() {
		int result = NumberParser.parseInt("100");
		Assert.assertEquals(100, result);
	}
}
