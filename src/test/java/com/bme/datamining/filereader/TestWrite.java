package com.bme.datamining.filereader;

import java.io.BufferedOutputStream;
import java.io.DataOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.Random;

public class TestWrite {
	interface IntWriter {
		void write(float[] floats);
	}

	public static final int NUM_FLOATS = 1000000;
	public static final int NUM_TESTS = 3;
	public static final String[] TESTS = { "Buffered DataOutputStream", "DataOutputStream", "ObjectOutputStream", "FileChannel alt" };

	public static void main(String[] args) {
		// create buffer
		float[] floats = new float[NUM_FLOATS];
		Random r = new Random();
		for (int i = 0; i < NUM_FLOATS; i++)
			floats[i] = r.nextFloat();

		// run tests
		double[][] results = new double[TESTS.length][NUM_TESTS];

		System.out.print("Running tests... ");
		for (int i = 0; i < NUM_TESTS; i++) {
			System.out.print(i + " ");

			results[0][i] = time("Buffered DataOutputStream", new IntWriter() {
				@Override
				public void write(float[] floats) {
					storeBDO(floats);

				}
			}, floats);

			results[1][i] = time("DataOutputStream", new IntWriter() {
				public void write(float[] ints) {
					storeDO(ints);
				}
			}, floats);

			results[2][i] = time("ObjectOutputStream", new IntWriter() {
				public void write(float[] ints) {
					storeOO(ints);
				}
			}, floats);

			results[3][i] = time("FileChannel alt", new IntWriter() {
				public void write(float[] ints) {
					storeFCAlt(ints);
				}
			}, floats);
		}
		System.out.println();

		// print results
		for (int i = 0; i < TESTS.length; i++) {
			System.out.print(TESTS[i] + "\t");
			for (int j = 0; j < NUM_TESTS; j++) {
				System.out.print(results[i][j] + "\t");
			}
			System.out.println();
		}

		// time("FileChannel", new IntWriter() {
		// public void write(int[] ints) {
		// storeFC(ints);
		// }
		// }, ints);
	}

	private static double time(String name, IntWriter writer, float[] floats) {
		long start = System.nanoTime();
		writer.write(floats);
		long end = System.nanoTime();
		double ms = (end - start) / 1000000d;
		return ms;
		// System.out.printf("%s wrote %,d ints in %,.3f ms%n", name,
		// ints.length,
		// ms);
	}

	// ========================================================================
	// tests
	// ========================================================================

	private static void storeOO(float[] ints) {
		ObjectOutputStream out = null;
		try {
			out = new ObjectOutputStream(new FileOutputStream("object.out"));
			out.writeObject(ints);
		} catch (IOException e) {
			throw new RuntimeException(e);
		} finally {
			safeClose(out);
		}
	}

	private static void storeBDO(float[] ints) {
		DataOutputStream out = null;
		try {
			out = new DataOutputStream(new BufferedOutputStream(new FileOutputStream("data.out")));
			for (float anInt : ints) {
				out.writeFloat(anInt);
			}
		} catch (IOException e) {
			throw new RuntimeException(e);
		} finally {
			safeClose(out);
		}
	}

	private static void storeDO(float[] ints) {
		DataOutputStream out = null;
		try {
			out = new DataOutputStream(new FileOutputStream("data.out"));
			for (float anInt : ints) {
				out.writeFloat(anInt);
			}
		} catch (IOException e) {
			throw new RuntimeException(e);
		} finally {
			safeClose(out);
		}
	}

	private static void storeFC(float[] ints) {
		FileOutputStream out = null;
		try {
			out = new FileOutputStream("fc.out");
			FileChannel file = out.getChannel();
			ByteBuffer buf = file.map(FileChannel.MapMode.READ_WRITE, 0, 4 * ints.length);
			for (float i : ints) {
				buf.putFloat(i);
			}
			file.close();
		} catch (IOException e) {
			throw new RuntimeException(e);
		} finally {
			safeClose(out);
		}
	}

	private static void storeFCAlt(float[] ints) {
		ByteBuffer buffer = ByteBuffer.allocate(4 * ints.length);
		for (float i : ints)
			buffer.putFloat(i);

		FileChannel fc = null;
		try {
			fc = new FileOutputStream("fcalt.out").getChannel();
			fc.write(buffer);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			safeClose(fc);
		}
	}

	// safe close helpers

	private static void safeClose(OutputStream out) {
		try {
			if (out != null) {
				out.close();
			}
		} catch (IOException e) {
			// do nothing
		}
	}

	private static void safeClose(FileChannel out) {
		try {
			if (out != null) {
				out.close();
			}
		} catch (IOException e) {
			// do nothing
		}
	}

}