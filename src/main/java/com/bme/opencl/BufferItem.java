package com.bme.opencl;

import com.nativelibs4java.opencl.CLBuffer;

public class BufferItem {
	private CLBuffer buffer;
	private boolean isUsed;
	private int size;

	public BufferItem(CLBuffer buffer, boolean isUsed) {
		super();
		this.buffer = buffer;
		this.isUsed = isUsed;
		this.size = (int) buffer.getElementCount();
	}

	public CLBuffer getBuffer() {
		return buffer;
	}

	public void setBuffer(CLBuffer buffer) {
		this.buffer = buffer;
	}

	public boolean isUsed() {
		return isUsed;
	}

	public void setUsed(boolean isUsed) {
		this.isUsed = isUsed;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

}
