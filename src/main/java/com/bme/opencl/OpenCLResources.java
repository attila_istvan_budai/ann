package com.bme.opencl;

import java.nio.ByteOrder;
import java.util.HashMap;

import com.nativelibs4java.opencl.CLContext;
import com.nativelibs4java.opencl.CLKernel;
import com.nativelibs4java.opencl.CLProgram;
import com.nativelibs4java.opencl.CLQueue;

public class OpenCLResources {

	CLContext context;
	CLQueue queue;
	ByteOrder byteOrder;
	CLProgram program;
	HashMap<String, CLKernel> kernelList;
	
	public OpenCLResources(CLContext context, CLQueue queue, ByteOrder byteOrder, CLProgram program) {
		this.context = context;
		this.queue = queue;
		this.byteOrder = byteOrder;
		this.program = program;
		kernelList = new HashMap<String, CLKernel>();
	}
	
	public CLContext getContext() {
		return context;
	}
	public CLQueue getQueue() {
		return queue;
	}
	public ByteOrder getByteOrder() {
		return byteOrder;
	}
	public CLProgram getProgram() {
		return program;
	}
	
	public CLKernel createKernel(String kernelName){
		CLKernel kernel = kernelList.get(kernelName);
		if (kernel != null) {
			return kernel;
		} else {		
			kernel = program.createKernel(kernelName);
			kernelList.put(kernelName, kernel);
			return kernel;
		}
	}

}
