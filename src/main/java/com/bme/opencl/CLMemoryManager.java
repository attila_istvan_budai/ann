package com.bme.opencl;

import static org.bridj.Pointer.allocateFloats;

import java.util.LinkedList;

import org.bridj.Pointer;

import com.nativelibs4java.opencl.CLBuffer;
import com.nativelibs4java.opencl.CLMem.Usage;

public class CLMemoryManager {
	private static OpenCLResources res;
	private static LinkedList<BufferItem> buffers;

	public CLMemoryManager(OpenCLResources r) {
		res = r;
		buffers = new LinkedList<BufferItem>();
	}

	public static CLBuffer<Float> searchForBufferBySize(int length) {
		for (BufferItem buffer : buffers) {
			if (buffer.getSize() == length || buffer.isUsed() == false) {
				System.out.println("buffer has been found by size");
				return buffer.getBuffer();
			}
		}
		return null;
	}

	public static CLBuffer<Float> searchForBuffer(CLBuffer<Float> buff) {
		BufferItem bufferItem = searchForBufferItem(buff);
		if (bufferItem != null)
			return searchForBufferItem(buff).getBuffer();
		else
			return null;
	}

	public static BufferItem searchForBufferItem(CLBuffer<Float> buff) {
		for (BufferItem buffer : buffers) {
			if (buffer.getBuffer() == buff) {
				return buffer;
			}
		}
		return null;
	}

	public static void addBuffer(CLBuffer<Float> buff) {
		buffers.add(new BufferItem(buff, true));
	}

	public static void inactivateBuffer(CLBuffer<Float> buff) {
		searchForBufferItem(buff).setUsed(false);
	}

	public static CLBuffer<Float> createEmptyCLBuffer(long length) {
		CLBuffer<Float> buffer = searchForBufferBySize((int) length);
		if (buffer == null) {
			System.out.println("New buffer has been created");
			buffer = res.getContext().createFloatBuffer(Usage.InputOutput, length);
			addBuffer(buffer);
		}
		return buffer;

	}

	public static CLBuffer<Float> createCLBuffer(float[] data) {
		CLBuffer<Float> buffer = searchForBufferBySize(data.length);
		if (buffer == null) {
			Pointer<Float> oPointer = allocateFloats(data.length).order(res.getByteOrder());
			oPointer.setFloats(data);
			buffer = res.getContext().createFloatBuffer(Usage.InputOutput, oPointer);
			addBuffer(buffer);
		}
		return buffer;

	}

	public static CLBuffer<Float> createCLBuffer(Pointer<Float> pointer) {
		CLBuffer<Float> buffer = searchForBufferBySize((int) pointer.getSizeT());
		if (buffer == null) {
			buffer = res.getContext().createFloatBuffer(Usage.InputOutput, pointer);
			addBuffer(buffer);
		}
		return buffer;

	}

	public static void freeBuffer(CLBuffer<Float> buffer) {
		System.out.println("buffer has been inactivated");
		inactivateBuffer(buffer);
		// buffer.release();
	}
}
