package com.bme.datamining.filereader;

import org.apache.log4j.Logger;

/**
 * The class NumberParse parses numbers from string objects
 */
public class NumberParser {

	/** The Constant log. */
	static final Logger LOGGER = Logger.getLogger(NumberParser.class);

	/**
	 * Parses Integer
	 *
	 * @param integerString
	 *            input String
	 * @return input as an integer
	 */
	public static int parseInt(final String integerString) {
		// Check for a sign.
		int num = 0;
		int sign = -1;
		final int len = integerString.length();
		final char ch = integerString.charAt(0);
		if (ch == '-') {
			sign = 1;
		} else {
			num = '0' - ch;
		}
		// Build the number.
		int i = 1;
		while (i < len) {
			num = num * 10 + '0' - integerString.charAt(i++);
		}

		return sign * num;
	}

	/**
	 * Parses the float.
	 *
	 * @param floatString
	 *            the input string
	 * @return the string as a float primitive
	 * @throws NumberFormatException
	 *             the number format exception
	 */
	public static float parseFloat(String floatString) throws NumberFormatException {
		final int len = floatString.length();
		float ret = 0f; // return value
		int pos = 0; // read pointer position
		int part = 0; // the current part (int, float and sci parts of the
						// number)
		boolean neg = false; // true if part is a negative number

		// find start
		while (pos < len && (floatString.charAt(pos) < '0' || floatString.charAt(pos) > '9') && floatString.charAt(pos) != '-' && floatString.charAt(pos) != '.') {
			pos++;
		}
		if (pos == len) {
			LOGGER.debug("Cannot convert to float: " + floatString);
			throw new NumberFormatException();
		}
		// sign
		if (floatString.charAt(pos) == '-') {
			neg = true;
			pos++;
		}

		// integer part
		while (pos < len && !(floatString.charAt(pos) > '9' || floatString.charAt(pos) < '0'))
			part = part * 10 + (floatString.charAt(pos++) - '0');
		ret = neg ? (float) (part * -1) : (float) part;

		// float part
		if (pos < len && floatString.charAt(pos) == '.') {
			pos++;
			int mul = 1;
			part = 0;
			while (pos < len && !(floatString.charAt(pos) > '9' || floatString.charAt(pos) < '0')) {
				part = part * 10 + (floatString.charAt(pos) - '0');
				mul *= 10;
				pos++;
			}
			ret = neg ? ret - (float) part / (float) mul : ret + (float) part / (float) mul;
		}

		// scientific part
		if (pos < len && (floatString.charAt(pos) == 'e' || floatString.charAt(pos) == 'E')) {
			pos++;
			neg = (floatString.charAt(pos) == '-');
			pos++;
			part = 0;
			while (pos < len && !(floatString.charAt(pos) > '9' || floatString.charAt(pos) < '0')) {
				part = part * 10 + (floatString.charAt(pos++) - '0');
			}
			if (neg) {
				ret = ret / (float) Math.pow(10, part);
			} else {
				ret = ret * (float) Math.pow(10, part);
			}
		}
		return ret;
	}

}
