package com.bme.datamining.filereader;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import com.bme.data.matrix.MatrixElement;

/**
 * The ReadFiles class handle the file reading operations
 */
public class ReadFiles {

	/** The label list. */
	public static ArrayList<String> labelList = new ArrayList<String>();

	/**
	 * Read file as array.
	 *
	 * @param filename
	 *            the filename as full path
	 * @param delimiter
	 *            the delimiter
	 * @return the float[]
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public static float[] readFileAsArray(String filename, String delimiter) throws IOException {
		FloatList input = new FloatList();
		BufferedReader br = new BufferedReader(new java.io.FileReader(filename));
		String line;
		List<String> words = new ArrayList<String>();
		int delimsize = delimiter.length();
		while ((line = br.readLine()) != null) {
			words.clear();
			int pos = 0, end;
			while ((end = line.indexOf(delimiter, pos)) >= 0) {
				words.add(line.substring(pos, end));
				pos = end + delimsize;
			}
			// words.
			// System.out.println(words);
			labelList.add(words.get(0));
			for (int i = 1; i < words.size(); i++) {
				input.add(NumberParser.parseFloat(words.get(i)));
			}

		}
		br.close();
		return input.toArray();
	}

	/**
	 * Read annotation. The function is used to read annotations from file
	 *
	 * @param filename
	 *            the filename
	 * @param delimiter
	 *            the delimiter
	 * @return the float[]
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public static float[] readAnnotation(String filename, String delimiter) throws IOException {
		FloatList input = new FloatList();
		BufferedReader br = new BufferedReader(new java.io.FileReader(filename));
		String line;
		int index = 0;
		while ((line = br.readLine()) != null && index < labelList.size()) {
			String[] words = line.split(" +");
			if (labelList.get(index).startsWith(words[0])) {
				index++;
				try {
					if (NumberParser.parseInt(words[1]) == 1)
						input.add(1);
					else
						input.add(0);
				} catch (NumberFormatException e) {

				}
			}

		}
		br.close();
		return input.toArray();

	}

	/**
	 * Reads movielens data
	 *
	 * @param filename
	 *            the filename
	 * @param delimiter
	 *            the delimiter
	 * @return the list
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public static List<MatrixElement> load(String filename, String delimiter) throws IOException {
		FileInputStream fis = new FileInputStream(filename);
		FileChannel fc = fis.getChannel();

		MappedByteBuffer mmb = fc.map(FileChannel.MapMode.READ_ONLY, 0, fc.size());

		byte[] buffer = new byte[(int) fc.size()];
		mmb.get(buffer);

		fis.close();

		BufferedReader in = new BufferedReader(new InputStreamReader(new ByteArrayInputStream(buffer)));

		List<MatrixElement> input = new ArrayList<MatrixElement>();
		List<String> words = new ArrayList<String>();
		int delimsize = delimiter.length();

		for (String line = in.readLine(); line != null; line = in.readLine()) {
			words.clear();
			int pos = 0, end;
			while ((end = line.indexOf(delimiter, pos)) >= 0) {
				words.add(line.substring(pos, end));
				pos = end + delimsize;
			}
			int user = NumberParser.parseInt(words.get(0)) - 1;
			int movie = NumberParser.parseInt(words.get(1));
			int rating = NumberParser.parseInt(words.get(2));
			input.add(new MatrixElement(rating, movie, user));
		}

		in.close();
		return input;
	}

	/**
	 * Read file to string.
	 *
	 * @param path
	 *            the path
	 * @param encoding
	 *            the encoding
	 * @return the string
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public static String readFileToString(String path, Charset encoding) throws IOException {
		byte[] encoded = Files.readAllBytes(Paths.get(path));
		return new String(encoded, encoding);
	}

}
