package com.bme.datamining.filereader;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import com.bme.datamining.ann.activatefunctions.SigmoidFunction;
import com.bme.datamining.ann.dnn.DNNOpenCL;
import com.bme.datamining.ann.dnn.DNNOpenCL.DNNBuilder;
import com.bme.datamining.ann.logisticregression.LogisticRegression;
import com.bme.datamining.ann.neurons.InnerNeuron;
import com.bme.datamining.ann.rbm.RBMLayer;
import com.bme.datamining.filewriter.SaveFloatArray;
import com.bme.datamining.filewriter.SaveModel;
import com.bme.datamining.svm.SVM;

public class LoadModel {

	public final static String rootName = SaveModel.rootName;

	public static DNNOpenCL loadModel(File directory) {
		File[] fList = directory.listFiles();

		DNNBuilder dnnBuilder = new DNNBuilder();
		int rbmLayerNumber = 0;
		List<float[]> softMaxWeights = new ArrayList<>();
		int lastHiddenNumber = 0;

		boolean isFirst = true;
		for (File f : fList) {
			System.out.println(f.getName());
			if (f.getName().matches("-?\\d+(\\.\\d+)?")) {
				lastHiddenNumber = createLayer(f.getPath(), dnnBuilder, isFirst);
				isFirst = false;
				rbmLayerNumber++;
			} else if (f.getName().equals("SVM")) {
				dnnBuilder.addSVMToTopLayer();
				SVM svm = dnnBuilder.getSvm();
				svm.loadModel(new File(f, "svm.dat"));
			}

		}

		DNNOpenCL dnn = dnnBuilder.create();

		LinkedList<RBMLayer> layers = new LinkedList<RBMLayer>();
		List<float[]> weights = new ArrayList<float[]>();
		layers = dnn.getLayers();
		for (int i = 0; i < rbmLayerNumber; i++) {
			File weightFile = new File(fList[i], "weight.dat");
			weights.add(SaveFloatArray.readOO(layers.get(i).visibleUnitNumber * layers.get(i).hiddenUnitNumber, weightFile.getPath()));
		}
		dnn.setWeights(weights);
		// dnn.setSoftMaxWeights(softMaxWeights);
		return dnn;

	}

	public static DNNOpenCL loadModel(String folderName) {
		File directory = new File(rootName, folderName);
		return loadModel(directory);
	}

	public static LogisticRegression loadLogRegModel(String folderName) {
		File directory = new File(rootName, folderName);
		return loadLogRegModel(directory);
	}

	public static LogisticRegression loadLogRegModel(File directory) {
		LogisticRegression logreg = new LogisticRegression(new InnerNeuron(new SigmoidFunction()));

		LogisticRegression.Result resultMeta = readMetadata(directory.getPath());
		File weightFile = new File(directory, "weight.dat");
		float[] weight = SaveFloatArray.readOO(resultMeta.visibleNumber, weightFile.getPath());
		logreg.setWeight(weight);
		return logreg;
	}

	public static LogisticRegression.Result readMetadata(String dir) {
		int visibleNumber = 0;
		int iterationNumber = 0;
		float accuracy = 0;
		double RMSE = 0;
		File meta = new File(dir, "model.meta");
		try (BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(meta.getPath()), "utf-8"))) {
			String line = reader.readLine();
			String num = line.substring(SaveModel.strLogregVisibleNumber.length(), line.length());
			visibleNumber = Integer.parseInt(num);
			line = reader.readLine();
			num = line.substring(SaveModel.strLogregIterations.length(), line.length());
			iterationNumber = Integer.parseInt(num);
			num = line.substring(SaveModel.strLogregAccuracy.length(), line.length());
			accuracy = Float.parseFloat(num);
			num = line.substring(SaveModel.strLogregRMSE.length(), line.length());
			RMSE = Double.parseDouble(num);
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return new LogisticRegression.Result(iterationNumber, visibleNumber, RMSE, accuracy);

	}

	public static int createLayer(String dir, DNNBuilder builder, boolean isFirst) {
		int visible = 0;
		int hidden = 0;
		File meta = new File(dir, "model.meta");
		try (BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(meta.getPath()), "utf-8"))) {
			String line = reader.readLine();
			String num = line.substring(SaveModel.strVisibleNumber.length(), line.length());
			visible = Integer.parseInt(num);
			line = reader.readLine();
			num = line.substring(SaveModel.strHiddenNumber.length(), line.length());
			hidden = Integer.parseInt(num);
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (isFirst) {
			builder.addFirstLayer(hidden, visible);
		} else {
			builder.addLayer(hidden);
		}
		return hidden;

	}
}
