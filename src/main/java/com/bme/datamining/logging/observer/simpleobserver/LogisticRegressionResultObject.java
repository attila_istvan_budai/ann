package com.bme.datamining.logging.observer.simpleobserver;

public class LogisticRegressionResultObject {
	private int iterationNumber;
	private long exectuitonTime;
	private double error;
	private float[] model;

	public LogisticRegressionResultObject(int iterationNumber, long exectuitonTime, double error, float[] model) {
		super();
		this.iterationNumber = iterationNumber;
		this.exectuitonTime = exectuitonTime;
		this.error = error;
		this.model = model;
	}

	public int getIterationNumber() {
		return iterationNumber;
	}

	public void setIterationNumber(int iterationNumber) {
		this.iterationNumber = iterationNumber;
	}

	public long getExectuitonTime() {
		return exectuitonTime;
	}

	public void setExectuitonTime(long exectuitonTime) {
		this.exectuitonTime = exectuitonTime;
	}

	public double getError() {
		return error;
	}

	public void setError(double error) {
		this.error = error;
	}

	public float[] getModel() {
		return model;
	}

	public void setModel(float[] model) {
		this.model = model;
	}

}
