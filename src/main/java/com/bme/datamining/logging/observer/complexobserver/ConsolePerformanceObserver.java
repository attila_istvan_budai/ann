package com.bme.datamining.logging.observer.complexobserver;

import com.bme.datamining.logging.observer.Observer;
import com.bme.datamining.logging.observer.Subject;

public class ConsolePerformanceObserver implements Observer {
	private Subject topic;
	private final String[] timeStrings = new String[] { "positiveHiddenProbabilities", "datasetTransposed", "positiveGradient", "transposedWeight", "negativeVisibleProbabilities",
			"negativeHiddenProbabilities", "transposedNegativeVisibleProbabilities", "negativeGradient", "grad", "WeightUpdate" };

	public ConsolePerformanceObserver() {
	}

	@Override
	public void update() {
		RBMScalePerformanceObject msg = (RBMScalePerformanceObject) topic.getUpdate(this);
		if (msg == null) {
			System.out.println(" No new message");
		} else
			printResult(msg);
	}

	public void printResult(RBMScalePerformanceObject msg) {

		long[] executionTimes = msg.getExectuitonTime();
		for (int i = 0; i < 10; i++) {
			System.out.println(timeStrings[i] + " execution Time: " + executionTimes[i] + "  ");
		}
		System.out.println();
	}

	@Override
	public void setSubject(Subject sub) {
		this.topic = sub;
	}
}
