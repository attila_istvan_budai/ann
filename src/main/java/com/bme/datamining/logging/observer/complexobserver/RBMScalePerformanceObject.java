package com.bme.datamining.logging.observer.complexobserver;

public class RBMScalePerformanceObject {
	private long[] executionTimes;

	public RBMScalePerformanceObject(long[] executionTimes) {
		super();
		this.executionTimes = executionTimes;
	}

	public long[] getExectuitonTime() {
		return executionTimes;
	}

	public void setExectuitonTime(long[] executionTimes) {
		this.executionTimes = executionTimes;
	}

}
