package com.bme.datamining.logging.observer.simpleobserver;

import com.bme.datamining.logging.observer.Observer;
import com.bme.datamining.logging.observer.Subject;

public class ConsoleObserver implements Observer {
	private Subject topic;

	public ConsoleObserver() {
	}

	@Override
	public void update() {
		RBMResultObject msg = (RBMResultObject) topic.getUpdate(this);
		if (msg == null) {
			System.out.println(" No new message");
		} else
			printResult(msg);
	}

	public void printResult(RBMResultObject msg) {
		System.out.print("Iteration Number: " + msg.getIterationNumber() + "  ");
		System.out.print("Error: " + msg.getError() + "  ");
		System.out.print("Execution Time: " + msg.getExectuitonTime() + "  ");
		System.out.println();
	}

	@Override
	public void setSubject(Subject sub) {
		this.topic = sub;
	}
}
