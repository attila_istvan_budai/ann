package com.bme.datamining.logging.observer.complexobserver;

import java.util.ArrayList;
import java.util.List;

import com.bme.datamining.logging.observer.Observer;
import com.bme.datamining.logging.observer.Subject;

public class RBMScalePerformanceSubject implements Subject {

	private List<Observer> observers;
	private String message;
	private boolean changed;
	// private final Object MUTEX = new Object();

	RBMScalePerformanceObject rbmObj;

	public RBMScalePerformanceSubject() {
		this.observers = new ArrayList<>();
	}

	public void setRBMObj(RBMScalePerformanceObject obj) {
		this.rbmObj = obj;
		this.changed = true;
		notifyObservers();
	}

	@Override
	public void register(Observer obj) {
		if (obj == null)
			throw new NullPointerException("Null Observer");
		// synchronized (MUTEX) {
		if (!observers.contains(obj)) {
			observers.add(obj);
			obj.setSubject(this);
		}
		// }

	}

	@Override
	public void unregister(Observer obj) {
		// synchronized (MUTEX) {
		observers.remove(obj);
		// }

	}

	@Override
	public void notifyObservers() {
		List<Observer> observersLocal = null;
		// synchronization is used to make sure any observer registered after
		// message is received is not notified
		// synchronized (MUTEX) {
		if (!changed)
			return;
		observersLocal = new ArrayList<>(this.observers);
		this.changed = false;
		// }
		for (Observer obj : observersLocal) {
			obj.update();
		}

	}

	@Override
	public RBMScalePerformanceObject getUpdate(Observer obj) {
		return rbmObj;
	}

}
