package com.bme.datamining.filewriter;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;

import com.bme.datamining.ann.dnn.DNNOpenCL;
import com.bme.datamining.ann.logisticregression.LogisticRegression;
import com.bme.datamining.ann.rbm.RBMLayer;
import com.bme.datamining.logging.observer.simpleobserver.RBMResultObject;
import com.bme.datamining.svm.SVM;

public class SaveModel {

	public final static String rootName = "models";
	public final static String strVisibleNumber = "visible number ";
	public final static String strHiddenNumber = "hidden number ";

	public final static String strLogregVisibleNumber = "visible item number ";
	public final static String strLogregIterations = "iteration number ";
	public final static String strLogregRMSE = "RMSE ";
	public final static String strLogregAccuracy = "Accuracy ";

	public static File createRoot() {
		File dir = new File(rootName);
		if (!dir.exists()) {
			dir.mkdir();
		}
		return dir;
	}

	public static File createFolder(String folderName) {
		File dir = new File(rootName, folderName);
		if (!dir.exists()) {
			dir.mkdir();
		}
		return dir;
	}

	static void saveModelToName(DNNOpenCL dnn, String fileName, List<List<RBMResultObject>> resultLists) {
		File dir = createFolder(fileName);
		LinkedList<RBMLayer> layers = dnn.getLayers();
		List<float[]> weights = dnn.getWeights();

		for (int i = 0; i < layers.size(); i++) {
			if (resultLists != null && resultLists.size() > 0) {
				saveLayer(dir.getPath(), i, layers.get(i), weights.get(i), resultLists.get(i));
			} else {
				saveLayer(dir.getPath(), i, layers.get(i), weights.get(i), null);
			}
		}
		if (dnn.getSvm() != null) {
			SVM svm = dnn.getSvm();
			File svmDirectory = new File(dir.getPath(), "SVM");
			svmDirectory.mkdir();
			File fileSVM = new File(svmDirectory, "svm.dat");
			svm.saveModel(fileSVM);
		}
	}

	public static String saveNewModel(DNNOpenCL dnn, List<List<RBMResultObject>> resultLists) {
		createRoot();
		String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());

		String folderName = "DNN_model-" + timeStamp;

		saveModelToName(dnn, folderName, resultLists);

		return folderName;

	}

	private static void saveSoftmaxLayer(String path, List<float[]> weight) {
		String folderName = "softmax";
		File dir = new File(path, folderName);
		dir.mkdir();

		for (int i = 0; i < 10; i++) {
			File weightFile = new File(dir, "weight " + Integer.toString(i) + ".dat");
			SaveFloatArray.storeOO(weight.get(i), weightFile.getPath());
		}
	}

	private static void saveLayer(String path, int index, RBMLayer layer, float[] weight, List<RBMResultObject> resultList) {
		String folderName = Integer.toString(index);
		File dir = new File(path, folderName);
		dir.mkdir();
		String seperator = System.getProperty("line.separator");

		saveMetaData(layer, dir, seperator);
		if (resultList != null) {
			saveResults(resultList, dir, seperator);
		}

		File weightFile = new File(dir, "weight.dat");
		SaveFloatArray.storeOO(weight, weightFile.getPath());

	}

	private static void saveResults(List<RBMResultObject> resultList, File dir, String seperator) {
		File meta = new File(dir, "result.dat");

		try (Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(meta.getPath()), "utf-8"))) {
			for (RBMResultObject result : resultList) {
				writer.write(Integer.toString(result.getIterationNumber()) + '\t');
				writer.write(Double.toString(result.getError()) + '\t');
				writer.write(seperator);
			}
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private static void saveMetaData(RBMLayer layer, File dir, String seperator) {
		File meta = new File(dir, "model.meta");
		try (Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(meta.getPath()), "utf-8"))) {
			writer.write(strVisibleNumber + layer.visibleUnitNumber + seperator);
			writer.write(strHiddenNumber + layer.hiddenUnitNumber + seperator);
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void saveModelToName(LogisticRegression logreg, String fileName) {
		File dir = createFolder(fileName);
		saveLogReg(dir.getPath(), logreg);
	}

	public static void saveNewModel(LogisticRegression logreg) {
		createRoot();
		String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());

		String folderName = "LogReg_model-" + timeStamp;

		saveModelToName(logreg, folderName);

	}

	public static void saveLogReg(String path, LogisticRegression logreg) {
		String seperator = System.getProperty("line.separator");

		File meta = new File(path, "model.meta");
		try (Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(meta.getPath()), "utf-8"))) {
			writer.write("visible item number " + logreg.getResult().visibleNumber + seperator);
			writer.write("iteration number " + logreg.getResult().iteration + seperator);
			writer.write("RMSE " + logreg.getResult().rmse + seperator);
			writer.write("Accuracy " + logreg.getResult().accuracy + seperator);
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		File weightFile = new File(path, "weight.dat");
		SaveFloatArray.storeOO(logreg.getResult().modelWeight, weightFile.getPath());

	}
}
