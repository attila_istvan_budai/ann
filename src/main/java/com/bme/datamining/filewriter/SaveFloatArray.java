package com.bme.datamining.filewriter;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.ByteBuffer;
import java.nio.FloatBuffer;

public class SaveFloatArray {

	public static void storeOO(float[] ints, String fileName) {
		ObjectOutputStream out = null;
		try {
			out = new ObjectOutputStream(new FileOutputStream(fileName));
			out.writeObject(ints);
		} catch (IOException e) {
			throw new RuntimeException(e);
		} finally {
			safeClose(out);
		}
	}

	private static void safeClose(ObjectOutputStream out) {
		try {
			if (out != null) {
				out.close();
			}
		} catch (IOException e) {
			// do nothing
		}
	}

	private static void safeClose(ObjectInputStream out) {
		try {
			if (out != null) {
				out.close();
			}
		} catch (IOException e) {
			// do nothing
		}
	}

	public static float[] readOO(int length, String fileName) {
		float[] res = new float[length];
		ObjectInputStream objIn = null;
		try {
			objIn = new ObjectInputStream(new FileInputStream(fileName));
			res = (float[]) objIn.readObject();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			safeClose(objIn);
		}
		return res;
	}

	private static float[] toFloatArray(ByteBuffer buffer) {
		FloatBuffer fb = buffer.asFloatBuffer();

		float[] floatArray = new float[fb.limit()];
		fb.get(floatArray);

		return floatArray;
	}
}
