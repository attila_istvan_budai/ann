package com.bme.datamining.ann.weights;

import java.util.Arrays;
import java.util.Random;

/**
 * The class Weight stores and handles the ANN model
 */
public class Weight {

	private float[] weight;

	/**
	 * Instantiates a new weight.
	 *
	 * @param size
	 *            the size
	 */
	public Weight(int size) {
		weight = new float[size];
	}

	/**
	 * Instantiates a new weight.
	 *
	 * @param w
	 *            the w
	 */
	public Weight(float[] w) {
		this.weight = w;
	}

	/**
	 * Gets the.
	 *
	 * @param index
	 *            the index
	 * @return the float
	 */
	public float get(int index) {
		return weight[index];
	}

	/**
	 * Sets the.
	 *
	 * @param index
	 *            the index
	 * @param value
	 *            the value
	 */
	public void set(int index, float value) {
		weight[index] = value;
	}

	/**
	 * Gets the weight.
	 *
	 * @return the weight
	 */
	public float[] getWeight() {
		return weight;
	}

	/**
	 * Sets the weight.
	 *
	 * @param w
	 *            the new weight
	 */
	public void setWeight(float[] w) {
		System.out.println(Arrays.asList(w));
		this.weight = w;
	}

	/**
	 * Initialize normal distributed weight.
	 */
	public void initializeNormalDistributedWeight() {
		Random r = new Random();
		for (int i = 0; i < weight.length; i++) {
			float random = (float) (r.nextGaussian() * 0.5 + 0.5);
			weight[i] = random;
		}
	}

}
