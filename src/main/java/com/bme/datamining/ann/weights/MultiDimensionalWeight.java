package com.bme.datamining.ann.weights;

import com.bme.data.densematrix.DenseMatrixOperations;

public class MultiDimensionalWeight extends Weight {
	private int columnNumber;
	private int rowNumber;

	public MultiDimensionalWeight(int size) {
		super(size);
		columnNumber = size;
		rowNumber = 0;
	}

	public MultiDimensionalWeight(int rowNumber, int colNumber) {
		super(rowNumber * colNumber);
		this.columnNumber = colNumber;
		this.rowNumber = rowNumber;

	}

	public Weight getColumn(int colNumber) {
		float[] w = DenseMatrixOperations.getSelectedColumn(super.getWeight(), colNumber, columnNumber, rowNumber);
		return new Weight(w);
	}

}
