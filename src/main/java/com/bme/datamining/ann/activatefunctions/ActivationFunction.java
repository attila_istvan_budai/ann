package com.bme.datamining.ann.activatefunctions;

/**
 * The Interface ActivationFunction.
 */
public interface ActivationFunction {

	/**
	 * Calculate output.
	 *
	 * @param input
	 *            the input number
	 * @return the result in float
	 */
	float calculateOutput(float input);

	/**
	 * Calculate output.
	 *
	 * @param input
	 *            the input number
	 * @return the result in double
	 */
	double calculateOutput(double input);

}
