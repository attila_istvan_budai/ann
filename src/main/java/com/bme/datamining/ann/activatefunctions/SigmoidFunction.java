package com.bme.datamining.ann.activatefunctions;

/**
 * Handles sigmoid functions
 */
public class SigmoidFunction implements ActivationFunction {

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.bme.datamining.ann.ActivationFunction#calculateOutput(float)
	 */
	public float calculateOutput(float x) {
		return (1 / (1 + (float) Math.exp(-1 * x)));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.bme.datamining.ann.ActivationFunction#calculateOutput(double)
	 */
	public double calculateOutput(double x) {
		return 1 / (1 + Math.exp(-1 * x));
	}

}
