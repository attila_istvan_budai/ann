package com.bme.datamining.ann;

import java.util.ArrayList;
import java.util.Collections;

/**
 * The ErrorCalculation does the calculation in different metrics
 */
public class ErrorCalculation {

	private ErrorCalculation() {

	}

	/**
	 * Calculate rmse.
	 *
	 * @param error
	 *            the error
	 * @return the double
	 */
	public static double calculateRMSE(float[] error) {
		float sum = 0;
		int size = error.length;
		for (int i = 0; i < size; i++) {
			sum += error[i] * error[i];
		}
		return (double) Math.sqrt((sum / (double) size));
	}

	/**
	 * Calculate sparse rmse.
	 *
	 * @param error
	 *            the error
	 * @return the double
	 */
	public static double calculateSparseRMSE(float[] error) {
		float sum = 0;
		int size = error.length;
		int relevantSize = 0;
		for (int i = 0; i < size; i++) {
			sum += error[i] * error[i];
			if (error[i] != 0) {
				relevantSize++;
			}
		}
		return (double) Math.sqrt(sum / (double) relevantSize);
	}

	/**
	 * Calculate accuracy.
	 *
	 * @param error
	 *            the error
	 * @return the float
	 */
	public static float calculateAccuracy(float[] error) {
		int good = 0;
		int bad = 0;
		for (int i = 0; i < error.length; i++) {
			if (error[i] < 0.2) {
				good++;
			} else {
				bad++;
			}
		}
		return (float) good / (float) (good + bad);

	}

	public static float calculateAccuracy(float[] real, float[] prediction) {
		int good = 0;
		int bad = 0;
		for (int i = 0; i < real.length; i++) {
			if (real[i] == prediction[i]) {
				good++;
			} else {
				bad++;
			}
		}
		return (float) good / (float) (good + bad);

	}

	/**
	 * The Class ROCPair.
	 */
	private static class ROCPair implements Comparable<ROCPair> {

		public float prediction;
		public float label;

		/**
		 * Instantiates a new ROC pair.
		 *
		 * @param prediction
		 *            the prediction
		 * @param label
		 *            the label
		 */
		public ROCPair(float prediction, float label) {
			this.prediction = prediction;
			this.label = label;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see java.lang.Comparable#compareTo(java.lang.Object)
		 */
		public int compareTo(ROCPair o) {
			if (this.prediction < o.prediction) {
				return 1;
			} else if (this.prediction == o.prediction) {
				return 0;
			} else {
				return -1;
			}
		}

	}

	/**
	 * Calculate auc.
	 *
	 * @param prediction
	 *            the prediction
	 * @param realclass
	 *            the realclass
	 * @return the float
	 */
	public static float calculateAUC(float[] prediction, float[] realclass) {
		ArrayList<ROCPair> pairs = new ArrayList<ROCPair>();
		int datanumber = prediction.length;
		int positive = 0, negative = 0, positivePrev = 0, negativePrev = 0, allPositive = 0, allNegative = 0;
		ROCPair pair;
		for (int k = 0; k < datanumber; k++) {
			pair = new ROCPair(prediction[k], realclass[k]);
			pairs.add(pair);
			if (realclass[k] == 1) {
				allPositive++;
			} else {
				allNegative++;
			}
		}
		if (allNegative == 0 || allPositive == 0) {
			return 1f;
		} else {
			Collections.sort(pairs);
			float x1, x2, y1, y2;
			float area = 0;
			for (int k = 0; k < datanumber; k++) {
				if (pairs.get(k).label == 1) {
					positive++;
				} else {
					negative++;
				}

				x1 = (float) negative / (float) allNegative;
				x2 = (float) negativePrev / (float) allNegative;

				y1 = (float) positive / allPositive;
				y2 = (float) positivePrev / allPositive;

				area += (x1 - x2) * (y1 + y2) / 2;
				positivePrev = positive;
				negativePrev = negative;

			}

			return area;
		}
	}
}
