package com.bme.datamining.ann.logisticregression;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.apache.log4j.Logger;

import com.bme.datamining.ann.activatefunctions.SigmoidFunction;
import com.bme.datamining.ann.neurons.InnerNeuron;
import com.bme.datamining.filereader.ReadFiles;

public class Main {

	static final Logger LOGGER = Logger.getLogger(Main.class);

	static float[] trainingSet, trainingLabels, testSet, testLabels;

	public void testLogReg() {

		LOGGER.debug("program started");

		try {
			long startTime = System.currentTimeMillis();
			float[] array = ReadFiles.readFileAsArray("C:\\logreg\\ch64.full.tr.tr", " ");
			float[] labels = ReadFiles.readAnnotation("C:\\logreg\\1-bicycle.all", " ");

			simpleSeperation(array, labels);
			seperateInputData(array, labels, 20480);

			LogisticRegression logreg = new LogisticRegression(new InnerNeuron(new SigmoidFunction()));
			// logreg.learnCL(trainingSet, trainingLabels, testSet, testLabels,
			// trainingLabels.length - 1, 20480, testLabels.length - 1, 0.1f,
			// 100000);
			long stopTime = System.currentTimeMillis();
			long elapsedTime = stopTime - startTime;
			System.out.println(elapsedTime);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static void main(String[] args) throws IOException {
		new Main().testLogReg();
	}

	public static void seperateInputData(float[] array, float[] labels, int colNumber) {
		List<Integer> positives = new ArrayList<Integer>();
		List<Integer> negatives = new ArrayList<Integer>();
		for (int i = 0; i < labels.length; i++) {
			if (labels[i] == 1f)
				positives.add(i);
			else
				negatives.add(i);
		}
		int size = array.length;
		int labelSize = labels.length;
		int trainingLabelSize = (int) (labelSize * 0.9);
		int testLabelSize = labelSize - trainingLabelSize;
		int trainingSize = trainingLabelSize * colNumber;
		int testSize = testLabelSize * colNumber;
		trainingSet = new float[trainingSize];
		trainingLabels = new float[trainingLabelSize];
		testSet = new float[testSize];
		testLabels = new float[testLabelSize];

		Collections.shuffle(positives);
		Collections.shuffle(negatives);

		int positiveSize = (int) (positives.size() * 0.9);
		int negativeSize = trainingLabelSize - positiveSize;

		List<Integer> trainingRows = positives.subList(0, positiveSize);
		trainingRows.addAll(negatives.subList(0, negativeSize));
		Collections.sort(trainingRows);

		int trainingNum = 0, testNum = 0;
		for (int i = 0; i < labels.length - 1; i++) {
			LOGGER.debug("seperate iteration" + i);
			LOGGER.debug("training row: " + trainingRows.get(0));
			if (trainingRows.get(0) == i) {
				trainingRows.remove(0);
				for (int j = 0; j < colNumber; j++) {
					int tsize = i * colNumber + j;
					trainingSet[testNum * colNumber + j] = array[tsize];
				}
				trainingLabels[trainingNum] = labels[i];
				trainingNum++;

			} else {
				for (int j = 0; j < colNumber; j++) {
					int tsize = i * colNumber + j;
					testSet[testNum * colNumber + j] = array[tsize];
				}
				testLabels[testNum] = labels[i];
				testNum++;
			}
		}

	}

	public static void simpleSeperation(float[] array, float[] labels) {
		trainingSet = Arrays.copyOfRange(array, 0, 4500 * 20480);
		trainingLabels = Arrays.copyOfRange(labels, 0, 4500);
		testSet = Arrays.copyOfRange(array, 4500 * 20480, labels.length * 20480);
		testLabels = Arrays.copyOfRange(labels, 4500, labels.length);
	}

}
