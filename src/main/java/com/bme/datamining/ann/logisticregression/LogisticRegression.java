package com.bme.datamining.ann.logisticregression;

import static org.bridj.Pointer.allocateFloats;

import java.io.IOException;
import java.nio.ByteOrder;
import java.util.Random;

import org.apache.log4j.Logger;
import org.bridj.Pointer;

import com.bme.datamining.ann.ErrorCalculation;
import com.bme.datamining.ann.neurons.InnerNeuron;
import com.bme.datamining.ann.weights.Weight;
import com.bme.datamining.logging.observer.Observer;
import com.bme.datamining.logging.observer.simpleobserver.LogisticRegressionResultObject;
import com.bme.datamining.logging.observer.simpleobserver.LogisticRegressionSubject;
import com.bme.opencl.OpenCLResources;
import com.nativelibs4java.opencl.CLBuffer;
import com.nativelibs4java.opencl.CLContext;
import com.nativelibs4java.opencl.CLEvent;
import com.nativelibs4java.opencl.CLKernel;
import com.nativelibs4java.opencl.CLMem.Usage;
import com.nativelibs4java.opencl.CLProgram;
import com.nativelibs4java.opencl.CLQueue;
import com.nativelibs4java.opencl.JavaCL;
import com.nativelibs4java.util.IOUtils;

public class LogisticRegression {

	private InnerNeuron neuron;
	static final Logger LOGGER = Logger.getLogger(LogisticRegression.class);
	private float[] modelWeight;
	private Result result;

	LogisticRegressionSubject subject;

	public LogisticRegression(InnerNeuron n) {
		neuron = n;
		subject = new LogisticRegressionSubject();

	}

	public float[] getWeight() {
		return modelWeight;
	}

	public void setWeight(float[] weight) {
		this.modelWeight = weight;
	}

	public Result getResult() {
		return result;
	}

	public void setResult(Result result) {
		this.result = result;
	}

	public void registerObserver(Observer obj) {
		subject.register(obj);
	}

	public float[] learn(float[] trainingSet, float[] trainingLabels, float[] testSet, float[] testLabels, int rowNumber, int colNumber, int testRowNumber,
			final float LEARNING_RATE, final int ITERATION, Weight weight) {
		if (weight == null) {
			weight = new Weight(colNumber + 1);
		}
		float[] error = new float[rowNumber];
		float[] actualoutput = new float[rowNumber];
		Random r = new Random();
		for (int iteration = 0; iteration < ITERATION * 5000; iteration++) {
			int row = r.nextInt(rowNumber);
			actualoutput[row] = neuron.calculateOutput(trainingSet, weight, row * colNumber, (row + 1) * colNumber);
			error[row] = trainingLabels[row] - actualoutput[row];
			updateWeight(trainingSet, weight, error, row, colNumber, LEARNING_RATE);
			if (iteration % 5000000 == 0) {
				double rmse = ErrorCalculation.calculateRMSE(error);
				System.out.println("RMSE is " + rmse);
				System.out.println("Accuracy is " + ErrorCalculation.calculateAccuracy(error));
				System.out.println("AUC is " + ErrorCalculation.calculateAUC(actualoutput, trainingLabels));

				// subject.setLogRegObj(new
				// LogisticRegressionResultObject(iteration, 0, rmse,
				// weight.getWeight()));
				calculateTestAUC(testSet, testLabels, testRowNumber, colNumber, weight);
			}

		}

		return weight.getWeight();
	}

	public float calculateOutput(float[] trainingSet, float[] weight) {
		Weight w = new Weight(weight);

		float sum = 0;
		for (int i = 0; i < weight.length - 1; i++) {
			sum += trainingSet[i] * w.get(i);
		}
		sum += w.get(weight.length - 1);
		return sum;

		// return neuron.calculateOutput(trainingSet, w, 0, weight.length - 1);
	}

	public float[] learnCL(float[] trainingSet, float[] trainingLabels, int rowNumber, int colNumber, float LEARNING_RATE, final int ITERATION) throws IOException {
		CLContext context = JavaCL.createBestContext();
		CLQueue queue = context.createDefaultQueue();
		ByteOrder byteOrder = context.getByteOrder();
		String src = IOUtils.readText(LogisticRegression.class.getResource("logreg.cl"));
		CLProgram program = context.createProgram(src);
		OpenCLResources resources = new OpenCLResources(context, queue, byteOrder, program);

		Weight weight = new Weight(colNumber + 1);
		// weight.initializeNormalDistributedWeight();

		CLBuffer<Float> weightBuffer = convertArrayIntoCLBuffer(weight.getWeight(), resources);
		CLBuffer<Float> trainingSetBuffer = convertArrayIntoCLBuffer(trainingSet, resources);
		CLBuffer<Float> trainingLabelBuffer = convertArrayIntoCLBuffer(trainingLabels, resources);
		CLBuffer<Float> errorBuffer = convertArrayIntoCLBuffer(new float[rowNumber], resources);

		double rmse = 99999;
		float accuracy = 0;

		for (int iteration = 0; iteration <= ITERATION; iteration++) {
			errorBuffer = calculateErrorCL(trainingSetBuffer, trainingLabelBuffer, weightBuffer, errorBuffer, colNumber, rowNumber, resources);
			weightBuffer = updateWeightCL(trainingSetBuffer, errorBuffer, weightBuffer, colNumber, rowNumber, LEARNING_RATE, resources);

			if (iteration % 1000 == 0) {
				float[] error = getData(errorBuffer, resources);

				weight.setWeight(getData(weightBuffer, resources));
				rmse = ErrorCalculation.calculateRMSE(error);
				accuracy = ErrorCalculation.calculateAccuracy(error);
				System.out.println("Iteration " + iteration);
				System.out.println("RMSE is " + rmse);
				System.out.println("Accuracy is " + accuracy);

				subject.setLogRegObj(new LogisticRegressionResultObject(iteration, 0, rmse, weight.getWeight()));
			}
		}
		modelWeight = getData(weightBuffer, resources);
		this.result = new Result(modelWeight, ITERATION, colNumber, rmse, accuracy);
		return modelWeight;
	}

	public void updateWeight(float[] exampleset, Weight weight, float[] error, int currentRow, int colNumber, final float LEARNING_RATE) {
		for (int col = 0; col < colNumber; col++) {
			float delta = LEARNING_RATE * (error[currentRow] * exampleset[currentRow * colNumber + col]);
			float w = weight.get(col);
			weight.set(col, w + delta);

		}
		float delta = LEARNING_RATE * (error[currentRow]);
		float w = weight.get(colNumber);
		weight.set(colNumber, w + delta);
	}

	public CLBuffer<Float> calculateErrorCL(CLBuffer<Float> X, CLBuffer<Float> Y, CLBuffer<Float> weight, CLBuffer<Float> out, int dimension, int datanumber, OpenCLResources res) {
		CLQueue queue = res.getQueue();
		queue.finish();

		int[] globalSize = new int[1];
		globalSize[0] = datanumber;

		CLKernel calculateErrorKernel = res.createKernel("calculateError");
		calculateErrorKernel.setArgs(X, Y, weight, out, dimension);
		CLEvent addEvt = calculateErrorKernel.enqueueNDRange(queue, globalSize);

		return out;
	}

	public CLBuffer<Float> updateWeightCL(CLBuffer<Float> X, CLBuffer<Float> error, CLBuffer<Float> weight, int dimension, int datanumber, float alpha, OpenCLResources res) {
		CLQueue queue = res.getQueue();
		queue.finish();

		int[] globalSize = new int[1];
		globalSize[0] = dimension;

		CLKernel updateWeightKernel = res.createKernel("updateWeights");
		updateWeightKernel.setArgs(X, error, weight, datanumber, dimension, alpha);
		CLEvent addEvt = updateWeightKernel.enqueueNDRange(queue, globalSize);

		return weight;
	}

	public void calculateTestAUC(float[] testSet, float[] testLabels, int rowNumber, int colNumber, Weight weight) {
		float[] actualoutput = new float[rowNumber];
		for (int row = 0; row < rowNumber - 1; row++) {
			actualoutput[row] = neuron.calculateOutput(testSet, weight, row * colNumber, (row + 1) * colNumber);
		}
		// LOGGER.debug("TestAUC is " +
		// ErrorCalculation.calculateAUC(actualoutput, testLabels));
		System.out.println("AUC is " + ErrorCalculation.calculateAUC(actualoutput, testLabels));
	}

	public CLBuffer<Float> convertArrayIntoCLBuffer(float[] array, OpenCLResources resources) {
		Pointer<Float> oPointer = allocateFloats(array.length).order(resources.getByteOrder());
		oPointer.setFloats(array);
		CLBuffer<Float> outBuffer = resources.getContext().createBuffer(Usage.InputOutput, oPointer);
		return outBuffer;
	}

	public float[] getData(CLBuffer<Float> data, OpenCLResources resources) {
		Pointer<Float> outPtr = data.read(resources.getQueue());
		return outPtr.getFloats();
	}

	public static class Result {
		public float[] modelWeight;
		public int iteration;
		public int visibleNumber;
		public double rmse;
		public float accuracy;

		public Result(float[] modelWeight, int iteration, int visibleNumber, double rmse, float accuracy) {
			super();
			this.modelWeight = modelWeight;
			this.iteration = iteration;
			this.visibleNumber = visibleNumber;
			this.rmse = rmse;
			this.accuracy = accuracy;
		}

		public Result(int iteration, int visibleNumber, double rmse, float accuracy) {
			this.iteration = iteration;
			this.visibleNumber = visibleNumber;
			this.rmse = rmse;
			this.accuracy = accuracy;
		}

	}

}
