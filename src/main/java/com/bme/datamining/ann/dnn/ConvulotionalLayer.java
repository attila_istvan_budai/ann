package com.bme.datamining.ann.dnn;

import java.util.LinkedList;

import com.bme.datamining.ann.rbm.RestrictedBoltzmannMachineOpenCL;

public class ConvulotionalLayer {
	public LinkedList<RestrictedBoltzmannMachineOpenCL> rbmList;
	public int hiddenUnitNumber;
	public int visibleUnitNumber;

	public ConvulotionalLayer(int hiddenUnitNumber, int visibleUnitNumber) {
		super();
		this.rbmList = new LinkedList<>();
		this.hiddenUnitNumber = hiddenUnitNumber;
		this.visibleUnitNumber = visibleUnitNumber;
	}

	public void addRBM(RestrictedBoltzmannMachineOpenCL rbm) {
		rbmList.add(rbm);
	}

	public RestrictedBoltzmannMachineOpenCL getRBM(int index) {
		return rbmList.get(index);
	}

}
