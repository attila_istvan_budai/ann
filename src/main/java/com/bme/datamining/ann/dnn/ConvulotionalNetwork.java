package com.bme.datamining.ann.dnn;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import com.bme.datamining.ann.rbm.RestrictedBoltzmannMachineOpenCL;
import com.bme.datamining.logging.observer.Observer;

public class ConvulotionalNetwork {

	LinkedList<ConvulotionalLayer> layers;
	List<float[]> weights;
	private static ConvulotionalNetwork instance = null;
	Observer perfObserver;
	Observer observer;
	int slices;

	private ConvulotionalNetwork(LinkedList<ConvulotionalLayer> layers) {
		this.layers = layers;
		weights = new ArrayList<float[]>();
	}

	public LinkedList<ConvulotionalLayer> getLayers() {
		return layers;
	}

	public void setLayers(LinkedList<ConvulotionalLayer> layers) {
		this.layers = layers;
	}

	public List<float[]> getWeights() {
		return weights;
	}

	public void setWeights(List<float[]> weights) {
		this.weights = weights;
	}

	public int countSlices(int columnSize, int rowSize, int sliceWidth, int sliceHeight) {
		return columnSize / sliceWidth * rowSize / sliceHeight;

	}

	public float[] getSlice(float[] trainingSet, int columnSize, int rowSize, int sliceWidth, int sliceHeight, int index) {
		float[] sliceData = new float[sliceHeight * sliceWidth];
		int sliceCol = columnSize / sliceWidth;
		int sliceRow = rowSize / sliceHeight;
		int firstElementIndex = index / sliceCol * sliceHeight * columnSize + (index % sliceCol) * sliceWidth;
		for (int j = 0; j < sliceHeight; j++) {
			int start = firstElementIndex + j * columnSize;
			System.arraycopy(trainingSet, start, sliceData, j * sliceWidth, sliceWidth);
		}

		return sliceData;
	}

	public float[] createDataFromSlices(float[] trainingSet, int numberSamples, int columnSize, int rowSize, int sliceWidth, int sliceHeight, int index) {
		float[] sliceData = new float[sliceHeight * sliceWidth * numberSamples];
		int sliceSize = sliceHeight * sliceWidth;
		int slices = countSlices(columnSize, rowSize, sliceWidth, sliceHeight);
		for (int i = 0; i < numberSamples; i++) {
			int sliceIndex = i * slices + index;
			float[] res = getSlice(trainingSet, columnSize, rowSize, sliceWidth, sliceHeight, sliceIndex);
			System.arraycopy(res, 0, sliceData, i * sliceSize, sliceSize);
		}
		return sliceData;
	}

	public float[] train(float[] trainingSet, int numberSamples, final int ITERATION, float LEARNINGRATE, float MOMENTUM, final int LOGITERATION, final float UPDATE_WEIGHT_PARAM,
			int BATCHSIZE, int columnSize, int rowSize, int sliceWidth, int sliceHeight) throws Exception {
		if (layers == null) {
			throw new IOException("no layer!");
		}

		slices = countSlices(columnSize, rowSize, sliceWidth, sliceHeight);
		for (int i = 0; i < slices; i++) {
			System.out.println("slice " + i);
			float[] data = createDataFromSlices(trainingSet, numberSamples, columnSize, rowSize, sliceWidth, sliceHeight, i);
			RestrictedBoltzmannMachineOpenCL rbm = new RestrictedBoltzmannMachineOpenCL();

			if (observer != null) {
				rbm.registerObserver(observer);
			}
			setInitialWeight(rbm, 0);
			float[] result = rbm.train(data, sliceWidth * sliceHeight, layers.get(0).hiddenUnitNumber / slices, numberSamples, ITERATION, LEARNINGRATE, MOMENTUM, LOGITERATION,
					UPDATE_WEIGHT_PARAM, BATCHSIZE);
			layers.get(0).addRBM(rbm);
			addOrSetWeight(0, result);
		}

		// weights.set(0, result);

		RestrictedBoltzmannMachineOpenCL rbm = new RestrictedBoltzmannMachineOpenCL();
		layers.get(1).addRBM(rbm);

		if (observer != null) {
			rbm.registerObserver(observer);
		}
		setInitialWeight(rbm, 1);
		float[] input = new float[layers.get(0).hiddenUnitNumber * numberSamples];
		for (int i = 0; i < slices; i++) {
			RestrictedBoltzmannMachineOpenCL rbmSlice = layers.get(0).getRBM(0);
			float[] sliceData = RestrictedBoltzmannMachineOpenCL.stepForward(input, rbmSlice.getWeight(), layers.get(0).visibleUnitNumber, layers.get(0).hiddenUnitNumber / slices,
					numberSamples);
			System.arraycopy(sliceData, 0, input, i * layers.get(0).hiddenUnitNumber * numberSamples / slices, layers.get(0).hiddenUnitNumber * numberSamples / slices);

		}
		float[] result = rbm.train(input, layers.get(1).visibleUnitNumber, layers.get(1).hiddenUnitNumber, numberSamples, ITERATION, LEARNINGRATE, MOMENTUM, LOGITERATION,
				UPDATE_WEIGHT_PARAM, BATCHSIZE);
		// weights.set(i, result);
		addOrSetWeight(1, result);
		// result = backFitting(trainingSet, visibleUnitNumber, numberSamples,
		// ITERATION, LEARNINGRATE, MOMENTUM, LOGITERATION,
		// UPDATE_WEIGHT_PARAM);

		if (observer != null) {
			rbm.unRegisterObserver(observer);
		}
		if (perfObserver != null) {
			rbm.unRegisterPerformanceObserver(perfObserver);
		}
		return result;
	}

	public void addOrSetWeight(int index, float[] data) {
		if (weights.size() <= index) {
			weights.add(data);
		} else {
			weights.set(index, data);
		}

	}

	public void setInitialWeight(RestrictedBoltzmannMachineOpenCL rbm, int index) {
		if (weights.size() > index) {
			rbm.setInitialWeight(weights.get(index));
		}
	}

	public float[] reconstructData(float[] data, int numberSamples) {
		float[] d = data;
		for (int i = 0; i < layers.size(); i++) {
			d = RestrictedBoltzmannMachineOpenCL.stepForward(d, weights.get(i), layers.get(i).visibleUnitNumber, layers.get(i).hiddenUnitNumber, numberSamples);

		}
		System.out.println("hidden units in rec");
		printArray(d);
		for (int i = layers.size() - 1; i >= 0; i--) {
			d = RestrictedBoltzmannMachineOpenCL.stepBackward(d, weights.get(i), layers.get(i).visibleUnitNumber, layers.get(i).hiddenUnitNumber, numberSamples);
		}
		return d;
	}

	private void printArray(float[] data) {
		for (int i = 0; i < data.length; i++) {
			System.out.print(data[i] + " ");
		}
		System.out.println();
	}

	public float[] calculateHiddenLayer(float[] data, int numberSamples) {
		float[] input = new float[layers.get(0).hiddenUnitNumber * numberSamples];
		for (int i = 0; i < slices; i++) {
			RestrictedBoltzmannMachineOpenCL rbmSlice = layers.get(0).getRBM(0);
			float[] sliceData = RestrictedBoltzmannMachineOpenCL.stepForward(input, rbmSlice.getWeight(), layers.get(0).visibleUnitNumber, layers.get(0).hiddenUnitNumber / slices,
					numberSamples);
			System.arraycopy(sliceData, 0, input, i * layers.get(0).hiddenUnitNumber * numberSamples / slices, layers.get(0).hiddenUnitNumber * numberSamples / slices);

		}
		RestrictedBoltzmannMachineOpenCL rbm = layers.get(1).getRBM(0);
		float[] result = rbm.stepForward(input, rbm.getWeight(), layers.get(1).visibleUnitNumber, layers.get(1).hiddenUnitNumber, numberSamples);

		return result;
	}

	public float[] calculateHiddenLayer(float[] data, int numberSamples, int indexOfHiddenLayer) {
		float[] d = data;
		for (int i = 0; i < indexOfHiddenLayer; i++) {
			d = RestrictedBoltzmannMachineOpenCL.stepForward(d, weights.get(i), layers.get(i).visibleUnitNumber, layers.get(i).hiddenUnitNumber, numberSamples);
		}
		return d;
	}

	public void addLayer(int hiddenNumber, int visibleUnitNumber) {
		layers.add(new ConvulotionalLayer(hiddenNumber, visibleUnitNumber));
	}

	public void registerObserver(Observer obj) {
		this.observer = obj;
	}

	public void registerPerformanceObserver(Observer obj) {
		this.perfObserver = obj;
	}

	public static class ConvulotionalNetworkBuilder {
		LinkedList<ConvulotionalLayer> layers;
		List<float[]> weights;

		public ConvulotionalNetworkBuilder addFirstLayer(int hiddenNumber, int visibleUnitNumber) {
			layers = new LinkedList<ConvulotionalLayer>();
			this.layers.add(new ConvulotionalLayer(hiddenNumber, visibleUnitNumber));
			return this;
		}

		public ConvulotionalNetworkBuilder addLayer(int hiddenNumber) {
			this.layers.add(new ConvulotionalLayer(hiddenNumber, layers.getLast().hiddenUnitNumber));
			return this;
		}

		public ConvulotionalNetwork create() {
			return new ConvulotionalNetwork(layers);
		}

		public ConvulotionalNetwork createSingleton() {
			if (instance == null) {
				instance = new ConvulotionalNetwork(layers);
			}
			return instance;
		}
	}

}
