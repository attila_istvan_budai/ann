package com.bme.datamining.ann.dnn;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import com.bme.datamining.ann.ErrorCalculation;
import com.bme.datamining.ann.rbm.RBMLayer;
import com.bme.datamining.ann.rbm.RestrictedBoltzmannMachineOpenCL;
import com.bme.datamining.logging.observer.Observer;
import com.bme.datamining.svm.SVM;

import de.bwaldvogel.liblinear.SolverType;

public class DNNOpenCL {

	LinkedList<RBMLayer> layers;
	List<float[]> weights;
	private static DNNOpenCL instance = null;
	Observer perfObserver;
	Observer observer;
	SVM svm;

	private DNNOpenCL(LinkedList<RBMLayer> layers, SVM svm) {
		this.layers = layers;
		this.svm = svm;
		weights = new ArrayList<float[]>();
	}

	public LinkedList<RBMLayer> getLayers() {
		return layers;
	}

	public void setLayers(LinkedList<RBMLayer> layers) {
		this.layers = layers;
	}

	public List<float[]> getWeights() {
		return weights;
	}

	public void setWeights(List<float[]> weights) {
		this.weights = weights;
	}

	public SVM getSvm() {
		return svm;
	}

	public void setSvm(SVM svm) {
		this.svm = svm;
	}

	public float[] train(float[] trainingSet, int numberSamples, final int ITERATION, float LEARNINGRATE, float MOMENTUM, final int LOGITERATION, final float UPDATE_WEIGHT_PARAM,
			int BATCHSIZE, float[] trainingLabels) throws Exception {
		if (layers == null) {
			throw new IOException("no layer!");
		}
		if (trainingLabels == null && svm != null) {
			throw new Exception("no training labels for classification!");
		}

		RestrictedBoltzmannMachineOpenCL rbm;
		rbm = layers.get(0).rbm;
		if (observer != null) {
			rbm.registerObserver(observer);
		}
		if (perfObserver != null) {
			rbm.registerPerformanceObserver(perfObserver);
		}
		setInitialWeight(rbm, 0);
		float[] result = rbm.train(trainingSet, layers.get(0).visibleUnitNumber, layers.get(0).hiddenUnitNumber, numberSamples, ITERATION, LEARNINGRATE, MOMENTUM, LOGITERATION,
				UPDATE_WEIGHT_PARAM, BATCHSIZE);
		// weights.set(0, result);
		addOrSetWeight(0, result);
		float[] data = null;
		if (layers.size() > 1) {
			for (int i = 1; i < layers.size(); i++) {
				rbm = layers.get(i).rbm;

				if (observer != null) {
					rbm.registerObserver(observer);
				}
				if (perfObserver != null) {
					rbm.registerPerformanceObserver(perfObserver);
				}

				setInitialWeight(rbm, i);
				float[] input = null;
				if (data == null) {
					input = trainingSet;
				} else {
					input = data;
				}
				data = RestrictedBoltzmannMachineOpenCL.stepForward(input, weights.get(i - 1), layers.get(i - 1).visibleUnitNumber, layers.get(i - 1).hiddenUnitNumber,
						numberSamples);
				result = rbm.train(data, layers.get(i).visibleUnitNumber, layers.get(i).hiddenUnitNumber, numberSamples, ITERATION, LEARNINGRATE, MOMENTUM, LOGITERATION,
						UPDATE_WEIGHT_PARAM, BATCHSIZE);
				// weights.set(i, result);
				addOrSetWeight(i, result);
			}
		}
		// result = backFitting(trainingSet, visibleUnitNumber, numberSamples,
		// ITERATION, LEARNINGRATE, MOMENTUM, LOGITERATION,
		// UPDATE_WEIGHT_PARAM);

		if (svm != null) {
			learnSVM(trainingSet, trainingLabels, numberSamples, layers.getLast().hiddenUnitNumber);
		}

		if (observer != null) {
			rbm.unRegisterObserver(observer);
		}
		if (perfObserver != null) {
			rbm.unRegisterPerformanceObserver(perfObserver);
		}
		return result;
	}

	public float[] backFitting(float[] trainingSet, int visibleUnitNumber, int numberSamples, final int ITERATION, float LEARNINGRATE, float MOMENTUM, final int LOGITERATION,
			final float UPDATE_WEIGHT_PARAM, int BATCHSIZE) throws Exception {
		RestrictedBoltzmannMachineOpenCL rbm = layers.getLast().rbm;
		float[] data = calculateHiddenLayer(trainingSet, numberSamples);
		float[] result = rbm.train(data, layers.getLast().hiddenUnitNumber, layers.getLast().visibleUnitNumber, numberSamples, ITERATION, LEARNINGRATE, MOMENTUM, LOGITERATION,
				UPDATE_WEIGHT_PARAM, BATCHSIZE);
		addOrSetWeight(layers.size() - 1, result);
		if (layers.size() > 1) {
			for (int i = layers.size() - 2; i >= 0; i--) {
				rbm = layers.get(i).rbm;
				float[] input = null;
				if (data == null) {
					input = trainingSet;
				} else {
					input = data;
				}
				data = RestrictedBoltzmannMachineOpenCL.stepBackward(input, weights.get(i + 1), layers.get(i + 1).visibleUnitNumber, layers.get(i + 1).hiddenUnitNumber,
						numberSamples);
				result = rbm.train(data, layers.get(i).hiddenUnitNumber, layers.get(i).visibleUnitNumber, numberSamples, ITERATION, LEARNINGRATE, MOMENTUM, LOGITERATION,
						UPDATE_WEIGHT_PARAM, BATCHSIZE);
				// weights.set(i, result);
				addOrSetWeight(i, result);
			}
		}

		return result;
	}

	public void setSVMParameters(SolverType solve, double C, double eps) {
		svm.setSolverParameters(solve, C, eps);
	}

	public float learnSVM(float[] trainingSet, float[] trainingLabels, int numberSamples, int hiddenUnits) {
		float[] constructedData = this.calculateHiddenLayer(trainingSet, numberSamples);
		svm.learnLibLinear(constructedData, trainingLabels, trainingLabels.length, hiddenUnits, 0.01f, 100000, null);
		float[] predictions = svm.evaluateLibLinear(constructedData, trainingLabels, trainingLabels.length, hiddenUnits, 10, false);
		float trainingAccuracy = ErrorCalculation.calculateAccuracy(trainingLabels, predictions);
		System.out.println("Training accuracy is " + trainingAccuracy);
		// appendToFile("Training accuracy is " + accuracy);
		// return learnSVM(hiddenUnits, constructedData, constructedData);
		return trainingAccuracy;
	}

	public float evaulateTestData(float[] testSet, float[] testLabels, int numberSamples, int hiddenUnits) {
		float[] constructedTestData = this.calculateHiddenLayer(testSet, numberSamples);
		float[] predictions = svm.evaluateLibLinear(constructedTestData, testLabels, testLabels.length, hiddenUnits, 10, false);
		float testAccuracy = ErrorCalculation.calculateAccuracy(testLabels, predictions);
		System.out.println("Test accuracy is " + testAccuracy);
		return testAccuracy;
	}

	public float predict(float[] testSet, int numberSamples) {
		float[] constructedTestData = this.calculateHiddenLayer(testSet, numberSamples);
		float predictions = svm.predictOneLibLinear(constructedTestData, this.getLayers().getLast().hiddenUnitNumber, 10);
		return predictions;
	}

	public float learnlibSVM(float[] trainingSet, float[] trainingLabels, int numberSamples, int hiddenUnits) {
		float[] constructedData = this.calculateHiddenLayer(trainingSet, numberSamples);
		svm.learn(constructedData, trainingLabels, trainingLabels.length, hiddenUnits, 0.01f, 100000, null);
		float[] predictions = svm.evaluate(constructedData, trainingLabels, trainingLabels.length, hiddenUnits, 10);
		float trainingAccuracy = ErrorCalculation.calculateAccuracy(trainingLabels, predictions);
		System.out.println("Training accuracy is " + trainingAccuracy);
		// appendToFile("Training accuracy is " + accuracy);
		// return learnSVM(hiddenUnits, constructedData, constructedData);
		return trainingAccuracy;
	}

	public float evaulateLibSVMTestData(float[] testSet, float[] testLabels, int numberSamples, int hiddenUnits) {
		float[] constructedTestData = this.calculateHiddenLayer(testSet, numberSamples);
		float[] predictions = svm.evaluate(constructedTestData, testLabels, testLabels.length, hiddenUnits, 10);
		float testAccuracy = ErrorCalculation.calculateAccuracy(testLabels, predictions);
		System.out.println("Test accuracy is " + testAccuracy);
		return testAccuracy;
	}

	public void addOrSetWeight(int index, float[] data) {
		if (weights.size() <= index) {
			weights.add(data);
		} else {
			weights.set(index, data);
		}

	}

	public void setInitialWeight(RestrictedBoltzmannMachineOpenCL rbm, int index) {
		if (weights.size() > index) {
			rbm.setInitialWeight(weights.get(index));
		}
	}

	public float[] reconstructData(float[] data, int numberSamples) {
		float[] d = data;
		for (int i = 0; i < layers.size(); i++) {
			d = RestrictedBoltzmannMachineOpenCL.stepForward(d, weights.get(i), layers.get(i).visibleUnitNumber, layers.get(i).hiddenUnitNumber, numberSamples);

		}
		System.out.println("hidden units in rec");
		printArray(d);
		for (int i = layers.size() - 1; i >= 0; i--) {
			d = RestrictedBoltzmannMachineOpenCL.stepBackward(d, weights.get(i), layers.get(i).visibleUnitNumber, layers.get(i).hiddenUnitNumber, numberSamples);
		}
		return d;
	}

	private void printArray(float[] data) {
		for (int i = 0; i < data.length; i++) {
			System.out.print(data[i] + " ");
		}
		System.out.println();
	}

	public float[] calculateHiddenLayer(float[] data, int numberSamples) {
		float[] d = data;
		for (int i = 0; i < layers.size(); i++) {
			d = RestrictedBoltzmannMachineOpenCL.stepForward(d, weights.get(i), layers.get(i).visibleUnitNumber, layers.get(i).hiddenUnitNumber, numberSamples);
		}
		return d;
	}

	public float[] calculateHiddenLayer(float[] data, int numberSamples, int indexOfHiddenLayer) {
		float[] d = data;
		for (int i = 0; i < indexOfHiddenLayer; i++) {
			d = RestrictedBoltzmannMachineOpenCL.stepForward(d, weights.get(i), layers.get(i).visibleUnitNumber, layers.get(i).hiddenUnitNumber, numberSamples);
		}
		return d;
	}

	public void addLayer(int hiddenNumber, int visibleUnitNumber) {
		layers.add(new RBMLayer(new RestrictedBoltzmannMachineOpenCL(), hiddenNumber, visibleUnitNumber));
	}

	public void registerObserver(Observer obj) {
		this.observer = obj;
	}

	public void registerPerformanceObserver(Observer obj) {
		this.perfObserver = obj;
	}

	public static class DNNBuilder {
		LinkedList<RBMLayer> layers;
		List<float[]> weights;
		SVM svm;

		public SVM getSvm() {
			return svm;
		}

		public void setSvm(SVM svm) {
			this.svm = svm;
		}

		public DNNBuilder addFirstLayer(int hiddenNumber, int visibleUnitNumber) {
			layers = new LinkedList<RBMLayer>();
			this.layers.add(new RBMLayer(new RestrictedBoltzmannMachineOpenCL(), hiddenNumber, visibleUnitNumber));
			return this;
		}

		public DNNBuilder addLayer(int hiddenNumber) {
			this.layers.add(new RBMLayer(new RestrictedBoltzmannMachineOpenCL(), hiddenNumber, layers.getLast().hiddenUnitNumber));
			return this;
		}

		public DNNBuilder addSVMToTopLayer() {
			this.svm = new SVM();
			return this;
		}

		public DNNOpenCL create() {
			return new DNNOpenCL(layers, svm);
		}

		public DNNOpenCL createSingleton() {
			if (instance == null) {
				instance = new DNNOpenCL(layers, svm);
			}
			return instance;
		}
	}

}
