package com.bme.datamining.ann.rbm;

import org.apache.log4j.Logger;

import com.bme.data.densematrix.DenseMatrix;
import com.bme.data.densematrix.DenseMatrixOperations;
import com.bme.datamining.ann.ErrorCalculation;
import com.bme.datamining.ann.activatefunctions.SigmoidFunction;
import com.bme.datamining.ann.neurons.InnerNeuron;
import com.bme.datamining.ann.weights.MultiDimensionalWeight;
import com.bme.datamining.ann.weights.Weight;

public class RestrictedBoltzmannMachine {
	private InnerNeuron neuron;

	static final Logger LOGGER = Logger.getLogger(RestrictedBoltzmannMachine.class);

	public RestrictedBoltzmannMachine(InnerNeuron n) {
		neuron = n;

	}

	public Weight updateWeight(Weight weight, float[] positiveGradient, float[] negativeGradient, float alpha) {
		positiveGradient = DenseMatrixOperations.substractMatrix(positiveGradient, negativeGradient);
		// ResultLogger.logRMSE(ErrorCalculation.calculateRMSE(positiveGradient));
		// ResultLogger.logGradientDifference(positiveGradient, 3);
		positiveGradient = DenseMatrixOperations.multiplyMatrix(positiveGradient, alpha);
		weight.setWeight(DenseMatrixOperations.addMatrix(weight.getWeight(), positiveGradient));
		return weight;
	}

	public float[] calculateHiddenUnits(float[] visibleUnits, MultiDimensionalWeight weight, int visibleUnitNumber, int hiddenUnitNumber) {
		Weight w;
		float[] hiddenUnits = new float[hiddenUnitNumber];
		for (int i = 0; i < hiddenUnitNumber; i++) {
			w = weight.getColumn(i);
			hiddenUnits[i] = neuron.calculateOutput(visibleUnits, w, 0, visibleUnitNumber);
		}
		return hiddenUnits;
	}

	public float[] calculateVisibleUnits(float[] visibleUnits, float[] hiddenUnits, MultiDimensionalWeight weight, int visibleUnitNumber, int hiddenUnitNumber) {
		for (int i = 0; i < visibleUnitNumber; i++) {
			visibleUnits[i] = neuron.calculateOutput(hiddenUnits, weight, 0, hiddenUnitNumber);
		}
		return visibleUnits;

	}

	public void train(float[] trainingSet, int visibleUnitNumber, int hiddenUnitNumber, int numberSamples, final int ITERATION, final float LEARNINGRATE) {
		float[] w = new float[visibleUnitNumber * hiddenUnitNumber];
		DenseMatrix weight = new DenseMatrix(w, hiddenUnitNumber, visibleUnitNumber);
		DenseMatrix dataSet = new DenseMatrix(trainingSet, visibleUnitNumber, numberSamples);

		for (int i = 0; i < ITERATION; i++) {

			DenseMatrix positiveHiddenProbabilities = dataSet.dot(weight).applyFunction(new SigmoidFunction());
			DenseMatrix positiveGradient = dataSet.transpose().dot(positiveHiddenProbabilities);

			DenseMatrix negativeVisibleProbabilities = positiveHiddenProbabilities.dot(weight.transpose()).applyFunction(new SigmoidFunction());
			DenseMatrix negativeHiddenProbabilities = negativeVisibleProbabilities.dot(weight).applyFunction(new SigmoidFunction());
			DenseMatrix negativeGradient = negativeVisibleProbabilities.transpose().dot(negativeHiddenProbabilities);

			DenseMatrix grad = positiveGradient.substract(negativeGradient).multiplyWithConstant(LEARNINGRATE / (float) numberSamples);
			weight.add(grad);

			// System.out.println("posHiddens " + positiveHiddenProbabilities);
			// System.out.println("posGrad " + positiveGradient);
			// System.out.println("negVis " + negativeVisibleProbabilities);
			// System.out.println("negHid " + negativeHiddenProbabilities);
			// System.out.println("negGrad " + negativeGradient);
			// System.out.println("grad " + grad);
			// System.out.println("weight " + weight);
			System.out.println(ErrorCalculation.calculateRMSE(grad.getData()));

		}
		System.out.println(weight);
	}

}
