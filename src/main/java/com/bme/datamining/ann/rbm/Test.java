package com.bme.datamining.ann.rbm;

import java.io.IOException;
import java.util.List;

import com.bme.data.densematrix.DenseMatrix;
import com.bme.data.matrix.MatrixElement;
import com.bme.datamining.ann.activatefunctions.SigmoidFunction;
import com.bme.datamining.ann.neurons.InnerNeuron;
import com.bme.datamining.filereader.ReadFiles;

public class Test {

	public static void main(String[] args) throws IOException {
		// RestrictedBoltzmannMachine rbm = new RestrictedBoltzmannMachine(new
		// InnerNeuron(new SigmoidFunction()));

		Runnable r = new Runnable() {
			public void run() {
				try {
					testWithRealData();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		};

		Thread t = new Thread(r);
		t.start();

	}

	public static void testWithRealData() throws Exception {
		RestrictedBoltzmannMachineOpenCL rbm = new RestrictedBoltzmannMachineOpenCL();
		List<MatrixElement> elements = ReadFiles.load("./src/main/resources/ratings.train", " ");
		DenseMatrix matrix = new DenseMatrix(elements, 3952, 6040);

		float[] w = rbm.train(matrix.getData(), 3952, 100, 6040, 2000, 0.01f, 0.5f, 50, 0.01f, 6040);
	}

	public static void testWithRealDataWithJava() throws IOException {
		RestrictedBoltzmannMachine rbm = new RestrictedBoltzmannMachine(new InnerNeuron(new SigmoidFunction()));
		List<MatrixElement> elements = ReadFiles.load("./src/main/resources/ratings.train", " ");
		DenseMatrix matrix = new DenseMatrix(elements, 3952, 6040);

		rbm.train(matrix.getData(), 3952, 100, 6040, 10, 0.01f);
	}

	public void simpleTest() throws Exception {
		RestrictedBoltzmannMachineOpenCL rbm = new RestrictedBoltzmannMachineOpenCL();

		float[] input = new float[] { 1, 1, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 1 };
		float[] in = new float[4000 * 6000];
		float[] w = rbm.train(input, 6, 3, 6, 10000, 0.01f, 0.5f, 50, 0.01f, 6000);
	}

}
