package com.bme.datamining.ann.rbm;

public class RBMLayer {
	public RestrictedBoltzmannMachineOpenCL rbm;
	public int hiddenUnitNumber;
	public int visibleUnitNumber;

	public RBMLayer(RestrictedBoltzmannMachineOpenCL rbm, int hiddenUnitNumber, int visibleUnitNumber) {
		super();
		this.rbm = rbm;
		this.hiddenUnitNumber = hiddenUnitNumber;
		this.visibleUnitNumber = visibleUnitNumber;
	}

}