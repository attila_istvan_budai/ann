package com.bme.datamining.ann.rbm;

import static org.bridj.Pointer.allocateFloats;

import java.io.IOException;
import java.nio.ByteOrder;
import java.util.Arrays;

import org.bridj.Pointer;

import com.bme.data.clmatrix.CLMatrix;
import com.bme.datamining.ann.ErrorCalculation;
import com.bme.datamining.ann.weights.Weight;
import com.bme.datamining.logging.observer.Observer;
import com.bme.datamining.logging.observer.complexobserver.RBMScalePerformanceSubject;
import com.bme.datamining.logging.observer.simpleobserver.ConsoleObserver;
import com.bme.datamining.logging.observer.simpleobserver.RBMResultObject;
import com.bme.datamining.logging.observer.simpleobserver.RBMSubject;
import com.bme.opencl.OpenCLResources;
import com.nativelibs4java.opencl.CLBuffer;
import com.nativelibs4java.opencl.CLContext;
import com.nativelibs4java.opencl.CLMem.Usage;
import com.nativelibs4java.opencl.CLProgram;
import com.nativelibs4java.opencl.CLQueue;
import com.nativelibs4java.opencl.JavaCL;
import com.nativelibs4java.util.IOUtils;

public class RestrictedBoltzmannMachineOpenCL {

	Boolean LOGGER_ENABLED = false;
	RBMSubject simpleSubject;
	RBMScalePerformanceSubject performanceObject;
	float[] w;

	long executionTimes[];

	static OpenCLResources resources = initResources();

	public RestrictedBoltzmannMachineOpenCL() {
		simpleSubject = new RBMSubject();
		performanceObject = new RBMScalePerformanceSubject();
	}

	public float[] getWeight() {
		return w;
	}

	public void setInitialWeight(float[] w) {
		this.w = w;
	}

	public float[] getCurrentBatch(float[] array, int visibleUnitNumber, int batchSize, int index) {
		return Arrays.copyOfRange(array, visibleUnitNumber * batchSize * index, visibleUnitNumber * batchSize * (index + 1));
	}

	public float[] train(float[] trainingSet, int visibleUnitNumber, int hiddenUnitNumber, int numberSamples, final int ITERATION, final float INiTIAL_LEARNINGRATE,
			float MOMENTUM, final int LOGITERATION, float updateWeightParam, int BATCHSIZE) throws Exception {

		ConsoleObserver obs = new ConsoleObserver();
		registerObserver(obs);
		// registerPerformanceObserver(new ConsolePerformanceObserver());
		// ResultLogger.logInitializeData(visibleUnitNumber, hiddenUnitNumber,
		// LEARNINGRATE, trainingSet);
		long start = System.currentTimeMillis();
		// CLContext context = JavaCL.createBestContext();
		// CLQueue queue = context.createDefaultQueue();
		// ByteOrder byteOrder = context.getByteOrder();
		// String src =
		// IOUtils.readText(RestrictedBoltzmannMachineOpenCL.class.getResource("rbm.cl"));
		// CLProgram program = context.createProgram(src);
		// OpenCLResources resources = new OpenCLResources(context, queue,
		// byteOrder, program);

		if (w == null) {
			w = new float[visibleUnitNumber * hiddenUnitNumber];
			Weight weightVec = new Weight(w);
			weightVec.initializeNormalDistributedWeight();
			w = weightVec.getWeight();
		}

		// CLMatrix dataSet = convertArrayIntoClMatrix(trainingSet,
		// visibleUnitNumber, numberSamples, resources);

		double lastRmse = 1000;

		CLMatrix weight = convertArrayIntoClMatrix(w, hiddenUnitNumber, visibleUnitNumber, resources);

		executionTimes = new long[10];
		long timepositiveHiddenProbabilities = 0;
		long timedatasetTransposed = 0;
		long timepositiveGradient = 0;
		long timetransposedWeight = 0;
		long timenegativeVisibleProbabilities = 0;
		long timenegativeHiddenProbabilities = 0;
		long timetransposedNegativeVisibleProbabilities = 0;
		long timenegativeGradient = 0;
		long timegrad = 0;
		long timeWeightUpdate = 0;

		float learningRate = INiTIAL_LEARNINGRATE;
		CLMatrix lastDelta = null;
		double rmse = 0;
		long time = 0;
		for (int currentBatch = 0; currentBatch < numberSamples / BATCHSIZE; currentBatch++) {
			CLMatrix dataSet = convertArrayIntoClMatrix(getCurrentBatch(trainingSet, visibleUnitNumber, BATCHSIZE, currentBatch), visibleUnitNumber, BATCHSIZE, resources);
			for (int i = 0; i < ITERATION; i++) {
				// ResultLogger.logIteration(i + 1);
				resources.getQueue().finish();
				long startIteration = System.currentTimeMillis();
				// positive phase
				CLMatrix positiveHiddenProbabilities = dataSet.dotThenSigmoid(weight);
				// CLMatrix positiveHiddenProbabilities =
				// dataSet.dotThenSigmoid(weight);
				// positiveHiddenProbabilities.addVectorToMatrix(weightHidden).applySigmoid();
				timepositiveHiddenProbabilities = logExecutionTime(startIteration, 0);

				CLMatrix datasetTransposed = dataSet.transpose();
				timedatasetTransposed = logExecutionTime(timepositiveHiddenProbabilities, 1);

				CLMatrix positiveGradient = datasetTransposed.dot(positiveHiddenProbabilities);
				timepositiveGradient = logExecutionTime(timedatasetTransposed, 2);

				// negative phase
				CLMatrix transposedWeight = weight.transpose();
				timetransposedWeight = logExecutionTime(timepositiveGradient, 3);

				CLMatrix negativeVisibleProbabilities = positiveHiddenProbabilities.dotThenSigmoid(transposedWeight);
				// CLMatrix negativeVisibleProbabilities =
				// positiveHiddenProbabilities.dot(transposedWeight);
				// negativeVisibleProbabilities.addVectorToMatrix(weightVisible).applySigmoid();
				timenegativeVisibleProbabilities = logExecutionTime(timetransposedWeight, 4);

				CLMatrix negativeHiddenProbabilities = negativeVisibleProbabilities.dotThenSigmoid(weight);
				// CLMatrix negativeHiddenProbabilities =
				// negativeVisibleProbabilities.dot(weight);
				// negativeHiddenProbabilities.addVectorToMatrix(weightHidden).applySigmoid();
				timenegativeHiddenProbabilities = logExecutionTime(timenegativeVisibleProbabilities, 5);

				CLMatrix transposedNegativeVisibleProbabilities = negativeVisibleProbabilities.transpose();
				timetransposedNegativeVisibleProbabilities = logExecutionTime(timenegativeHiddenProbabilities, 6);

				CLMatrix negativeGradient = transposedNegativeVisibleProbabilities.dot(negativeHiddenProbabilities);
				timenegativeGradient = logExecutionTime(timetransposedNegativeVisibleProbabilities, 7);

				if (i % LOGITERATION == 0 && i != 0 || i == ITERATION - 1) {
					CLMatrix reconstructErr = negativeVisibleProbabilities.copy().substract(dataSet).mask(dataSet);
					rmse = ErrorCalculation.calculateRMSE(reconstructErr.getData());
					reconstructErr.freeMemory();
				}

				//
				// CLMatrix deltaVisible =
				// (dataSet.copy().substract(negativeVisibleProbabilities)).multiplyWithConstant(1
				// / (float) numberSamples);
				// CLMatrix deltaHidden =
				// (positiveHiddenProbabilities.substract(negativeHiddenProbabilities)).multiplyWithConstant(1
				// / (float) numberSamples);
				//
				// weightVisible.add(deltaVisible);
				// weightHidden.add(deltaHidden);
				//
				// deltaVisible.freeMemory();
				// deltaHidden.freeMemory();

				positiveHiddenProbabilities.freeMemory();
				datasetTransposed.freeMemory();
				transposedWeight.freeMemory();
				negativeVisibleProbabilities.freeMemory();
				negativeHiddenProbabilities.freeMemory();
				transposedNegativeVisibleProbabilities.freeMemory();

				// update phase
				CLMatrix delta = (positiveGradient.substract(negativeGradient)).multiplyWithConstant(1 / (float) numberSamples);
				CLMatrix grad;
				if (lastDelta == null) {
					grad = delta.multiplyWithConstant(learningRate);
					lastDelta = delta.copy();
				} else {
					grad = delta.multiplyWithConstant(learningRate).add(lastDelta.multiplyWithConstant(MOMENTUM));
					lastDelta.freeMemory();
					lastDelta = delta.copy();
				}

				timegrad = logExecutionTime(timenegativeGradient, 8);
				weight.add(grad);
				timeWeightUpdate = logExecutionTime(timegrad, 9);
				positiveGradient.freeMemory();
				negativeGradient.freeMemory();
				time = System.currentTimeMillis() - start;
				// System.out.println("Execution time is: " + time + " ms");
				if (i % LOGITERATION == 0 && i != 0 || i == ITERATION - 1) {
					learningRate = INiTIAL_LEARNINGRATE / (1 + updateWeightParam * i);
					// System.out.println(learningRate);
					float[] weightRes = weight.getData();
					simpleSubject.setRBMObj(new RBMResultObject(i, time, rmse, weightRes));
					executionTimes = new long[10];
				}

				lastRmse = rmse;

				// delta.freeMemory();
			}
			float[] weightRes = weight.getData();
			simpleSubject.setRBMObj(new RBMResultObject(ITERATION, time, rmse, weightRes));
			dataSet.freeMemory();
		}
		time = System.currentTimeMillis() - start;
		resources.getQueue().finish();
		w = weight.getData();
		// simpleSubject.setRBMObj(new RBMResultObject(ITERATION, time,
		// lastRmse, w));
		executionTimes = new long[10];

		// dataSet.freeMemory();
		weight.freeMemory();
		if (lastDelta != null) {
			lastDelta.freeMemory();
		}

		System.out.println(CLMatrix.matrices.toString());
		unRegisterObserver(obs);

		return w;

	}

	public void registerObserver(Observer obj) {
		simpleSubject.register(obj);
	}

	public void unRegisterObserver(Observer obj) {
		simpleSubject.unregister(obj);
	}

	public void registerPerformanceObserver(Observer obj) {
		performanceObject.register(obj);
	}

	public void unRegisterPerformanceObserver(Observer perfObserver) {
		performanceObject.unregister(perfObserver);

	}

	public long printExecutionTime(long timeBefore, String name) {
		if (LOGGER_ENABLED) {
			long currentTime = System.currentTimeMillis() - timeBefore;
			System.out.println(name + " time is: " + currentTime + " ms");
		}
		return System.currentTimeMillis();
	}

	public long logExecutionTime(long timeBefore, int i) {
		long currentTime = System.currentTimeMillis() - timeBefore;
		executionTimes[i] += currentTime;
		return System.currentTimeMillis();
	}

	public static float[] reConstruct(float[] trainingSet, float[] w, int visibleUnitNumber, int hiddenUnitNumber, int rows) {

		// Pointer<Integer> random = allocateInts(hiddenUnitNumber *
		// visibleUnitNumber).order(resources.getByteOrder());
		// ParallelRandom pRand = null;
		// try {
		// pRand = new ParallelRandom(resources.getQueue(), hiddenUnitNumber *
		// visibleUnitNumber, System.currentTimeMillis());
		// } catch (IOException e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }
		// pRand.next(random);

		CLMatrix dataSet = convertArrayIntoClMatrix(trainingSet, visibleUnitNumber, rows, resources);
		CLMatrix weight = convertArrayIntoClMatrix(w, hiddenUnitNumber, visibleUnitNumber, resources);

		CLMatrix positiveHiddenProbabilities = dataSet.dotThenSigmoid(weight);

		// negative phase
		CLMatrix transposedWeight = weight.transpose();

		CLMatrix negativeVisibleProbabilities = positiveHiddenProbabilities.dotThenSigmoid(transposedWeight);

		CLMatrix reconstructErr = negativeVisibleProbabilities.copy().substract(dataSet).mask(dataSet);
		double rmse = ErrorCalculation.calculateRMSE(reconstructErr.getData());
		reconstructErr.freeMemory();
		System.out.println("reconstruct rmse is" + rmse);

		dataSet.freeMemory();
		weight.freeMemory();
		positiveHiddenProbabilities.freeMemory();

		float[] data = negativeVisibleProbabilities.getData();
		negativeVisibleProbabilities.freeMemory();
		return data;
	}

	public static float[] stepForward(float[] trainingSet, float[] w, int visibleUnitNumber, int hiddenUnitNumber, int rows) {

		CLMatrix dataSet = convertArrayIntoClMatrix(trainingSet, visibleUnitNumber, rows, resources);
		CLMatrix weight = convertArrayIntoClMatrix(w, hiddenUnitNumber, visibleUnitNumber, resources);
		CLMatrix positiveHiddenProbabilities = dataSet.dotThenSigmoid(weight);

		dataSet.freeMemory();
		weight.freeMemory();

		// System.out.println(negativeVisibleProbabilities);
		float[] data = positiveHiddenProbabilities.getData();
		positiveHiddenProbabilities.freeMemory();
		dataSet = null;
		weight = null;
		positiveHiddenProbabilities = null;
		return data;

	}

	public static float[] stepForward(float[] trainingSet, float[] w, int visibleUnitNumber, int hiddenUnitNumber, int rows, Pointer<Integer> random) {

		CLMatrix dataSet = convertArrayIntoClMatrix(trainingSet, visibleUnitNumber, rows, resources);
		CLMatrix weight = convertArrayIntoClMatrix(w, hiddenUnitNumber, visibleUnitNumber, resources);
		CLMatrix positiveHiddenProbabilities = dataSet.dotThenSigmoid(weight);
		// CLMatrix positiveHiddenValues =
		// positiveHiddenProbabilities.toBinary(random);

		dataSet.freeMemory();
		weight.freeMemory();

		// System.out.println(negativeVisibleProbabilities);
		float[] data = positiveHiddenProbabilities.getData();
		positiveHiddenProbabilities.freeMemory();
		dataSet = null;
		weight = null;
		positiveHiddenProbabilities = null;
		return data;

	}

	public static float[] stepBackward(float[] hiddenUnits, float[] w, int visibleUnitNumber, int hiddenUnitNumber, int rows) {

		CLMatrix dataSet = convertArrayIntoClMatrix(hiddenUnits, hiddenUnitNumber, rows, resources);
		CLMatrix weight = convertArrayIntoClMatrix(w, hiddenUnitNumber, visibleUnitNumber, resources);

		// negative phase
		CLMatrix transposedWeight = weight.transpose();

		CLMatrix negativeVisibleProbabilities = dataSet.dotThenSigmoid(transposedWeight);

		dataSet.freeMemory();
		weight.freeMemory();
		transposedWeight.freeMemory();

		float[] data = negativeVisibleProbabilities.getData();
		negativeVisibleProbabilities.freeMemory();
		return data;

	}

	public static OpenCLResources initResources() {
		CLContext context = JavaCL.createBestContext();
		CLQueue queue = context.createDefaultQueue();
		ByteOrder byteOrder = context.getByteOrder();
		String src = "not found";
		try {
			src = IOUtils.readText(RestrictedBoltzmannMachineOpenCL.class.getResource("rbm.cl"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		CLProgram program = context.createProgram(src);
		OpenCLResources resources = new OpenCLResources(context, queue, byteOrder, program);
		return resources;
	}

	public static CLMatrix convertArrayIntoClMatrix(float[] array, int col, int row, OpenCLResources resources) {
		Pointer<Float> oPointer = allocateFloats(array.length).order(resources.getByteOrder());
		oPointer.setFloats(array);
		CLBuffer<Float> outBuffer = resources.getContext().createBuffer(Usage.InputOutput, oPointer);
		CLMatrix out = new CLMatrix(outBuffer, col, row, resources);
		oPointer.release();
		return out;
	}

}
