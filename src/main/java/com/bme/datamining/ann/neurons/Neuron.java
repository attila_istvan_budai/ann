package com.bme.datamining.ann.neurons;

import com.bme.datamining.ann.weights.Weight;

/**
 * The Interface Neuron handles the different kind of neurons in ANNs
 */
public interface Neuron {

	/**
	 * Calculate output.
	 *
	 * @param input
	 *            the input
	 * @param weight
	 *            the weight
	 * @param start
	 *            the start
	 * @param end
	 *            the end
	 * @return the float
	 */
	float calculateOutput(float[] input, Weight weight, int start, int end);

	/**
	 * Calculate output.
	 *
	 * @param input
	 *            the input
	 * @param weight
	 *            the weight
	 * @param start
	 *            the start
	 * @param end
	 *            the end
	 * @return the double
	 */
	double calculateOutput(double[] input, Weight weight, int start, int end);

}
