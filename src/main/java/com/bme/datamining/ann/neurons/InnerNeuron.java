package com.bme.datamining.ann.neurons;

import com.bme.datamining.ann.activatefunctions.ActivationFunction;
import com.bme.datamining.ann.weights.Weight;

// TODO: Auto-generated Javadoc
/**
 * The inner neuron
 */
public class InnerNeuron implements Neuron {

	private ActivationFunction func;

	/**
	 * Instantiates a new inner neuron.
	 *
	 * @param func
	 *            the func
	 */
	public InnerNeuron(ActivationFunction func) {
		this.func = func;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.bme.datamining.ann.Neuron#calculateOutput(float[],
	 * com.bme.datamining.ann.Weight, int, int)
	 */
	public float calculateOutput(float[] input, Weight weight, int start, int end) {
		float sum = 0;
		int i = start;
		for (i = start; i < end; i++) {
			sum += input[i] * weight.get(i - start);
		}
		sum += weight.get(i - end + 1);
		return func.calculateOutput(sum);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.bme.datamining.ann.Neuron#calculateOutput(double[],
	 * com.bme.datamining.ann.Weight, int, int)
	 */
	public double calculateOutput(double[] input, Weight weight, int start, int end) {
		double sum = 0;
		int i = start;
		for (i = start; i < end; i++) {
			sum += input[i] * weight.get(i - start);
		}
		sum += weight.get(i - end + 1);
		return func.calculateOutput(sum);
	}
}
