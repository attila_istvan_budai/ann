package com.bme.datamining.svm;

public class RealAndPredictedValuePairs implements Comparable<RealAndPredictedValuePairs> {
	private int realValue;
	private int predictedValue;

	public RealAndPredictedValuePairs(int realValue, int predictedValue) {
		this.realValue = realValue;
		this.predictedValue = predictedValue;
	}

	public int getRealValue() {
		return realValue;
	}

	public void setRealValue(int realValue) {
		this.realValue = realValue;
	}

	public int getPredictedValue() {
		return predictedValue;
	}

	public void setPredictedValue(int predictedValue) {
		this.predictedValue = predictedValue;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + predictedValue;
		result = prime * result + realValue;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RealAndPredictedValuePairs other = (RealAndPredictedValuePairs) obj;
		if (predictedValue != other.predictedValue)
			return false;
		if (realValue != other.realValue)
			return false;
		return true;
	}

	@Override
	public int compareTo(RealAndPredictedValuePairs o) {
		if (Integer.compare(realValue, o.realValue) != 0) {
			return Integer.compare(realValue, o.realValue);
		} else {
			return Integer.compare(predictedValue, o.predictedValue);
		}
	}

}