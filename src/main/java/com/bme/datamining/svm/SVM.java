package com.bme.datamining.svm;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;

import libsvm.svm;
import libsvm.svm_model;
import libsvm.svm_node;
import libsvm.svm_parameter;
import libsvm.svm_problem;

import com.bme.datamining.ann.ErrorCalculation;
import com.bme.datamining.ann.weights.Weight;

import de.bwaldvogel.liblinear.Feature;
import de.bwaldvogel.liblinear.FeatureNode;
import de.bwaldvogel.liblinear.Linear;
import de.bwaldvogel.liblinear.Model;
import de.bwaldvogel.liblinear.Parameter;
import de.bwaldvogel.liblinear.Problem;
import de.bwaldvogel.liblinear.SolverType;

public class SVM {

	Map<Integer, double[]> misPredictedElementIndexes;
	Map<Integer, Float> indexClassMap;
	Map<Integer, TreeMap<Double, Integer>> falsePredictionMap;
	Map<Integer, Integer> resultMap;
	Map<RealAndPredictedValuePairs, Integer> realVsPredicted;
	Model libLinearModel;

	svm_model libSVMmodel;

	SolverType solver = SolverType.L2R_LR; // -s 0
	double C = 4.0; // cost of constraints violation
	double eps = 0.001; // stopping criteria

	public Map<Integer, double[]> getMisPredictedElementIndexes() {
		return misPredictedElementIndexes;
	}

	public void loadModel(File file) {
		try {
			libLinearModel = Model.load(file);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void saveModel(File file) {
		try {
			if (libLinearModel != null) {
				libLinearModel.save(file);
			} else if (libSVMmodel != null) {
				FileOutputStream fos = new FileOutputStream(file.getAbsolutePath(), false);
				ObjectOutputStream out = new ObjectOutputStream(fos);
				out.writeObject(libSVMmodel);
				out.close();
				System.out.println("Object Persisted");
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public svm_model learn(float[] trainingSet, float[] trainingLabels, int rowNumber, int colNumber, final float LEARNING_RATE, final int ITERATION, Weight weight) {

		svm_problem prob = new svm_problem();
		prob.y = new double[rowNumber];
		prob.l = rowNumber;
		prob.x = new svm_node[rowNumber][];

		for (int i = 0; i < rowNumber; i++) {
			prob.x[i] = new svm_node[colNumber];
			prob.y[i] = trainingLabels[i];
			for (int j = 0; j < colNumber; j++) {
				svm_node node = new svm_node();
				node.index = j;
				node.value = trainingSet[i * colNumber + j];
				prob.x[i][j] = node;
			}
		}

		svm_parameter param = new svm_parameter();
		param.C = 4;
		param.kernel_type = svm_parameter.C_SVC;
		param.eps = 0.001;

		libSVMmodel = svm.svm_train(prob, param);

		return libSVMmodel;
	}

	public float[] evaluate(float[] testSet, float[] testLabels, int rowNumber, int colNumber, int numberOfClasses) {
		svm_node[][] nodes = new svm_node[rowNumber][];

		for (int i = 0; i < rowNumber; i++) {
			svm_node[] row = new svm_node[colNumber];
			for (int j = 0; j < colNumber; j++) {
				svm_node node = new svm_node();
				node.index = j;
				node.value = testSet[i * colNumber + j];
				row[j] = node;
			}
			nodes[i] = row;
		}

		int[] labels = new int[numberOfClasses];
		svm.svm_get_labels(libSVMmodel, labels);

		double[] prob_estimates = new double[numberOfClasses];
		float[] predictions = new float[rowNumber];
		for (int i = 0; i < rowNumber; i++) {
			predictions[i] = (float) svm.svm_predict_probability(libSVMmodel, nodes[i], prob_estimates);
			System.out.println("(Actual:" + testLabels[i] + " Prediction:" + predictions[i] + ")");
			for (int j = 0; j < numberOfClasses; j++) {
				System.out.print("(" + labels[j] + ":" + prob_estimates[j] + ")");
			}
		}
		float accuracy = ErrorCalculation.calculateAccuracy(testLabels, predictions);
		System.out.println("Äccuracy is " + accuracy);

		return predictions;
	}

	public void learnLibLinear(float[] trainingSet, float[] trainingLabels, int rowNumber, int colNumber, final float LEARNING_RATE, final int ITERATION, Weight weight) {

		Problem prob = new Problem();
		prob.y = new double[rowNumber];
		prob.l = rowNumber;
		prob.n = colNumber;
		prob.x = new Feature[rowNumber][];

		for (int i = 0; i < rowNumber; i++) {
			prob.x[i] = new Feature[colNumber];
			prob.y[i] = trainingLabels[i];
			for (int j = 0; j < colNumber; j++) {
				Feature node = new FeatureNode(j + 1, trainingSet[i * colNumber + j]);
				prob.x[i][j] = node;
			}
		}
		Parameter parameter = new Parameter(solver, C, eps);
		libLinearModel = Linear.train(prob, parameter);

		// return libLinearModel;
	}

	public void setSolverParameters(SolverType solverType, double C, double eps) {
		this.C = C;
		this.solver = solverType;
		this.eps = eps;
	}

	public float predictOneLibLinear(float[] testSet, int colNumber, int numberOfClasses) {
		float[] pred = predictLibLinear(testSet, 1, colNumber, numberOfClasses);
		return pred[0];
	}

	public float[] predictLibLinear(float[] testSet, int rowNumber, int colNumber, int numberOfClasses) {
		Feature[][] nodes = new Feature[rowNumber][];

		for (int i = 0; i < rowNumber; i++) {
			Feature[] row = new Feature[colNumber];
			for (int j = 0; j < colNumber; j++) {
				Feature node = new FeatureNode(j + 1, testSet[i * colNumber + j]);
				row[j] = node;
			}
			nodes[i] = row;
		}
		float[] predictions = new float[rowNumber];
		for (int i = 0; i < rowNumber; i++) {
			double[] prob_estimates = new double[numberOfClasses];
			predictions[i] = (float) Linear.predictProbability(libLinearModel, nodes[i], prob_estimates);
		}
		return predictions;
	}

	public float[] evaluateLibLinear(float[] testSet, float[] testLabels, int rowNumber, int colNumber, int numberOfClasses, boolean logPrediction) {
		misPredictedElementIndexes = new HashMap<>();
		indexClassMap = new HashMap<>();
		falsePredictionMap = new HashMap<>();
		realVsPredicted = new TreeMap<>();

		Feature[][] nodes = new Feature[rowNumber][];

		for (int i = 0; i < rowNumber; i++) {
			Feature[] row = new Feature[colNumber];
			for (int j = 0; j < colNumber; j++) {
				Feature node = new FeatureNode(j + 1, testSet[i * colNumber + j]);
				row[j] = node;
			}
			nodes[i] = row;
		}

		int[] labels = new int[numberOfClasses];
		labels = libLinearModel.getLabels();

		Set<Float> nums = new HashSet<Float>();
		for (int i = 0; i < 10; i++) {
			nums.add((float) i);
		}

		float[] predictions = new float[rowNumber];
		for (int i = 0; i < rowNumber; i++) {
			double[] prob_estimates = new double[numberOfClasses];
			predictions[i] = (float) Linear.predictProbability(libLinearModel, nodes[i], prob_estimates);
			if (predictions[i] != testLabels[i]) {
				misPredictedElementIndexes.put(i, prob_estimates);
				RealAndPredictedValuePairs realPredicatedPair = new RealAndPredictedValuePairs((int) testLabels[i], (int) predictions[i]);
				if (realVsPredicted.containsKey(realPredicatedPair)) {
					int count = realVsPredicted.get(realPredicatedPair);
					count++;
					realVsPredicted.put(realPredicatedPair, count);
				} else {
					realVsPredicted.put(realPredicatedPair, 1);
				}
			}
			if (nums.contains(predictions[i])) {
				int index = getMaxIndex(prob_estimates);
				indexClassMap.put(index, predictions[i]);
				nums.remove(predictions[i]);

			}

			if (logPrediction)
				System.out.println("(Actual:" + testLabels[i] + " Prediction:" + predictions[i] + ")");

		}

		if (nums.size() != 0) {
			for (Float num : nums) {
				indexClassMap.put(8, num);
			}
		}

		System.out.println("------------------------------------------");
		for (Entry<Integer, Float> entry : indexClassMap.entrySet()) {
			System.out.println("ArrayIndex: " + entry.getKey() + " ActualValue: " + entry.getValue());
		}
		System.out.println("------------------------------------------");

		System.out.println("--------------------------");
		for (Entry<RealAndPredictedValuePairs, Integer> entry : realVsPredicted.entrySet()) {
			Float real = indexClassMap.get(entry.getKey().getRealValue());
			Float predicted = indexClassMap.get(entry.getKey().getPredictedValue());

			System.out.println("Real value is " + real + " Predicted value is " + predicted + " count: " + entry.getValue());
		}
		System.out.println("--------------------------");

		addToTreeMap(testLabels);
		calculateAccuracy(testLabels);

		return predictions;
	}

	private void addToTreeMap(float[] testLabels) {

		Iterator<Entry<Integer, double[]>> iter = misPredictedElementIndexes.entrySet().iterator();
		while (iter.hasNext()) {
			TreeMap<Double, Integer> valueIndexMap = new TreeMap<>(Collections.reverseOrder());
			Entry<Integer, double[]> entry = iter.next();
			Integer index = entry.getKey();
			double[] prob_estimates = entry.getValue();
			for (int i = 0; i < prob_estimates.length; i++) {
				float realIndexValue = indexClassMap.get(i);
				valueIndexMap.put(prob_estimates[i], (int) realIndexValue);
			}
			falsePredictionMap.put(index, valueIndexMap);

		}
	}

	public void calculateAccuracy(float[] testLabels) {
		Iterator<Entry<Integer, TreeMap<Double, Integer>>> iter = falsePredictionMap.entrySet().iterator();
		resultMap = new HashMap<>();
		for (int i = 1; i <= 10; i++) {
			resultMap.put(i, 0);
		}

		while (iter.hasNext()) {
			Entry<Integer, TreeMap<Double, Integer>> entry = iter.next();
			int i = 1;
			for (Entry<Double, Integer> e : entry.getValue().entrySet()) {
				if (e.getValue() == (int) testLabels[entry.getKey()]) {
					int topN = resultMap.get(i);
					topN++;
					resultMap.put(i, topN);
				}
				i++;
			}

		}
		System.out.println("--------------------------");
		for (Entry<Integer, Integer> entry : resultMap.entrySet()) {
			System.out.println("top" + entry.getKey() + " " + entry.getValue());
		}
		System.out.println("--------------------------");
	}

	public int getMaxIndex(double[] array) {
		int maxIndex = 0;
		for (int i = 1; i < array.length; i++) {
			double newnumber = array[i];
			if ((newnumber > array[maxIndex])) {
				maxIndex = i;
			}
		}
		return maxIndex;
	}

}
