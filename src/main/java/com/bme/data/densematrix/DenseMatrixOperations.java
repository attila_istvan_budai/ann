package com.bme.data.densematrix;

import com.bme.datamining.ann.activatefunctions.ActivationFunction;

public class DenseMatrixOperations {

    public static float[] getSelectedColumn(float[] array, int selectedColumn,
            int columnNumber, int rowNumber) {
        float[] col = new float[rowNumber];
        for (int i = 0; i < rowNumber; i++) {
            col[i] = array[i * columnNumber + selectedColumn];
        }
        return col;
    }

    public static float[] calculateOuterProduct(float[] row, float[] column) {
        int columnSize = column.length;
        int rowSize = row.length;
        int productSize = columnSize * rowSize;

        float[] outerProduct = new float[productSize];
        for (int i = 0; i < rowSize; i++) {
            for (int j = 0; j < columnSize; j++) {
                outerProduct[j * rowSize + i] = row[i] * column[j];
            }
        }
        return outerProduct;

    }

    public static float[] calculateOuterProductWithWeight(float[] row,
            float[] column) {
        int columnSize = column.length;
        int rowSize = row.length;
        int productSize = (columnSize + 1) * (rowSize + 1);

        float[] outerProduct = new float[productSize];
        for (int i = 0; i < rowSize; i++) {
            for (int j = 0; j < columnSize; j++) {
                outerProduct[j * (rowSize + 1) + i] = row[i] * column[j];
            }
        }

        for (int i = 0; i < rowSize; i++) {
            outerProduct[(rowSize + 1) * columnSize + i] = row[i];
        }

        for (int i = 0; i < columnSize; i++) {
            outerProduct[(i) * (rowSize + 1) + rowSize] = column[i];
        }
        return outerProduct;

    }

    public static float[] substractMatrix(float[] first, float[] second) {
        for (int i = 0; i < first.length; i++) {
            first[i] -= second[i];
        }
        return first;
    }

    public static float[] addMatrix(float[] first, float[] second) {
        for (int i = 0; i < first.length; i++) {
            first[i] += second[i];
        }
        return first;
    }

    public static float[] multiplyMatrix(float[] matrix, float alpha) {
        for (int i = 0; i < matrix.length; i++) {
            matrix[i] *= alpha;
        }
        return matrix;
    }
    
    public static float[] multiplyByMatrix(float[] m1, float[] m2, int m1ColumnLength, int m2ColumnLength) {
    	int m1RowLength = m1.length/m1ColumnLength;
        int m2RowLength = m2.length/m2ColumnLength;
        if(m1ColumnLength != m2RowLength) return null; // matrix multiplication is not possible
        float[] mResult = new float[m1RowLength*m2ColumnLength];
        for(int i = 0; i < m1RowLength; i++) {         // rows from m1
            for(int j = 0; j < m2ColumnLength; j++) {     // columns from m2
                for(int k = 0; k < m1ColumnLength; k++) { // columns from m1
                    mResult[i*m2ColumnLength+j] += m1[i*m1ColumnLength+k] * m2[k*m2ColumnLength+j];
                }
            }
        }
        return mResult;
    }
    
    public static float[] transposeMatrix(float[] matrix, int columnLength){
    	int rowLength = matrix.length / columnLength;
    	float[] retMatrix = new float[matrix.length];
    	for(int i = 0; i < columnLength; i++) {
    		  for(int j = 0; j < rowLength; j++) {
    			  retMatrix[i*rowLength+j] = matrix[j*columnLength+i];

    		  }
    		}
    	return retMatrix;
    }
    
    public static float[] applyFunction(float[] matrix, ActivationFunction func){
    	for(int i = 0; i < matrix.length; i++) {
    		matrix[i] = func.calculateOutput(matrix[i]);
    	}
    	return matrix;
    }
}
