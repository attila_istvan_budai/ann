package com.bme.data.densematrix;

import java.util.List;

import com.bme.data.matrix.Matrix;
import com.bme.data.matrix.MatrixElement;
import com.bme.datamining.ann.activatefunctions.ActivationFunction;

public class DenseMatrix extends Matrix {

	private int columnLength;
	private int rowLength;
	private float[] data;

	public DenseMatrix(float[] d, int columnLength, int rowLength) {
		this.data = d;
		this.columnLength = columnLength;
		this.rowLength = rowLength;
	}

	public DenseMatrix(List<MatrixElement> elements, int columnLength, int rowLength) {
		this.columnLength = columnLength;
		this.rowLength = rowLength;
		this.data = new float[columnLength * rowLength];
		for (MatrixElement elem : elements) {
			this.data[elem.getRowNumber() * columnLength + elem.getColumnNumber()] = elem.getData();
		}
	}

	public int getColumnLength() {
		return columnLength;
	}

	public void setColumnLength(int columnLength) {
		this.columnLength = columnLength;
	}

	public int getRowLength() {
		return rowLength;
	}

	public void setRowLength(int rowLength) {
		this.rowLength = rowLength;
	}

	public float[] getData() {
		return data;
	}

	public void setData(float[] data) {
		this.data = data;
	}

	public float getAtIndex(int index) {
		return this.data[index];
	}

	public float getAtIndex(int column, int row) {
		return this.data[row * columnLength + column];
	}

	public void setAtIndex(float num, int index) {
		this.data[index] = num;
	}

	public void setAtIndex(float num, int column, int row) {
		this.data[row * columnLength + column] = num;
	}

	public DenseMatrix add(DenseMatrix other) {
		float[] d = DenseMatrixOperations.addMatrix(this.getData(), other.getData());
		return new DenseMatrix(d, columnLength, rowLength);

	}

	public DenseMatrix substract(DenseMatrix other) {
		float[] d = DenseMatrixOperations.substractMatrix(this.getData(), other.getData());
		return new DenseMatrix(d, columnLength, rowLength);

	}

	public DenseMatrix multiplyWithConstant(float lEARNINGRATE) {
		float[] d = DenseMatrixOperations.multiplyMatrix(this.getData(), lEARNINGRATE);
		return new DenseMatrix(d, columnLength, rowLength);
	}

	public DenseMatrix dot(DenseMatrix other) {
		float[] d = DenseMatrixOperations.multiplyByMatrix(this.getData(), other.getData(), this.getColumnLength(), other.getColumnLength());
		return new DenseMatrix(d, other.getColumnLength(), rowLength);
	}

	public DenseMatrix transpose() {
		float[] d = DenseMatrixOperations.transposeMatrix(this.getData(), this.getColumnLength());
		return new DenseMatrix(d, rowLength, columnLength);
	}

	public DenseMatrix applyFunction(ActivationFunction func) {
		float[] d = DenseMatrixOperations.applyFunction(this.getData(), func);
		return new DenseMatrix(d, columnLength, rowLength);
	}

	public String toString() {
		String str = "";
		for (int i = 0; i < data.length; i++) {
			if (i % columnLength == 0 && columnLength != 0)
				str += '\n';
			str += data[i] + "  ";
		}
		return str;
	}

}
