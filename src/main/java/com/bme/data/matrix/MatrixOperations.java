package com.bme.data.matrix;

public interface MatrixOperations {

	public Matrix add(Matrix other);

	public Matrix substract(Matrix other);

	public Matrix multiplyWithConstant(float lEARNINGRATE);

	public Matrix dot(Matrix other);

	public Matrix transpose();

}
