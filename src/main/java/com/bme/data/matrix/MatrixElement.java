package com.bme.data.matrix;

public class MatrixElement {

	private float data;
	private int columnNumber;
	private int rowNumber;

	public MatrixElement(float data, int columnNumber, int rowNumber) {
		this.data = data;
		this.columnNumber = columnNumber;
		this.rowNumber = rowNumber;
	}

	public float getData() {
		return data;
	}

	public void setData(float data) {
		this.data = data;
	}

	public int getColumnNumber() {
		return columnNumber;
	}

	public void setColumnNumber(int columnNumber) {
		this.columnNumber = columnNumber;
	}

	public int getRowNumber() {
		return rowNumber;
	}

	public void setRowNumber(int rowNumber) {
		this.rowNumber = rowNumber;
	}

}