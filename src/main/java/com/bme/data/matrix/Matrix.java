package com.bme.data.matrix;

public abstract class Matrix {

	public abstract int getColumnLength();

	public abstract void setColumnLength(int columnLength);

	public abstract int getRowLength();

	public abstract void setRowLength(int rowLength);

	public abstract float[] getData();

}
