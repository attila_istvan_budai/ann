package com.bme.data.clmatrix;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.bridj.Pointer;

import com.bme.opencl.OpenCLResources;
import com.nativelibs4java.opencl.CLBuffer;
import com.nativelibs4java.opencl.CLEvent;
import com.nativelibs4java.opencl.CLException;
import com.nativelibs4java.opencl.CLKernel;
import com.nativelibs4java.opencl.CLMem;
import com.nativelibs4java.opencl.CLMem.Usage;
import com.nativelibs4java.opencl.CLQueue;
import com.nativelibs4java.opencl.LocalSize;

public class CLMatrixOperations {

	static final Logger LOGGER = Logger.getLogger(CLMatrixOperations.class);

	public static CLBuffer<Float> multiplyByMatrix(CLBuffer<Float> m1, CLBuffer<Float> m2, CLBuffer<Float> out, int m1ColumnLength, int m2ColumnLength, int m1rowSize,
			OpenCLResources res) {
		LOGGER.debug("multiplyByMatrix start..");
		CLQueue queue = res.getQueue();
		queue.finish();

		int[] globalSize = new int[] { m2ColumnLength, m1rowSize };

		CLKernel matrixMultiplicationKernel = res.createKernel("matrixMultiplication");
		matrixMultiplicationKernel.setArgs(out, m1, m2, m1ColumnLength);

		CLEvent addEvt = matrixMultiplicationKernel.enqueueNDRange(queue, globalSize);

		return out;

		// matrixMultiplicationKernel.release();
	}

	public static CLBuffer<Float> multiplyByMatrixV2(CLBuffer<Float> m1, CLBuffer<Float> m2, CLBuffer<Float> out, int m1ColumnLength, int m2ColumnLength, int m1rowSize,
			OpenCLResources res) {
		LOGGER.debug("multiplyByMatrixV2 start..");
		CLQueue queue = res.getQueue();
		queue.finish();

		// System.out.println("Pdim  " + m1ColumnLength);
		// System.out.println("Mdim  " + m2ColumnLength);
		// System.out.println("Ndim  " + m1rowSize);

		int[] globalSize = new int[] { roundUpToMultiply(m2ColumnLength, 28) };
		int[] localWorkSize = new int[] { roundUpToMultiply(m2ColumnLength, 28) / 28 };

		// globalSize[1] = roundUpTo64(m1rowSize);

		// CLKernel matrixMultiplicationKernel = res.createKernel("mmul");
		// matrixMultiplicationKernel.setArgs(m2ColumnLength, m1rowSize,
		// m1ColumnLength, m1, m2, out);

		CLKernel matrixMultiplicationKernel = res.createKernel("mmul_private");
		matrixMultiplicationKernel.setArgs(m1ColumnLength, m2ColumnLength, m1rowSize, m1, m2, out, new LocalSize(m2ColumnLength));

		// if (m2ColumnLength > 20) {
		// CLEvent addEvt = matrixMultiplicationKernel.enqueueNDRange(queue,
		// globalSize, localWorkSize);
		// } else {
		matrixMultiplicationKernel.enqueueNDRange(queue, globalSize, localWorkSize);
		// }

		return out;

		// matrixMultiplicationKernel.release();
	}

	public static CLBuffer<Float> multiplyByMatrixV3(CLBuffer<Float> m1, CLBuffer<Float> m2, CLBuffer<Float> out, int m1ColumnLength, int m2ColumnLength, int m1rowSize,
			OpenCLResources res) {
		LOGGER.debug("multiplyByMatrixV3 start..");
		final int BLOCK_SIZE = 16;
		CLQueue queue = res.getQueue();
		queue.finish();

		Pointer<Integer> q = Pointer.allocateInt();
		q.set(16);
		CLBuffer<Integer> qInputBuffer = res.getContext().createIntBuffer(Usage.Input, q);
		CLKernel kernel = res.createKernel("floatMatrixMultLocals");
		int[] localWorkSizes = new int[] { BLOCK_SIZE, BLOCK_SIZE };
		int[] globalWorkSizes = new int[] { m1rowSize, m2ColumnLength };

		CLEvent clEvent = null;
		kernel.setArgs(out, //
				m1, //
				m2, //
				qInputBuffer);

		kernel.enqueueNDRange(queue, globalWorkSizes, localWorkSizes);
		return out;

	}

	static int roundUpToMultiply(int input, int toRoundUp) {
		return (input + toRoundUp - 1) / toRoundUp * toRoundUp;
	}

	public static CLBuffer<Float> multiplyByMatrix(CLBuffer<Float> m1, CLBuffer<Float> m2, int m1ColumnLength, int m2ColumnLength, int m1rowSize, OpenCLResources res) {
		CLBuffer<Float> dataBuffer = res.getContext().createFloatBuffer(CLMem.Usage.InputOutput, m2ColumnLength * m1rowSize);
		return multiplyByMatrix(m1, m2, dataBuffer, m1ColumnLength, m2ColumnLength, m1rowSize, res);
	}

	public static CLBuffer<Float> multiplyByMatrixV2(CLBuffer<Float> m1, CLBuffer<Float> m2, int m1ColumnLength, int m2ColumnLength, int m1rowSize, OpenCLResources res) {
		CLBuffer<Float> dataBuffer = res.getContext().createFloatBuffer(CLMem.Usage.InputOutput, m2ColumnLength * m1rowSize);
		return multiplyByMatrixV2(m1, m2, dataBuffer, m1ColumnLength, m2ColumnLength, m1rowSize, res);
	}

	public static CLBuffer<Float> multiplyByMatrixWithSigmoid(CLBuffer<Float> m1, CLBuffer<Float> weight, CLBuffer<Float> out, int m1ColumnLength, int m2ColumnLength,
			int m1rowSize, OpenCLResources res) {
		LOGGER.debug("multiplyByMatrixWithSigmoid start..");
		CLQueue queue = res.getQueue();
		queue.finish();

		int[] globalSize = new int[] { m2ColumnLength, m1rowSize };

		CLKernel matrixMultiplicationKernel = res.createKernel("matrixMultiplicationWithSigmoid");
		matrixMultiplicationKernel.setArgs(out, m1, weight, m1ColumnLength);
		try {
			matrixMultiplicationKernel.enqueueNDRange(queue, globalSize);
		} catch (CLException e) {
			System.out.println("matrix1 columnLength " + m1ColumnLength);
			System.out.println("matrix2 columnLength " + m2ColumnLength);
			System.out.println("rowsize " + m1rowSize);
		}

		return out;
	}

	public static CLBuffer<Float> multiplyByMatrixWithSigmoidV2(CLBuffer<Float> m1, CLBuffer<Float> m2, CLBuffer<Float> out, int m1ColumnLength, int m2ColumnLength, int m1rowSize,
			OpenCLResources res) {
		LOGGER.debug("multiplyByMatrixWithSigmoidV2 start..");
		CLQueue queue = res.getQueue();
		queue.finish();

		int[] globalSize = new int[] { roundUpToMultiply(m2ColumnLength, 64), roundUpToMultiply(m1rowSize, 64) };

		CLKernel matrixMultiplicationKernel = res.createKernel("mmul_private_sigmoid");
		matrixMultiplicationKernel.setArgs(m1ColumnLength, m2ColumnLength, m1rowSize, m1, m2, out);

		matrixMultiplicationKernel.enqueueNDRange(queue, globalSize);

		return out;
	}

	public static CLBuffer<Float> applySigmoid(CLBuffer<Float> m1, int m1ColumnLength, int m1rowSize, OpenCLResources res) {
		LOGGER.debug("applySigmoid start..");
		CLQueue queue = res.getQueue();
		queue.finish();
		int[] globalSize = new int[] { m1ColumnLength * m1rowSize };

		CLKernel applySigmoidKernel = res.createKernel("applySigmoid");
		applySigmoidKernel.setArgs(m1);

		applySigmoidKernel.enqueueNDRange(queue, globalSize);
		return m1;
	}

	public static CLBuffer<Float> multiplyByMatrixWithSigmoid(CLBuffer<Float> m1, CLBuffer<Float> weight, int m1ColumnLength, int wColumnLength, int m1rowSize, OpenCLResources res) {
		LOGGER.debug("multiplyByMatrixWithSigmoid start..");
		CLQueue queue = res.getQueue();
		queue.finish();
		CLBuffer<Float> dataBuffer = res.getContext().createFloatBuffer(CLMem.Usage.InputOutput, (wColumnLength) * m1rowSize);
		return multiplyByMatrixWithSigmoid(m1, weight, dataBuffer, m1ColumnLength, (wColumnLength), m1rowSize, res);

	}

	public static CLBuffer<Float> multiplyByMatrixWithSigmoidV2(CLBuffer<Float> m1, CLBuffer<Float> m2, int m1ColumnLength, int m2ColumnLength, int m1rowSize, OpenCLResources res) {
		LOGGER.debug("multiplyByMatrixWithSigmoidV2 start..");
		CLQueue queue = res.getQueue();
		queue.finish();
		CLBuffer<Float> dataBuffer = res.getContext().createFloatBuffer(CLMem.Usage.InputOutput, m2ColumnLength * m1rowSize);
		return multiplyByMatrixWithSigmoidV2(m1, m2, dataBuffer, m1ColumnLength, m2ColumnLength, m1rowSize, res);

	}

	public static CLBuffer<Float> transpose(CLBuffer<Float> m1, int columnNumber, int rowNumber, OpenCLResources res) {
		LOGGER.debug("transpose start..");
		CLQueue queue = res.getQueue();
		queue.finish();
		CLBuffer<Float> out = res.getContext().createFloatBuffer(CLMem.Usage.InputOutput, columnNumber * rowNumber);
		// CLBuffer<Float> out =
		// CLMemoryManager.createEmptyCLBuffer(columnNumber * rowNumber);
		int[] globalSize = new int[] { columnNumber, rowNumber };
		int[] localSize = new int[] { 16, 16 };

		CLKernel matrixTransposeKernel = res.createKernel("transpose");
		matrixTransposeKernel.setArgs(out, m1, columnNumber, rowNumber);
		matrixTransposeKernel.enqueueNDRange(queue, globalSize);

		return out;
	}

	public static CLBuffer<Float> transposeN(CLBuffer<Float> m1, int columnNumber, int rowNumber, OpenCLResources res) {
		LOGGER.debug("transposeN start..");
		CLQueue queue = res.getQueue();
		queue.finish();
		CLBuffer<Float> out = res.getContext().createFloatBuffer(CLMem.Usage.InputOutput, columnNumber * rowNumber);
		final int BLOCK_DIM = 16;
		int[] globalSize = new int[2];
		globalSize[0] = shrRoundUp(BLOCK_DIM, (rowNumber));
		globalSize[1] = shrRoundUp(BLOCK_DIM, (columnNumber));
		System.out.println("globalSize[0] " + globalSize[0]);
		System.out.println("globalSize[1] " + globalSize[1]);
		int[] localSize = new int[2];
		localSize[0] = BLOCK_DIM;
		localSize[1] = BLOCK_DIM;

		CLKernel matrixTransposeKernel = res.createKernel("transpose2");
		System.out.println("matrixTransposeKernel.setArgs " + columnNumber + " " + rowNumber + " " + (BLOCK_DIM + 1) * BLOCK_DIM);
		matrixTransposeKernel.setArgs(out, m1, columnNumber, rowNumber, new LocalSize((BLOCK_DIM + 1) * BLOCK_DIM));
		matrixTransposeKernel.enqueueNDRange(queue, globalSize, localSize);

		return out;
	}

	public static CLBuffer<Float> convertToBinary(CLBuffer<Float> m1, int columnNumber, int rowNumber, Pointer<Integer> random, OpenCLResources res) throws IOException {
		LOGGER.debug("transpose start..");
		CLQueue queue = res.getQueue();
		queue.finish();
		CLBuffer<Integer> randomBuffer = res.getContext().createIntBuffer(CLMem.Usage.Input, random);
		CLBuffer<Float> out = res.getContext().createFloatBuffer(CLMem.Usage.InputOutput, columnNumber * rowNumber);
		// CLBuffer<Float> out =
		// CLMemoryManager.createEmptyCLBuffer(columnNumber * rowNumber);
		int[] globalSize = new int[] { columnNumber * rowNumber };
		CLKernel matrixTransposeKernel = res.createKernel("convertToBinary");
		matrixTransposeKernel.setArgs(out, m1, randomBuffer, Integer.MAX_VALUE);
		matrixTransposeKernel.enqueueNDRange(queue, globalSize);

		return out;
	}

	static int shrRoundUp(int localWorkSize, int numItems) {
		int result = localWorkSize;
		while (result < numItems)
			result += localWorkSize;

		return result;
	}

	public static CLBuffer<Float> substractMatrix(CLBuffer<Float> m1, CLBuffer<Float> m2, int length, OpenCLResources res) {
		LOGGER.debug("substractMatrix start..");
		CLQueue queue = res.getQueue();
		queue.finish();
		int[] globalSize = new int[] { length };

		CLKernel substractMatrixKernel = res.createKernel("substractTwoArrays");
		substractMatrixKernel.setArgs(m1, m2);
		substractMatrixKernel.enqueueNDRange(queue, globalSize);

		return m1;
	}

	public static CLBuffer<Float> addMatrix(CLBuffer<Float> m1, CLBuffer<Float> m2, int length, OpenCLResources res) {
		LOGGER.debug("addMatrix start..");
		CLQueue queue = res.getQueue();
		queue.finish();
		int[] globalSize = new int[] { length };

		CLKernel addMatrixKernel = res.createKernel("addTwoArrays");
		addMatrixKernel.setArgs(m1, m2);
		addMatrixKernel.enqueueNDRange(queue, globalSize);

		return m1;
	}

	public static CLBuffer<Float> multipleMatrix(CLBuffer<Float> m1, int length, float alpha, OpenCLResources res) {
		LOGGER.debug("multipleMatrix start..");
		CLQueue queue = res.getQueue();
		queue.finish();
		int[] globalSize = new int[] { length };

		CLKernel matrixMultiplicationKernel = res.createKernel("multipleArray");
		matrixMultiplicationKernel.setArgs(m1, alpha);
		matrixMultiplicationKernel.enqueueNDRange(queue, globalSize);

		return m1;
	}

	public static CLBuffer<Float> maskMatrix(CLBuffer<Float> matrixToMask, CLBuffer<Float> mask, int length, OpenCLResources res) {
		LOGGER.debug("maskMatrix start..");
		CLQueue queue = res.getQueue();
		queue.finish();
		int[] globalSize = new int[] { length };

		CLKernel matrixMultiplicationKernel = res.createKernel("maskingMatrix");
		matrixMultiplicationKernel.setArgs(matrixToMask, mask);
		matrixMultiplicationKernel.enqueueNDRange(queue, globalSize);

		return matrixToMask;
	}

	public static CLBuffer<Float> reduce(CLBuffer<Float> data, int length, OpenCLResources res) {
		LOGGER.debug("reduce start..");
		CLQueue queue = res.getQueue();
		queue.finish();
		int[] globalSize = new int[] { 4 };
		int[] localSize = new int[] { 4 };

		CLBuffer resultBuffer = res.getContext().createFloatBuffer(CLMem.Usage.InputOutput, 1);
		CLKernel matrixReduceKernel = res.createKernel("reduce");
		matrixReduceKernel.setArgs(data, new LocalSize(localSize[0]), length, resultBuffer);
		matrixReduceKernel.enqueueNDRange(queue, globalSize, localSize);

		return resultBuffer;
	}

	public static CLBuffer<Float> addVectorToMatrix(CLBuffer<Float> m1, CLBuffer<Float> m2, int rowNumber, int colNumber, int biasRowNumber, int biasColNumber, OpenCLResources res)
			throws Exception {
		LOGGER.debug("addMatrix start..");
		CLQueue queue = res.getQueue();
		queue.finish();
		int[] globalSize = new int[] { rowNumber, colNumber };

		CLKernel addMatrixKernel = null;
		if (biasColNumber == 1) {
			addMatrixKernel = res.createKernel("addToRow");
		} else if (biasRowNumber == 1) {
			addMatrixKernel = res.createKernel("addToColumn");
		} else {
			throw new Exception("At least one of the bias column should be one" + " colNumber " + biasColNumber + " rowNumber " + biasRowNumber);
		}
		addMatrixKernel.setArgs(m1, m2);
		addMatrixKernel.enqueueNDRange(queue, globalSize);

		return m1;
	}
}
