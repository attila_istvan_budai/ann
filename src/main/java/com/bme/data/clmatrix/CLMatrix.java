package com.bme.data.clmatrix;

import static org.bridj.Pointer.allocateFloats;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.bridj.Pointer;

import com.bme.data.densematrix.DenseMatrix;
import com.bme.data.matrix.Matrix;
import com.bme.opencl.OpenCLResources;
import com.nativelibs4java.opencl.CLBuffer;
import com.nativelibs4java.opencl.CLMem.Usage;

public class CLMatrix extends Matrix {
	private int columnLength;
	private int rowLength;
	private CLBuffer<Float> data;
	OpenCLResources res;

	int matrixID;
	static int counter;

	public static Map<Integer, String> matrices = new HashMap<Integer, String>();

	public int getColumnLength() {
		return columnLength;
	}

	public void setColumnLength(int columnLength) {
		this.columnLength = columnLength;
	}

	public int getRowLength() {
		return rowLength;
	}

	public void setRowLength(int rowLength) {
		this.rowLength = rowLength;
	}

	public CLBuffer<Float> getBuffer() {
		return data;
	}

	public float[] getData() {
		Pointer<Float> outPtr = data.read(res.getQueue());
		return outPtr.getFloats();
	}

	public void setData(CLBuffer<Float> data) {
		this.data = data;
	}

	public CLMatrix(CLBuffer<Float> d, int columnLength, int rowLength, OpenCLResources res) {
		super();
		this.data = d;
		this.columnLength = columnLength;
		this.rowLength = rowLength;
		this.res = res;
		this.matrixID = counter++;
		matrices.put(matrixID, "col:" + columnLength + " row:" + rowLength);
		// System.out.println("matrix " + matrixID + " has been created");

	}

	public CLMatrix(final DenseMatrix matrix, OpenCLResources resources) {
		Pointer<Float> oPointer = allocateFloats(matrix.getData().length).order(resources.getByteOrder());
		oPointer.setFloats(matrix.getData());
		this.data = resources.getContext().createBuffer(Usage.InputOutput, oPointer);
		this.columnLength = matrix.getColumnLength();
		this.rowLength = matrix.getRowLength();
		this.res = resources;
	}

	public CLMatrix copy() {
		long startIteration = System.currentTimeMillis();
		CLBuffer<Float> newBuffer = this.res.getContext().createFloatBuffer(Usage.InputOutput, this.data.getElementCount());
		// CLBuffer<Float> newBuffer =
		// CLMemoryManager.createEmptyCLBuffer(this.data.getElementCount());

		this.data.copyTo(this.res.getQueue(), newBuffer);
		return new CLMatrix(newBuffer, columnLength, rowLength, res);
	}

	public CLMatrix dot(CLMatrix other, CLBuffer<Float> out) {
		CLMatrixOperations.multiplyByMatrix(this.getBuffer(), other.getBuffer(), out, this.getColumnLength(), other.getColumnLength(), this.getRowLength(), res);
		return new CLMatrix(out, other.getColumnLength(), rowLength, res);
	}

	public CLMatrix dot(CLMatrix other) {
		CLBuffer<Float> out = CLMatrixOperations.multiplyByMatrix(this.getBuffer(), other.getBuffer(), this.getColumnLength(), other.getColumnLength(), this.getRowLength(), res);
		return new CLMatrix(out, other.getColumnLength(), rowLength, res);
	}

	public CLMatrix dotV2(CLMatrix other) {
		CLBuffer<Float> out = CLMatrixOperations.multiplyByMatrixV2(this.getBuffer(), other.getBuffer(), this.getColumnLength(), other.getColumnLength(), this.getRowLength(), res);
		return new CLMatrix(out, other.getColumnLength(), rowLength, res);
	}

	public CLMatrix applySigmoid() {
		CLMatrixOperations.applySigmoid(this.getBuffer(), this.getColumnLength(), this.getRowLength(), res);
		return this;
	}

	public CLMatrix dotThenSigmoid(CLMatrix other, CLBuffer<Float> out) {
		CLMatrixOperations.multiplyByMatrixWithSigmoid(this.getBuffer(), other.getBuffer(), out, this.getColumnLength(), other.getColumnLength(), this.getRowLength(), res);
		return new CLMatrix(out, other.getColumnLength(), rowLength, res);
	}

	public CLMatrix dotThenSigmoid(CLMatrix weight) {
		CLBuffer<Float> out = CLMatrixOperations.multiplyByMatrixWithSigmoid(this.getBuffer(), weight.getBuffer(), this.getColumnLength(), weight.getColumnLength(),
				this.getRowLength(), res);
		return new CLMatrix(out, weight.getColumnLength(), rowLength, res);
	}

	public CLMatrix dotThenSigmoidV2(CLMatrix other) {
		CLBuffer<Float> out = CLMatrixOperations.multiplyByMatrixWithSigmoidV2(this.getBuffer(), other.getBuffer(), this.getColumnLength(), other.getColumnLength(),
				this.getRowLength(), res);
		return new CLMatrix(out, other.getColumnLength(), rowLength, res);
	}

	public CLMatrix transpose() {
		CLBuffer<Float> out = CLMatrixOperations.transpose(this.data, this.getColumnLength(), this.getRowLength(), res);
		return new CLMatrix(out, rowLength, columnLength, res);
	}

	public CLMatrix add(CLMatrix other) {
		int length = this.getColumnLength() * this.getRowLength();
		CLMatrixOperations.addMatrix(this.getBuffer(), other.getBuffer(), length, res);
		return this;

	}

	public CLMatrix addVectorToMatrix(CLMatrix other) throws Exception {
		CLMatrixOperations.addVectorToMatrix(this.getBuffer(), other.getBuffer(), this.getRowLength(), this.getColumnLength(), other.getRowLength(), other.getColumnLength(), res);
		return this;
	}

	public CLMatrix substract(CLMatrix other) {
		int length = this.getColumnLength() * this.getRowLength();
		CLMatrixOperations.substractMatrix(this.getBuffer(), other.getBuffer(), length, res);
		return this;
	}

	public CLMatrix multiplyWithConstant(float alpha) {
		int length = this.getColumnLength() * this.getRowLength();
		CLMatrixOperations.multipleMatrix(this.getBuffer(), length, alpha, res);
		return this;
	}

	public CLMatrix mask(CLMatrix other) {
		int length = this.getColumnLength() * this.getRowLength();
		CLMatrixOperations.maskMatrix(this.getBuffer(), other.getBuffer(), length, res);
		return this;

	}

	public CLMatrix toBinary(Pointer<Integer> random) {
		CLBuffer<Float> out = null;
		try {
			out = CLMatrixOperations.convertToBinary(this.data, this.getColumnLength(), this.getRowLength(), random, res);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return new CLMatrix(out, rowLength, columnLength, res);
	}

	public String toString() {
		int length = this.getColumnLength() * this.getRowLength();
		float[] data = this.getData();
		String str = "";
		for (int i = 0; i < length; i++) {
			if (i % columnLength == 0)
				str += '\n';
			str += data[i] + "  ";
		}
		return str;
	}

	public void freeMemory() {
		try {
			this.data.release();
			matrices.remove(matrixID);
		} catch (Exception e) {
			System.out.println("matrix" + this.matrixID + " already released");
		}

	}

}
