package com.bme.data.diagonalmatrix;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import com.bme.data.matrix.MatrixElement;

public class DiagonalMatrix {
	
	private float[] data;
	private float[] offset;
	private int columnLength;
	private int rowLength;
	
	public DiagonalMatrix(float[] data, float[] offset, int columnLength, int rowLength) {
		this.data = data;
		this.offset = offset;
		this.columnLength = columnLength;
		this.rowLength = rowLength;
	}
	

	public float[] getData() {
		return data;
	}

	public void setData(float[] data) {
		this.data = data;
	}

	public float[] getOffset() {
		return offset;
	}

	public void setOffset(float[] offset) {
		this.offset = offset;
	}

	public int getColumnLength() {
		return columnLength;
	}

	public void setColumnLength(int columnLength) {
		this.columnLength = columnLength;
	}

	public int getRowLength() {
		return rowLength;
	}

	public void setRowLength(int rowLength) {
		this.rowLength = rowLength;
	}
	
	public void constructDiagonalMatrixFromElements(List<MatrixElement> elements, int rows, int cols){
		Collections.sort(elements, new DiagonalMatrixElementComparator());
		int rowNumber = 0;
		int columnNumber = rows < cols ? rows : cols;
		float[] dataSet = new float[rows*cols];
		int lastdia=calculateDiagonalNumber(elements.get(0));
		for(MatrixElement elem : elements){
			int dia = calculateDiagonalNumber(elem);
			if(lastdia != dia){
				rowNumber++;
			}
			dataSet[rowNumber*columnNumber+ elem.getColumnNumber()] = elem.getData();
		}
		
	}
	
	public int calculateDiagonalNumber(MatrixElement elem){
		return elem.getColumnNumber() - elem.getRowNumber();
	}
	
	
	 static class DiagonalMatrixElementComparator implements Comparator<MatrixElement>
	 {
	     public int compare(MatrixElement m1, MatrixElement m2)
	     {
	         if(m1.getColumnNumber() - m1.getRowNumber() < m2.getColumnNumber() - m2.getRowNumber()){
	        	 return 1;
	         }
	         else if(m1.getColumnNumber() - m1.getRowNumber() > m2.getColumnNumber() - m2.getRowNumber()){
	        	 return -1;
	         }
	         else if(m1.getRowNumber() > m2.getRowNumber()){
	        	 return 1;
	         }
	         else if(m1.getRowNumber() < m2.getRowNumber()){
	        	 return -1;
	         }
	         else {
	        	 return 0;
	         }
	     }
	 }
	


}
