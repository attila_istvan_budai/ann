package com.bme.memorytest;

import java.awt.image.BufferedImage;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.imageio.ImageIO;

import com.bme.data.densematrix.DenseMatrix;
import com.bme.data.matrix.MatrixElement;
import com.bme.datamining.ann.ErrorCalculation;
import com.bme.datamining.ann.dnn.ConvulotionalNetwork;
import com.bme.datamining.ann.dnn.ConvulotionalNetwork.ConvulotionalNetworkBuilder;
import com.bme.datamining.ann.dnn.DNNOpenCL;
import com.bme.datamining.ann.dnn.DNNOpenCL.DNNBuilder;
import com.bme.datamining.ann.rbm.RestrictedBoltzmannMachineOpenCL;
import com.bme.datamining.filereader.LoadModel;
import com.bme.datamining.filereader.ReadFiles;
import com.bme.datamining.filewriter.SaveFloatArray;
import com.bme.datamining.filewriter.SaveModel;
import com.bme.datamining.svm.SVM;

import de.bwaldvogel.liblinear.SolverType;

public class MemoryTest {

	List<MinstItem> trainingSet;
	List<MinstItem> testSet;
	DNNOpenCL dnn;
	ConvulotionalNetwork convNN;
	DNNBuilder dnnBuilder;
	private float[] weight;
	float[] trainingMatrix = null;
	float[] trainingLabels = null;
	float[] testLabels = null;
	float[] testMatrix = null;

	List<float[]> weights = new ArrayList<>();

	static int numberOfElements = 60000;

	String fileName;

	public static class Config {
		String name;
		float learningRate;
		double ratio;
		int iterationNumber;
		float momentum;
		float updateWeightParam;
		int batchSize;
		List<Integer> hiddenUnitNumbers;

		public Config(float learningRate, double ratio, int iterationNumber, List<Integer> hiddenUnitNumbers, float momentum, float updateWeightParam, int batchSize) {
			this.name = MemoryTest.getFileName(hiddenUnitNumbers.get(0), learningRate, iterationNumber, momentum, updateWeightParam, hiddenUnitNumbers.size());
			this.learningRate = learningRate;
			this.ratio = ratio;
			this.iterationNumber = iterationNumber;
			this.momentum = momentum;
			this.updateWeightParam = updateWeightParam;
			this.batchSize = batchSize;
			this.hiddenUnitNumbers = hiddenUnitNumbers;
		}

	}

	public static String getFileName(int hiddenUnits, float learningRate, int iterationNumber, float momentum, float updateWeightParam, int layers) {
		return "info_hidden" + hiddenUnits + "_lr_" + learningRate + "_iteration_" + iterationNumber + "_momentum_" + momentum + "_updateWeightParam_" + updateWeightParam
				+ "_layers_" + layers;
	}

	public void moveFile(File src, File root) {
		try {
			if (src.renameTo(new File(root.getPath() + "//" + src.getName()))) {
				System.out.println("File is moved successful!");
			} else {
				System.out.println("File is failed to move!");
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static File createRoot() {
		File dir = new File("results");
		if (!dir.exists()) {
			dir.mkdir();
		}
		return dir;
	}

	public void appendToFile(String text) {

		try {
			File yourFile = new File(fileName);
			if (!yourFile.exists()) {
				yourFile.createNewFile();
			}
			Files.write(Paths.get(fileName), ('\n' + text).getBytes(), StandardOpenOption.APPEND);
		} catch (IOException e) {
			// exception handling left as an exercise for the reader
		}
	}

	public static void main(String[] args) throws Exception {
		MemoryTest test = new MemoryTest();
		test.testWithMovieData();
		// mnistDataTest();
		// loadDataTest("DNN_model-20151216_205302");
	}

	public static List<Config> getConfigs() {
		List<Config> configs = new ArrayList<Config>();
		List<Integer> hiddenUnitNumbers = new ArrayList<>();
		hiddenUnitNumbers.add(200);
		// configs.add(new Config(0.1f, 1, 201, hiddenUnitNumbers, 0.1f, 0.05f,
		// numberOfElements));
		// configs.add(new Config(0.1f, 1, 1001, hiddenUnitNumbers, 0.1f,
		// 0.01f));
		// configs.add(new Config(0.1f, 1, 1001, hiddenUnitNumbers, 0.1f,
		// 0.005f));
		// configs.add(new Config(0.1f, 1, 1001, hiddenUnitNumbers, 0.1f,
		// 0.001f, numberOfElements));

		// configs.add(new Config(0.1f, 1, 1001, hiddenUnitNumbers, 0.01f,
		// 0.05f));
		// configs.add(new Config(0.1f, 1, 1001, hiddenUnitNumbers, 0.01f,
		// 0.01f));
		// configs.add(new Config(0.1f, 1, 1001, hiddenUnitNumbers, 0.01f,
		// 0.005f));
		// configs.add(new Config(0.1f, 1, 1001, hiddenUnitNumbers, 0.01f,
		// 0.001f));

		hiddenUnitNumbers = new ArrayList<>();
		hiddenUnitNumbers.add(256);

		// configs.add(new Config(0.1f, 1, 1001, hiddenUnitNumbers, 0.1f,
		// 0.05f));
		// configs.add(new Config(0.1f, 1, 1001, hiddenUnitNumbers, 0.1f,
		// 0.01f));
		// configs.add(new Config(0.1f, 1, 1001, hiddenUnitNumbers, 0.1f,
		// 0.005f));
		// configs.add(new Config(0.1f, 1, 1001, hiddenUnitNumbers, 0.1f,
		// 0.001f, numberOfElements));
		//
		// configs.add(new Config(0.1f, 1, 1001, hiddenUnitNumbers, 0.01f,
		// 0.05f));
		// configs.add(new Config(0.1f, 1, 1001, hiddenUnitNumbers, 0.01f,
		// 0.01f));
		// configs.add(new Config(0.1f, 1, 1001, hiddenUnitNumbers, 0.01f,
		// 0.005f));
		// configs.add(new Config(0.1f, 1, 1001, hiddenUnitNumbers, 0.01f,
		// 0.001f, numberOfElements));

		hiddenUnitNumbers = new ArrayList<>();
		// hiddenUnitNumbers.add(500);s
		hiddenUnitNumbers.add(784);

		// configs.add(new Config(0.1f, 1, 1001, hiddenUnitNumbers, 0.01f,
		// 0.05f));
		// configs.add(new Config(0.1f, 1, 1001, hiddenUnitNumbers, 0.01f,
		// 0.01f, numberOfElements));
		// configs.add(new Config(0.1f, 1, 1001, hiddenUnitNumbers, 0.01f,
		// 0.005f));

		// configs.add(new Config(0.9f, 1, 101, hiddenUnitNumbers, 0.01f,
		// 0.001f, numberOfElements));
		// configs.add(new Config(0.5f, 1, 101, hiddenUnitNumbers, 0.01f,
		// 0.001f, numberOfElements));

		/* Error: 0.12479061970759983 */
		// configs.add(new Config(0.2f, 1, 101, hiddenUnitNumbers, 0.01f,
		// 0.001f, numberOfElements));

		/* Error: 0.11613783013669811 */
		// configs.add(new Config(0.1f, 1, 101, hiddenUnitNumbers, 0.01f,
		// 0.001f, numberOfElements));
		/* 0.11452383466055065 */
		// configs.add(new Config(0.08f, 1, 101, hiddenUnitNumbers, 0.01f,
		// 0.001f, numberOfElements));

		/* Error: 0.11316543091157016 */
		// configs.add(new Config(0.07f, 1, 101, hiddenUnitNumbers, 0.01f, 0.1f,
		// numberOfElements));
		/* 0.11097600115174745 */
		// configs.add(new Config(0.07f, 1, 101, hiddenUnitNumbers, 0.01f,
		// 0.01f, numberOfElements));
		/* 0.10971704897352637 */
		// configs.add(new Config(0.07f, 1, 101, hiddenUnitNumbers, 0.01f,
		// 0.001f, numberOfElements));
		/* 0.110386924822502 */
		// configs.add(new Config(0.07f, 1, 101, hiddenUnitNumbers, 0.01f,
		// 0.0001f, numberOfElements));

		/* Error: 0.20375816414510195 */
		// configs.add(new Config(0.01f, 1, 101, hiddenUnitNumbers, 0.01f,
		// 0.001f, numberOfElements));

		/* 0.10805062495490833 */
		// configs.add(new Config(0.07f, 1, 101, hiddenUnitNumbers, 0.01f,
		// -0.01f, numberOfElements));

		configs.add(new Config(0.07f, 1, 101, hiddenUnitNumbers, 0.01f, 0.001f, numberOfElements));

		// configs.add(new Config(0.07f, 1, 101, hiddenUnitNumbers, 0.01f,
		// 0.001f, numberOfElements));
		return configs;

	}

	private static byte[] intArrayToByte(int[] array) {
		byte[] res = new byte[array.length];
		for (int i = 0; i < array.length; i++) {
			res[i] = (byte) (array[i] & 0xFF);
		}
		return res;
	}

	public static BufferedImage getImageFromArray(int[] pixels, int width, int height) {
		BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_BYTE_GRAY);
		image.getRaster().setDataElements(0, 0, width, height, intArrayToByte(pixels));
		return image;
	}

	public void writeToFile(BufferedImage bi, String index) {
		try {
			File outputfile = new File(index + ".png");
			ImageIO.write(bi, "png", outputfile);
		} catch (IOException e) {
		}
	}

	public static void loadDataTest(String root) {
		int arrayLength = 28 * 28;
		MemoryTest test = new MemoryTest();
		test.readFromMnist(numberOfElements, 1);
		test.loadOriginalDataset(arrayLength);
		test.loadOriginalTestDataSet(arrayLength);

		test.dnn = LoadModel.loadModel(root);
		// test.dnn.evaulateTestData(test.testMatrix, test.testLabels, 10000,
		// 784);
		test.dnn.setSvm(new SVM());

		// float[] constructedData = test.constructTrainingData(784);

		double eps = 0.0001;
		double C = 4;
		test.dnn.setSVMParameters(SolverType.L2R_LR, C, eps);
		long startIteration = System.currentTimeMillis();
		float trainAccuracy = test.dnn.learnSVM(test.trainingMatrix, test.trainingLabels, test.numberOfElements, 784);
		// float trainAccuracy = test.dnn.learnlibSVM(test.trainingMatrix,
		// test.trainingLabels, test.numberOfElements, 784);
		long currentTime = System.currentTimeMillis() - startIteration;
		float testAccuracy = test.dnn.evaulateTestData(test.testMatrix, test.testLabels, 10000, 784);
		// float testAccuracy = test.dnn.evaulateLibSVMTestData(test.testMatrix,
		// test.testLabels, 10000, 784);
		System.out.println(currentTime);
		String rootNew = SaveModel.saveNewModel(test.dnn, null);

		PrintWriter writer = null;
		try {
			File file = new File("models/" + rootNew, "meta.dat");
			System.out.println(file.getAbsolutePath());
			if (!file.exists()) {
				file.createNewFile();
			}
			writer = new PrintWriter(file, "UTF-8");
			writer.print("Training accuracy:");
			writer.println(Float.toString(trainAccuracy));
			writer.print("Test accuracy:");
			writer.println(Float.toString(testAccuracy));
			writer.print("Execution time:");
			writer.print(Long.toString(currentTime));

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			writer.close();
		}

	}

	public static void mnistDataTest() throws Exception {
		List<Config> configs = getConfigs();
		int arrayLength = 28 * 28;
		boolean firstRun = true;

		MemoryTest test = new MemoryTest();
		test.readFromMnist(numberOfElements, 1);

		test.loadOriginalDataset(arrayLength);
		// test.loadOriginalDataset(arrayLength);
		// test.loadOriginalTestDataSet(arrayLength);

		for (Config config : configs) {

			test.fileName = config.name;

			// test.learnSVM(arrayLength, test.trainingMatrix, test.testMatrix);

			// test.learnConv(numberOfElements, config.learningRate,
			// config.iterationNumber, config.hiddenUnitNumbers, arrayLength,
			// config.momentum, config.updateWeightParam,
			// firstRun, config.batchSize, 28, 28, 14, 14);
			// firstRun = false;
			// ------------------------------------------------------------------------------------------------------------------

			test.learn(numberOfElements, config.learningRate, config.iterationNumber, config.hiddenUnitNumbers, arrayLength, config.momentum, config.updateWeightParam, firstRun,
					config.batchSize);
			firstRun = false;

			String root = SaveModel.saveNewModel(test.dnn, null);
			test.moveFile(new File(test.fileName), new File("models", root));

			// SVM model =
			// test.learnFromModel(config.hiddenUnitNumbers.get(config.hiddenUnitNumbers.size()
			// - 1));
			// test.saveImages(model.getMisPredictedElementIndexes());
			// ------------------------------------------------------------------------------------------------------------------

			// float[] constructedData =
			// test.constructTrainingData(hiddenUnits);
			// float[] constructedTestData =
			// test.constructTestData(hiddenUnits);
			//
			// test.learnSVM(hiddenUnits, constructedData, constructedTestData);

		}
	}

	public void saveImages(Map<Integer, double[]> misPredictedElementIndexes) {
		for (Entry<Integer, double[]> entry : misPredictedElementIndexes.entrySet()) {
			Integer key = entry.getKey();
			writeToFile(getImageFromArray(testSet.get(key).data, 28, 28), Integer.toString(key));
			float[] rec = dnn.reconstructData(normalizeData(testSet.get(key).data, 255), 1);
			writeToFile(getImageFromArray(convertBack(rec, 255), 28, 28), Integer.toString(key) + "R");
			double[] value = entry.getValue();
			System.out.print(key + " ");
			arrayToString(value, 10);
		}
	}

	public void arrayToString(double[] array, int columns) {
		for (int i = 0; i < array.length; i++) {
			if (i != 0 && i % columns == 0) {
				System.out.println();
			}
			System.out.print(array[i] + " ");
		}
		System.out.println();
	}

	public void testWithMovieData() throws Exception {
		RestrictedBoltzmannMachineOpenCL rbm = new RestrictedBoltzmannMachineOpenCL();
		List<MatrixElement> elements = ReadFiles.load("./src/main/resources/ratings.train", " ");
		DenseMatrix matrix = new DenseMatrix(elements, 3952, 6040);
		float[] data = normalizeData(matrix.getData(), 5);

		int trainingSize = 5000;
		int testSize = 1040;
		float[] training = new float[trainingSize * 3952];
		float[] test = new float[testSize * 3952];

		System.arraycopy(data, 0, training, 0, training.length);
		System.arraycopy(data, training.length, test, 0, test.length);

		float[] w = rbm.train(training, 3952, 100, trainingSize, 500, 0.05f, 0.01f, 50, 0.01f, trainingSize);

		float[] reconstructed = rbm.reConstruct(test, w, 3952, 100, 1040);
		printRow(test, 3952, 0);
		printRow(reconstructed, 3952, 0);
		System.out.println();
		printRow(test, 3952, 1);
		printRow(reconstructed, 3952, 1);

		List<MatrixElement> testElement = new ArrayList<>();
		for (MatrixElement elem : elements) {
			if (elem.getRowNumber() > trainingSize) {
				testElement.add(elem);
			}
		}
		System.out.println(testElement.size());
	}

	public void printRow(float[] data, int columnNumber, int rowIndex) {
		int startindex = columnNumber * rowIndex;
		for (int i = startindex; i < startindex + columnNumber; i++) {
			System.out.print(data[i] + "\t\t");
		}
		System.out.println();
	}

	private void loadOriginalTestDataSet(int arrayLength) {
		int testSize = 10000;
		testMatrix = new float[testSize * arrayLength];
		testLabels = new float[testSize];
		File f = new File("C:\\mnistPrepearedData\\testLabels.dat");
		File f2 = new File("C:\\mnistPrepearedData\\testMatrix.dat");
		if (f.exists() && f2.exists()) {
			testMatrix = SaveFloatArray.readOO(testSize * arrayLength, "C:\\mnistPrepearedData\\testMatrix.dat");
			testLabels = SaveFloatArray.readOO(testSize, "C:\\mnistPrepearedData\\testLabels.dat");
		} else {
			for (int i = 0; i < testSet.size(); i++) {
				testLabels[i] = Integer.parseInt(testSet.get(i).label);
				float[] currentArray = normalizeData(testSet.get(i).data, 255);
				System.arraycopy(currentArray, 0, testMatrix, i * arrayLength, arrayLength);
			}
			SaveFloatArray.storeOO(testLabels, "C:\\mnistPrepearedData\\testLabels.dat");
			SaveFloatArray.storeOO(testMatrix, "C:\\mnistPrepearedData\\testMatrix.dat");
		}

	}

	private void loadOriginalDataset(int arrayLength) {
		trainingMatrix = new float[numberOfElements * arrayLength];
		trainingLabels = new float[numberOfElements];
		File f = new File("C:\\mnistPrepearedData\\trainingLabels.dat");
		File f2 = new File("C:\\mnistPrepearedData\\trainingMatrix.dat");
		if (f.exists() && f2.exists()) {
			trainingMatrix = SaveFloatArray.readOO(numberOfElements * arrayLength, "C:\\mnistPrepearedData\\trainingMatrix.dat");
			trainingLabels = SaveFloatArray.readOO(numberOfElements, "C:\\mnistPrepearedData\\trainingLabels.dat");
		} else {
			for (int i = 0; i < trainingSet.size(); i++) {
				trainingLabels[i] = Integer.parseInt(trainingSet.get(i).label);
				float[] currentArray = normalizeData(trainingSet.get(i).data, 255);
				System.arraycopy(currentArray, 0, trainingMatrix, i * arrayLength, arrayLength);
			}
		}

		SaveFloatArray.storeOO(trainingLabels, "C:\\mnistPrepearedData\\trainingLabels.dat");
		SaveFloatArray.storeOO(trainingMatrix, "C:\\mnistPrepearedData\\trainingMatrix.dat");
	}

	public void addModel(float[] weight) {
		weights.add(weight);
	}

	public SVM learnFromModel(int hiddenUnits) {
		float[] constructedData = constructTrainingData(hiddenUnits);
		float[] constructedTestData = constructTestData(hiddenUnits);
		return learnSVM(hiddenUnits, constructedData, constructedTestData);
	}

	public void learnFromModel(int hiddenUnits, float[] trainingData, float[] testData) {
		learnSVM(hiddenUnits, trainingData, testData);
	}

	private SVM learnSVM(int hiddenUnits, float[] constructedData, float[] constructedTestData) {
		appendToFile("--------------------------------------------------------");
		SVM svm = new SVM();
		svm.learnLibLinear(constructedData, trainingLabels, trainingLabels.length, hiddenUnits, 0.01f, 100000, null);
		float[] predictions = svm.evaluateLibLinear(constructedData, trainingLabels, trainingLabels.length, hiddenUnits, 10, false);
		float accuracy = ErrorCalculation.calculateAccuracy(trainingLabels, predictions);
		System.out.println("Äccuracy is " + accuracy);
		appendToFile("Training accuracy is " + accuracy);

		predictions = svm.evaluateLibLinear(constructedTestData, testLabels, testLabels.length, hiddenUnits, 10, false);
		accuracy = ErrorCalculation.calculateAccuracy(testLabels, predictions);
		System.out.println("Äccuracy is " + accuracy);
		appendToFile("Test accuracy is " + accuracy);
		appendToFile("--------------------------------------------------------");
		return svm;
	}

	private float[] constructTestData(int hiddenUnits) {
		testLabels = new float[testSet.size()];
		float[] constructedTestData = new float[testSet.size() * hiddenUnits];
		for (int i = 0; i < testSet.size(); i++) {
			testLabels[i] = Integer.parseInt(testSet.get(i).label);
			float[] data = normalizeData(testSet.get(i).data, 255);
			float[] constructed = dnn.calculateHiddenLayer(data, 1);
			System.arraycopy(constructed, 0, constructedTestData, i * hiddenUnits, hiddenUnits);
			if (i % 10000 == 0) {
				System.out.println("test preapare iteration " + i);
			}
		}
		return constructedTestData;
	}

	private float[] constructTrainingData(int hiddenUnits) {
		trainingLabels = new float[numberOfElements];
		float[] constructedData = new float[trainingSet.size() * hiddenUnits];
		for (int i = 0; i < trainingSet.size(); i++) {
			trainingLabels[i] = Integer.parseInt(trainingSet.get(i).label);
			float[] data = normalizeData(trainingSet.get(i).data, 255);
			float[] constructed = dnn.calculateHiddenLayer(data, 1);
			System.arraycopy(constructed, 0, constructedData, i * hiddenUnits, hiddenUnits);
			if (i % 10000 == 0) {
				System.out.println("training preapare iteration " + i);
			}
		}
		return constructedData;
	}

	private float[] constructTrainingDataOneStep(int hiddenUnits) {
		trainingLabels = new float[numberOfElements];
		float[] constructedData = new float[trainingSet.size() * hiddenUnits];
		float[] inputMatrix = new float[trainingSet.size() * hiddenUnits];
		for (int i = 0; i < trainingSet.size(); i++) {
			trainingLabels[i] = Integer.parseInt(trainingSet.get(i).label);
			float[] data = normalizeData(trainingSet.get(i).data, 255);

			System.arraycopy(data, 0, inputMatrix, i * hiddenUnits, hiddenUnits);
		}
		constructedData = dnn.calculateHiddenLayer(inputMatrix, trainingSet.size());
		return constructedData;
	}

	public MemoryTest() {
		dnnBuilder = new DNNBuilder();
	}

	public void writeToFile(String filename, int layer) throws IOException {
		System.out.println("writeToFile " + filename + " " + layer);
		File fout = new File(filename);
		FileOutputStream fos = new FileOutputStream(fout);

		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos));

		int iteration = 0;
		bw.write("trainingSet");
		bw.newLine();
		for (MinstItem item : trainingSet) {

			iteration++;

			// already normalized
			float[] data = normalizeData(item.data, 255);
			float[] hidden = dnn.calculateHiddenLayer(data, 1, layer);
			bw.write(item.label + " ");
			for (float h : hidden) {
				bw.write(h + " ");
			}
			bw.newLine();
			if (iteration % 1000 == 0) {
				System.out.println(iteration);
			}
		}
		bw.write("testSet");
		bw.newLine();
		for (MinstItem item : testSet) {

			iteration++;
			float[] data = normalizeData(item.data, 255);
			float[] hidden = dnn.calculateHiddenLayer(data, 1, layer);
			bw.write(item.label + " ");
			for (float h : hidden) {
				bw.write(h + " ");
			}
			bw.newLine();
			if (iteration % 1000 == 0) {
				System.out.println(iteration);
			}
		}

		bw.close();
	}

	public void writeWeightsToFile(String filename, int layer) throws IOException {
		System.out.println("writeWeightsToFile " + filename + " " + layer);
		File fout = new File(filename);
		FileOutputStream fos = new FileOutputStream(fout);

		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos));

		int iteration = 0;
		float[] w = dnn.getWeights().get(layer - 1);
		for (int i = 0; i < w.length; i++) {
			if (i % dnn.getLayers().get(layer - 1).hiddenUnitNumber == 0) {
				bw.newLine();
			}
			bw.write(w[i] + " ");
		}

		bw.close();
	}

	public float[] normalizeData(int[] array, int max) {
		float[] ret = new float[array.length];
		for (int i = 0; i < array.length; i++) {
			ret[i] = (float) array[i] / max;
		}
		return ret;

	}

	public int[] convertBack(float[] array, int max) {
		int[] ret = new int[array.length];
		for (int i = 0; i < array.length; i++)
			if (array[i] > 0) {
				ret[i] = (int) (array[i] * max);
			} else {
				ret[i] = 0;
			}
		return ret;

	}

	public float[] normalizeData(float[] array, int max) {
		float[] ret = new float[array.length];
		for (int i = 0; i < array.length; i++) {
			ret[i] = (float) array[i] / max;
		}
		return ret;

	}

	private float[] learn(int numberOfElements, float learningRate, int iterationNumber, List<Integer> hiddenUnits, int arrayLength, float momentum, float updateWeightParam,
			boolean firstRun, int batchSize) throws Exception {

		DNNBuilder builder = dnnBuilder.addFirstLayer(hiddenUnits.get(0), arrayLength);
		if (hiddenUnits.size() > 1) {
			for (int i = 1; i < hiddenUnits.size(); i++) {
				dnnBuilder.addLayer(hiddenUnits.get(i));
			}
		}
		dnn = builder.createSingleton();
		// dnn = builder.addSVMToTopLayer().createSingleton();
		// dnn.setSVMParameters(SolverType.L2R_LR, 4, 0.0001);

		RBMObserver observer = new RBMObserver(this);
		dnn.registerObserver(observer);

		// rbm = new RestrictedBoltzmannMachineOpenCL();
		// rbm.registerObserver(new RBMViewObserver(this));
		// setWeight(rbm.train(trainingSet, arrayLength, hiddenNumber,
		// numberOfElements, iterationNumber, learningRate));

		return dnn.train(trainingMatrix, numberOfElements, iterationNumber, learningRate, momentum, 100, updateWeightParam, batchSize, trainingLabels);
	}

	private float[] learnConv(int numberOfElements, float learningRate, int iterationNumber, List<Integer> hiddenUnits, int arrayLength, float momentum, float updateWeightParam,
			boolean firstRun, int batchSize, int columnSize, int rowSize, int sliceWidth, int sliceHeight) throws Exception {

		// if (dnn == null) {
		ConvulotionalNetworkBuilder builder = new ConvulotionalNetworkBuilder();
		builder.addFirstLayer(hiddenUnits.get(0), arrayLength);
		if (hiddenUnits.size() > 1) {
			for (int i = 1; i < hiddenUnits.size(); i++) {
				builder.addLayer(hiddenUnits.get(i));
			}
		}
		convNN = builder.createSingleton();
		// }

		RBMObserver observer = new RBMObserver(this);
		convNN.registerObserver(observer);

		return convNN.train(trainingMatrix, numberOfElements, iterationNumber, learningRate, momentum, 50, updateWeightParam, batchSize, columnSize, rowSize, sliceWidth,
				sliceHeight);
	}

	public void readFromMnist(int mnistNumber, double ratio) {
		List<float[]> inputData = new ArrayList<>();
		File labelFile = new File("C:\\mnist\\train-labels-idx1-ubyte.gz");
		File imageFile = new File("C:\\mnist\\train-images-idx3-ubyte.gz");
		File labelFileTest = new File("C:\\mnist\\t10k-labels-idx1-ubyte.gz");
		File imageFileTest = new File("C:\\mnist\\t10k-images-idx3-ubyte.gz");
		MinstDatasetReader reader = new MinstDatasetReader(labelFile, imageFile, ratio, numberOfElements);

		MinstDatasetReader testReader = new MinstDatasetReader(labelFileTest, imageFileTest, ratio, 10000);
		trainingSet = new ArrayList<MinstItem>();
		trainingSet = reader.getAllTrainingItem();
		testSet = new ArrayList<MinstItem>();
		testSet = testReader.getAllTrainingItem();
		Collections.shuffle(trainingSet);
		System.out.println(trainingSet.size() + "number of training data loaded");
		System.out.println(testSet.size() + "number of test data loaded");

	}
}
