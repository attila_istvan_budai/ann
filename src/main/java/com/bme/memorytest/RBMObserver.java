package com.bme.memorytest;

import java.util.ArrayList;
import java.util.List;

import com.bme.datamining.logging.observer.Observer;
import com.bme.datamining.logging.observer.Subject;
import com.bme.datamining.logging.observer.simpleobserver.RBMResultObject;

public class RBMObserver implements Observer {

	private Subject topic;
	List<RBMResultObject> result;
	MemoryTest test;

	public RBMObserver(MemoryTest test) {
		this.result = new ArrayList<>();
		this.test = test;
	}

	@Override
	public void update() {
		RBMResultObject msg = (RBMResultObject) topic.getUpdate(this);
		if (msg == null) {
			System.out.println(" No new message");
		} else {
			printResult(msg);
			// model.setWeight(msg.getModel());
			// model.showSeperatedModel(msg.getModel());
		}
	}

	public void printResult(RBMResultObject msg) {
		result.add(msg);
		System.out.println(msg.getIterationNumber());
		System.out.println("obs");
		if (test != null)
			test.appendToFile("" + msg.getIterationNumber() + " " + msg.getExectuitonTime() + " " + msg.getError());
	}

	@Override
	public void setSubject(Subject sub) {
		this.topic = sub;
	}

}
