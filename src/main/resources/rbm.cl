//! Hack to make Eclipse's syntax highlighting work with OpenCL kernels
/*
#ifndef 1
#define __kernel
#define __global
#define __local
#endif
*/

float sigmoid(float z)
{
	return 1.0/(1.0+exp(-z));
}


float sigmoid4(float4 z)
{
	return 1.0/(1.0+exp(-1*(z.x+z.y+z.z+z.w)));
}


__kernel void sigmoidKernel(float input, float output)
{
	output =  1.0/(1.0+exp(input));
}



__kernel void matrixMultiplication(global float* C, const global float* A, const global float* B, int widthA)
{
  

    int i = get_global_id(0);			//m2 columnlength
    int j = get_global_id(1);			//m1 rowlength
    int widthB = get_global_size(0);
    float value=0;
    for ( int k = 0; k < widthA; k++)
    {
        value +=  A[k + j * widthA] * B[k*widthB + i];		//widthA ->m1 columnlength, widthB -> m2columnlentgh
    }
    C[i + widthB * j] = value;
}

kernel void matrix_mult(__global float4 *a_mat, __global float4 *b_mat, __global float *c_mat) {
	float sum;
	int num_rows = get_global_size(0);
	int vectors_per_row = num_rows/4;
	int start = get_global_id(0) * vectors_per_row;
	a_mat += start;
	c_mat += start*4;
	for(int i=0; i<num_rows; i++) {
		sum = 0.0f;
		for(int j=0; j<vectors_per_row; j++) {
			sum += dot(a_mat[j],
			b_mat[i*vectors_per_row + j]);
		}
	c_mat[i] = sum;
	}
}

__kernel void mmul(__global float* A, __global float* B, __global float* C, const int widthA, const int widthB, const lengthA) 
{ 
	int k,j; 
	int i= get_global_id(0); 
	float Awrk[1000]; 
	float tmp; 
	for(k=0; k<widthA; k++) 
		Awrk[k] = A[i*widthB+k]; 
		for(j=0; j<lengthA; j++){ 
			tmp= 0.0f; 
			for(k=0; k<widthA; k++) 
				tmp+= Awrk[k] * B[k*widthA+j]; 
			C[i*widthB+j] += tmp; 
	}
} 


__kernel void transpose(__global float *odata, __global float* idata, int width, int height)
{
    unsigned int xIndex = get_global_id(0);
    unsigned int yIndex = get_global_id(1);
    
    if (xIndex < width && yIndex < height)
    {
        unsigned int index_in  = xIndex + width * yIndex;
        unsigned int index_out = yIndex + height * xIndex;
        odata[index_out] = idata[index_in]; 
    }
}

__kernel void matrixMultiplicationWithSigmoid(global float* C, global float* A, global float* B, int widthA, int widthB)
{
  

    int i = get_global_id(0);			//m2 columnlength
    int j = get_global_id(1);			//m1 rowlength
    float value=0;
    for ( int k = 0; k < widthA; k++)
    {
        value +=  A[k + j * widthA] * B[k*widthB + i];		//widthA ->m1 columnlength, widthB -> m2columnlentgh
    }
    C[i + widthB * j] = sigmoid(value);
}

__kernel void addTwoArrays(global float* first, global float* second)
{
	uint i = get_global_id(0); 
	first[i]+=second[i];
}

__kernel void substractTwoArrays(global float* out, global float* first, global float* second)
{
	uint i = get_global_id(0); 
	out[i] = first[i]-second[i];
}

__kernel void multipleArray(global float* arr, const float num)
{
	uint j = get_global_id(0);
        arr[j] *= num;
   
}

__kernel void maskingMatrix(global float* matrixToMask, global float* mask)
{
	uint j = get_global_id(0);
        matrixToMask[j] *= mask[j];
   
}

__kernel void applySigmoidFunction(global float4* arr)
{
	uint j = get_global_id(0);
    arr[j] = sigmoid4(arr[j]);
   
}

#define BLOCK_DIM 16

__kernel void 	Transpose(
			 __global float2* dataout, 
			 __global float2* datain, 
			 int width, int height)

// width = N (signal length) 
// height = batch_size (number of signals in a batch)

{
// read the matrix tile into shared memory

__local float2 block[BLOCK_DIM * (BLOCK_DIM + 1)];
unsigned int xIndex = get_global_id(0);
unsigned int yIndex = get_global_id(1);

	if((xIndex < width) && (yIndex < height))
	{
	unsigned int index_in = yIndex * width + xIndex;
        int Idin = get_local_id(1)*(BLOCK_DIM+1)+get_local_id(0);
        block[Idin]=  datain[index_in];
	}

barrier(CLK_LOCAL_MEM_FENCE);

// write the transposed matrix tile to global memory

xIndex = get_group_id(1) * BLOCK_DIM + get_local_id(0);
yIndex = get_group_id(0) * BLOCK_DIM + get_local_id(1);

	if((xIndex < height) && (yIndex < width))
    {
		unsigned int index_out = yIndex * height + xIndex;
		int Idout = get_local_id(0)*(BLOCK_DIM+1)+get_local_id(1);
                dataout[index_out] = block[Idout];
    }

}


//==============================================================================================================
