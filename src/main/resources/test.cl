__kernel void addNumbers(global float* first, global float* second, global float* ret)
{
	uint i = get_global_id(0); 
	ret[i] = first[i]*second[i];
	ret[i] = ret[i]*second[i];
}

__kernel void addTwoArrays(global float* first, global float* second, global float* ret)
{
	uint i = get_global_id(0); 
	ret[i] = first[i]*second[i];
}

__kernel void appendToArray(global float* first, global float* ret)
{
	uint i = get_global_id(0); 
	ret[i] = ret[i]*first[i];
}

__kernel void powArray(global float* arr,  const int pow)
{
	uint j = get_global_id(0);
	float num = arr[j]; 
    	for(int i=0; i<pow; i++)
    	{
        	arr[j] *= num;
    	}
}

__kernel void multipleArray(global float* arr, const float num)
{
	uint j = get_global_id(0);
        arr[j] *= num;
   
}