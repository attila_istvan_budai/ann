//! Hack to make Eclipse's syntax highlighting work with OpenCL kernels
/*
 #ifndef 1
 #define __kernel
 #define __global
 #define __local
 #endif
 */

#pragma OPENCL EXTENSION cl_amd_printf : enable

float sigmoid(float z) {
	return 1.0 / (1.0 + exp(-z));
}

float sigmoidDerivative(float z) {
	return z * (1 - z);
}

float sigmoid4(float4 z) {
	return 1.0 / (1.0 + exp(-1 * (z.x + z.y + z.z + z.w)));
}

// Thread block size
#define BLOCK_SIZE 32

//////////////////////////////////////////////////////
//! Matrix multiplication on the device: C = A * B
//! wA is A's width and wB is B's width
//////////////////////////////////////////////////////
__kernel void
matrixMul(__global float* C,
		__global float* A,
		__global float* B, int wA, int wB)
{
	// Block index
	int bx = get_group_id(0);
	int by = get_group_id(1);

	// Thread index
	int tx = get_local_id(0);
	int ty = get_local_id(1);

	// Index of the first sub-matrix of A processed
	// by the block
	int aBegin = wA * BLOCK_SIZE * by;

	// Index of the last sub-matrix of A processed
	// by the block
	int aEnd = aBegin + wA - 1;

	// Step size used to iterate through the
	// sub-matrices of A
	int aStep = BLOCK_SIZE;

	// Index of the first sub-matrix of B processed
	// by the block
	int bBegin = BLOCK_SIZE * bx;

	// Step size used to iterate through the
	// sub-matrices of B
	int bStep = BLOCK_SIZE * wB;

	float Csub = 0;

	// Loop over all the sub-matrices of A and B
	// required to compute the block sub-matrix
	for (int a = aBegin, b = bBegin;
			a <= aEnd;
			a += aStep, b += bStep)
	{

		// Declaration of the local memory array As
		// used to store the sub-matrix of A
		__local float As[BLOCK_SIZE][BLOCK_SIZE];

		// Declaration of the local memory array Bs
		// used to store the sub-matrix of B
		__local float Bs[BLOCK_SIZE][BLOCK_SIZE];

		// Load the matrices from global memory
		// to local memory; each thread loads
		// one element of each matrix
		As[ty][tx] = A[a + wA * ty + tx];
		Bs[ty][tx] = B[b + wB * ty + tx];

		// Synchronize to make sure the matrices
		// are loaded
		barrier(CLK_LOCAL_MEM_FENCE);

		// Multiply the two matrices together;
		// each thread computes one element
		// of the block sub-matrix
		for (int k = 0; k < BLOCK_SIZE; ++k)
		Csub += As[ty][k] * Bs[k][tx];

		// Synchronize to make sure that the preceding
		// computation is done before loading two new
		// sub-matrices of A and B in the next iteration
		barrier(CLK_LOCAL_MEM_FENCE);

	}

	// Write the block sub-matrix to device memory;
	// each thread writes one element
	int c = wB * BLOCK_SIZE * by + BLOCK_SIZE * bx;
	C[c + wB * ty + tx] = Csub;

}

__kernel void matrixMultiplication(global float* C, const global float* A, const global float* B, const int widthA)
{

	int i = get_global_id(0);			//m2 columnlength
	int j = get_global_id(1);//m1 rowlength
	//printf("i = %d, j = %d \n",i,j);
	int widthB = get_global_size(0);
	float value=0;
		for ( int k = 0; k < widthA; k++)
		{
			value += A[k + j * widthA] * B[k*widthB + i];//widthA ->m1 columnlength, widthB -> m2columnlentgh
		}
	C[i + widthB * j] = value;
}

__kernel void matrixMultiplicationWithSigmoid(global float* C, global float* A, global float* B, int widthA)
{

	int i = get_global_id(0);			//m2 columnlength / Mdim
	int j = get_global_id(1);//m1 rowlength / Ndim
	int widthB = get_global_size(0);
	float value=0;
	for ( int k = 0; k < widthA; k++)//Pdim
	{
		value += A[k + j * widthA] * B[k*widthB + i];//widthA ->m1 columnlength, widthB -> m2columnlentgh
	}
	C[i + widthB * j] = sigmoid(value);
}

__kernel void addToRow(global float* matrix, global float* bias)
{
	int i = get_global_id(0);			//rowNumber
	int j = get_global_id(1);			//matrix Colnumber
	int matrixColumnNumber = get_global_size(1);
	matrix[j + matrixColumnNumber*i]+=bias[i];
}

__kernel void addToColumn(global float* matrix, global float* bias)
{
	int i = get_global_id(0);			//rowNumber
	int j = get_global_id(1);			//matrix Colnumber
	int matrixRowNumber = get_global_size(0);
	matrix[i + matrixRowNumber*j]+=bias[i];
}

__kernel void mmul(const int Mdim, const int Ndim, const int Pdim,
		const __global float* A, const __global float* B, __global float* C)
{
	int k, j;
	int i = get_global_id(0); // Mdim
	float tmp;
	for (j=0; j<Ndim; j++) { // Ndim
		tmp = 0.0f;
		for (k=0; k<Pdim; k++) {
			tmp += A[k + j * Pdim] * B[k*Mdim + i];
			printf("A[%d] * B[%d] \n",k + j * Pdim,k*Mdim + i);
		}
		C[j*Mdim+i] = tmp;
	}
}

__kernel void mmul_private(int widthA, int widthB,
		int heightA,
		__global float* A,
		__global float* B,
		__global float* C,
		__local float* shared) {

	int i = get_global_id(0);
	int id = get_local_id(0);
	int size = get_local_size(0);
	float Bwrk[5000];
	float tmp = 0;
	if ((i < widthB)) {
		for(int k = 0; k < widthB; ++k ) {
			Bwrk[k] = B[k*widthB + i];
		}

		for(int j=0; j<heightA; j++) {
			tmp = 0;
			for(int k = id; k < widthA; k += size) {
				shared[k] = A[k + j * widthA];
				//printf("shared[%d] = %f \n", k, A[k + k * widthA]);
			}
			barrier(CLK_LOCAL_MEM_FENCE);
			for(int k = 0; k < widthA; k++) {
				tmp += shared[k] * Bwrk[k];
				//printf("%d_%d - A[%d] * B[%d] \n",i,j, k + j * widthA, k*widthB + i);
			}
			C[j*widthB + i] = tmp;
		}
	}

}

__kernel void mmul_private_sigmoid(int widthA, int widthB,
		int heightA,
		__global float* A,
		__global float* B,
		__global float* C) {

	int i = get_global_id(0);
	int j = get_global_id(1);
	float tmp = 0;
	if ((i < widthB) && (j < heightA)) {
		tmp = 0;
		//printf("i = %d \n", i);
		//printf("j = %d \n", j);
		for(int k = 0; k < widthA; k++) {
			tmp += A[k + j * widthA] * B[k*widthB + i];
			//printf("tmp = %d \n", tmp);
		}
		//printf("C[%d] = %f \n", i*heightA + j, tmp);
		C[j*widthB + i] = sigmoid(tmp);
	}

}

__kernel void mmul_local(const int Mdim, const int Ndim, const int Pdim,
		const __global float* A, const __global float* B, __global float* C, __local float* Bwrk)
{


	int k, j;
	int i = get_global_id(0); // Mdim
	int iloc = get_local_id(0);
	int nloc = get_local_size(0);

	float Awrk[3952];

	float tmp;
	for (k=0; k<Pdim; k++)
		Awrk[k] = A[i*Ndim+k];
	for (j=0; j<Mdim; j++) {
		for (k=iloc; k<Pdim; k=k+nloc){
			Bwrk[k] = B[k*Pdim+j];
//			printf("Bwrk[%d] = B[%d] \n",k, k*Pdim+j);
		}
		barrier(CLK_LOCAL_MEM_FENCE);
		tmp = 0.0f;
		for (k=0; k<Pdim; k++)
			tmp += Awrk[k] * B[k*Pdim+j];
		C[i*Ndim+j] += tmp;
	}
}

__kernel void mmulWithSigmoid(const int Mdim, const int Ndim, const int Pdim,
__global float* A, __global float* B, __global float* C)
{
	int k, j;
	int i = get_global_id(0); // Mdim
	float tmp;
	for (j=0; j<Ndim; j++) { // Ndim
		tmp = 0.0f;
		for (k=0; k<Pdim; k++)
		tmp += A[j*Pdim+k] * B[k*Mdim+i];
		C[j*Mdim+i] = sigmoid(tmp);
	}
}

__kernel void convertToBinary(global float *odata, global float* idata, global int* random, const int max)
{
	unsigned int i = get_global_id(0);

	if (idata[i] < ((((float) random[i] / max) + 1) / 2)) {
		odata[i] = 0;
	} else {
		odata[i] = 1;
	}
}

__kernel void transpose(__global float *odata, __global float* idata, int width, int height)
{
	unsigned int xIndex = get_global_id(0);
	unsigned int yIndex = get_global_id(1);

	if (xIndex < width && yIndex < height)
	{
		unsigned int index_in = xIndex + width * yIndex;
		unsigned int index_out = yIndex + height * xIndex;
		odata[index_out] = idata[index_in];
	}
}



__kernel void addTwoArrays(global float* first, global float* second)
{
	uint i = get_global_id(0);
	first[i]+=second[i];
}

__kernel void substractTwoArrays(global float* first, global float* second)
{
	uint i = get_global_id(0);
	first[i] -= second[i];
}

__kernel void multipleArray(global float* arr, const float num)
{
	uint j = get_global_id(0);
	arr[j] *= num;

}

__kernel void maskingMatrix(global float* matrixToMask, global float* mask)
{
	uint j = get_global_id(0);
	if(mask[j] ==0){
		matrixToMask[j] = 0;
	}

}

__kernel void applySigmoidFunction(global float4* arr)
{
	uint j = get_global_id(0);
	arr[j] = sigmoid4(arr[j]);

}

__kernel void applySigmoid(global float* arr)
{
	uint j = get_global_id(0);
	arr[j] = sigmoid(arr[j]);

}


#define BLOCK_DIM 16

// This kernel is optimized to ensure all global reads and writes are coalesced,
// and to avoid bank conflicts in shared memory.  This kernel is up to 11x faster
// than the naive kernel below.  Note that the shared memory array is sized to
// (BLOCK_DIM+1)*BLOCK_DIM.  This pads each row of the 2D block in shared memory
// so that bank conflicts do not occur when threads address the array column-wise.
__kernel void transpose2(__global float *odata, __global float *idata, int width, int height, __local float* block)
{
	// read the matrix tile into shared memory
	unsigned int xIndex = get_global_id(0);
	unsigned int yIndex = get_global_id(1);

	if((xIndex < width) && (yIndex < height))
	{
		unsigned int index_in = yIndex * width + xIndex;
		block[get_local_id(1)*(BLOCK_DIM+1)+get_local_id(0)] = idata[index_in];
	}

	barrier(CLK_LOCAL_MEM_FENCE);

	// write the transposed matrix tile to global memory
	xIndex = get_group_id(1) * BLOCK_DIM + get_local_id(0);
	yIndex = get_group_id(0) * BLOCK_DIM + get_local_id(1);
	if((xIndex < height) && (yIndex < width))
    {
		unsigned int index_out = yIndex * height + xIndex;
		odata[index_out] = block[get_local_id(0)*(BLOCK_DIM+1)+get_local_id(1)];
	}
}

__kernel void
floatMatrixMultLocals(__global       float * MResp,
                      __global       float * M1,
                      __global       float * M2,
                      __global       int * q)
{
   //Identification of this workgroup
   int i = get_group_id(0);
   int j = get_group_id(1);

   //Identification of work-item
   int idX = get_local_id(0);
   int idY = get_local_id(1);

   //matrixes dimensions
   int p = get_global_size(0);
   int r = get_global_size(1);
   int qq = q[0];

   //Number of submatrixes to be processed by each worker (Q dimension)
   int numSubMat = qq/BLOCK_SIZE;

   float4 resp = (float4)(0,0,0,0);
   __local float A[BLOCK_SIZE][BLOCK_SIZE];
   __local float B[BLOCK_SIZE][BLOCK_SIZE];

   for (int k=0; k<numSubMat; k++)
   {
       //Copy submatrixes to local memory. Each worker copies one element
       //Notice that A[i,k] accesses elements starting from M[BLOCK_SIZE*i, BLOCK_SIZE*j]
       A[idX][idY] = M1[BLOCK_SIZE*i + idX + p*(BLOCK_SIZE*k+idY)];
       B[idX][idY] = M2[BLOCK_SIZE*k + idX + qq*(BLOCK_SIZE*j+idY)];
       barrier(CLK_LOCAL_MEM_FENCE);

       for (int k2 = 0; k2 < BLOCK_SIZE; k2+=4)
       {
            float4 temp1=(float4)(A[idX][k2],A[idX][k2+1],A[idX][k2+2],A[idX][k2+3]);
            float4 temp2=(float4)(B[k2][idY],B[k2+1][idY],B[k2+2][idY],B[k2+3][idY]);
            resp += temp1 * temp2;
       }
       barrier(CLK_LOCAL_MEM_FENCE);
   }

   MResp[BLOCK_SIZE*i + idX + p*(BLOCK_SIZE*j+idY)] = resp.x+resp.y+resp.z+resp.w;

}

__kernel
void reduce(__global float* x,
		__local float* localSum,
		__const int length,
		__global float* result) {

	uint global_id = get_global_id(0);
	uint global_size = get_global_size(0);
	uint local_id = get_local_id(0);
	uint group_size = get_local_size(0);

	localSum[local_id] = x[global_id] + x[global_id + global_size];
	barrier(CLK_LOCAL_MEM_FENCE);
	for(uint stride=group_size/2; stride>1; stride>>=1){
		if(local_id < stride){
			localSum[local_id] += localSum[local_id + stride];
		}
		barrier(CLK_LOCAL_MEM_FENCE);
	}
	if(local_id == 0){
		result[get_group_id(0)] = localSum[0] + localSum[1];
	}

}

//==============================================================================================================
