//! Hack to make Eclipse's syntax highlighting work with OpenCL kernels
/*
 #ifndef 1
 #define __kernel
 #define __global
 #define __local
 #endif
 */

#pragma OPENCL EXTENSION cl_amd_printf : enable

float sigmoid(float z){return 1.0/(1.0+exp(-z));}

kernel void calculateError(global read_only float* X, global read_only float* Y, global read_only float* theta, global write_only float* o, int feature_count)
{
    int j = get_global_id(0);  /* getting example index for the current thread */
    float s = 0.0;
    global float* x = X+j*feature_count;
    for(int i=0;i<feature_count;++i)
    {
        s += x[i] * theta[i];
    }
    o[j] = sigmoid(s) - Y[j];
}
/*
float sigmoid4(float4 z) {
	return 1.0 / (1.0 + exp(-1 * (z.x + z.y + z.z + z.w)));
}

__kernel void calculateError(global float* X, global float* Y, global float* weight, global float* output, int dimension)
{
	uint j = get_global_id(0);
	float4 sum = 0;
	global float4* x = (global float4*)(X+j*dimension);
	global float4* w = (global float4*)(weight);
	for(int i=0; i<dimension/4; i++)
	{
		sum += x[i] * weight[i];
	}
	output[j] = sigmoid(sum.x+sum.y+sum.z+sum.w)- Y[j];
}

__kernel void updateWeights(global float* X, global float* error, global float* weight, int datanumber, int dimension, float alpha)
{
	int j = get_global_id(0);
	float sum = 0;
	for(int i=0;i<datanumber;i++)
	{
		//printf("error[i] = %f\n",error[i]);
		//printf("X = %f\n",X[j+i*dimension]);
		sum += X[j+i*dimension] * error[i] / datanumber;
		printf("sum = %f\n",sum);
	}

	weight[j] -= alpha * sum;
}

__kernel void updateWeights(global float* X, global float* o, global float* weight, int datanumber, int dimension, float alpha)
{
    int i = get_global_id(0);
    int feature_count = get_global_size(0);
    float4 sum = (float4)(0.0, 0.0, 0.0, 0.0);
    global float4* x4 = (global float4*)(X+i*datanumber);
    global float4* o4 = (global float4*)o;

    for(int j=0;j<datanumber>>2;++j)
    {
        sum += x4[j] * o4[j];
    }
    weight[i] -= alpha * (sum.x+sum.y+sum.z+sum.w) / datanumber;
}*/

__kernel void updateWeights(global read_only float* Xtranspose, global read_only float* o, global float* theta, int example_count, int dimension, float alpha)
{
    int i = get_global_id(0); /* getting feature index for the current thread */
    int feature_count = get_global_size(0);
    float sum = 0.0;
    global float* x = Xtranspose+i*example_count;
    for(int j=0;j<example_count;++j)
    {
        sum += x[j] * o[j];
       // printf("sum = %f\n",sum);
    }
    theta[i] -= alpha * sum / example_count;
}
